/********************************************************************************************************/
/*                                            FIRMWARE                                                  */
/*                                                                                                      */
/* Firmware oficial                                                                                     */
/* Data  : 30/07/2018                                                                                   */
/* Autor : Eliel Rogerio Faccion                                                                        */
/*                                                                                                      */
/********************************************************************************************************/



/********************************************************************************************************/
/*                                  DEFINIÇÃO DE MACROS E STRINGS                                       */
/********************************************************************************************************/
#define LOW 					0
#define HIGH 					1
#define WEB_SERVER 				"wisehomefactory.hereit.com.br"
#define WEB_SERVER_FACTORY		"wisehomefactory.hereit.com.br"
#define WEB_SERVER_CLOUD		"whc.hereit.com.br"
#define WEB_PORT 				"443"
#define WEB_PORT_DEFAULT		80
#define IV_SIZE     			16
#define INPUT_SIZE  			1024
#define DEBUG_LINE				true


#define URL_DECRYPTTEST 		"https://wisehomefactory.hereit.com.br/api/device_manufacturing/DecryptTeste/"
#define URL_FACTORY 			"https://wisehomefactory.hereit.com.br/api/device_manufacturing/Register/"
#define URL_CLOUD 				"http://whc.hereit.com.br/api/queue/GetQueueDevice/"
#define URL_CLOUD_DEBUG			"http://192.168.8.125:18581/api/queue/GetQueueDevice/"
#define PORT 					800

#define POSTMASK 				"POST %s  HTTP/1.1\nHost: %s\nUser-Agent: WiseHomeSwitch\nkey: %s\niv: %s\nConnection: close\nContent-Length: %d\n\n%s\n"
#define POSTCLOUDMASK			"POST %s  HTTP/1.1\nHost: %s\nUser-Agent: WiseHomeSwitch\nGUID: %.36s\nConnection: close\nContent-Length: %d\n\n%s\n"
#define JSON_FACTORY_SCHEMA 	"{\"Manufacturing_Date\":\"%s\",\"Lote_Number\": %d,\"Serial_Number\": \"%s\",\"Factory\": %d,\"SMD_Line\": \"%s\",\"Chip_ID\": %d,\"Flash_ID\": %d, \"MacAddress\": \"%s\"}"
#define JSON_DEVICEMEMO_SCHEMA 	"{\"GUID\":\"%.36s\",\"key\":\"%.32s\",\"ssid_sta\":\"%s\",\"pass_sta\":\"%s\",\"ssid_ap\":\"%s\",\"pass_ap\":\"%s\",\"device_state\":\"%c\"}"
#define JSON_ADC_DATA			"{\"source\" : \"%.36s\", \"destiny\" : \"%.36s\", \"port\": %d, \"consumo\": %s, \"Adc\" : %.3f}"

#define btoa(x) ((x)?"true":"false")
#define wait(x) (vTaskDelay(x / portTICK_PERIOD_MS))
#define millis() sdk_system_get_rtc_time()

#define delay_ms(ms) vTaskDelay(ms / portTICK_PERIOD_MS)

//bool upnp_server_init(void);
static void mcast_task(void *arg);


/********************************************************************************************************/
/*                                       INCLUDES DE BIBLIOTECAS DE SISTEMA                             */
/********************************************************************************************************/

//****** BIBLIOTECAS DA PLATAFORMA RTOS *******
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "esp/hwrand.h"
#include <unistd.h>
#include <string.h>
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include <malloc.h>
#include "task.h"
#include <timers.h>
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"
#include "lwip/api.h"
#include <espressif/esp_wifi.h>

#include <lwip/pbuf.h>
#include <lwip/udp.h>
#include <lwip/tcp.h>
#include <lwip/ip_addr.h>
#include <lwip/api.h>
#include <lwip/netbuf.h>
#include <lwip/igmp.h>


#include <dhcpserver.h>
#include "mbedtls/config.h"
#include "mbedtls/net_sockets.h"
#include "mbedtls/debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"
#include "jsmn.h"



#include "esp8266.h"
#include "fcntl.h"
#include "unistd.h"
#include "spiffs.h"
#include "esp_spiffs.h"
#include "semphr.h"
#include "queue.h"
#include <httpd/httpd.h>
#include <stdint.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>


#include "espressif/esp_misc.h"
#include "esp/gpio_regs.h"
#include "esp/rtc_regs.h"
#include "sdk_internal.h"
#include "xtensa_ops.h"


// OTA
#include <ota-tftp.h>
#include <rboot-api.h>


/********************************************************************************************************/
/*                                    DEFINIÇÃO DE TIPOS E STRUCTS                                      */
/********************************************************************************************************/
typedef struct device_t
{
	char 				nome[10];										//Nome da porta
	uint8_t 			entsai;											//0 = Entrada; 1 = Saída; 2 = Entrada/Saída
	bool 				digital_value;									//Valor digital corrente da entrada / saída
	bool 				digital_before;									//Valor digital anterior da entrada / saída
	bool				digital_invert;									//Configura a inversão dos valores da porta
	int					analog_value;									//Valor analógico corrente da entrada/saída
	int					analog_before;									//Valor analógico anterior da entrada/saída
	bool				evt_on;											//dispara o evento ON
	bool				evt_off;										//dispara o evento OFF
	bool				evt_change;										//dispara o evento CHANGE
	bool				evt_click_up;									//dispara o evento CLICK UP
	bool				evt_click_down;									//dispara o evento CLICK DOWN
} device_t;

typedef struct memoryreg_t
{
	char 				ssid_sta[50];
	char 				pass_sta[50];
	char 				ssid_ap[50];
	char 				pass_ap[50];
	char 				GUID[36];
	char				key[32];
	char				device_state;
	uint32_t 			chip_id;
	uint32_t 			flash_id;
} memoryreg_t;

typedef struct command_t
{
	char 				source[37];
	char 				destiny[37];
	char 				date[33];
	char 				queueCommand[10][128];
} command_t;


/********************************************************************************************************/
/*                                    INICIALIZAÇÃO DE STRUCTS                                          */
/********************************************************************************************************/





/********************************************************************************************************/
/*                                    DEFINIÇÃO DE VARIÁVEIS GLOBAIS                                    */
/********************************************************************************************************/
extern const char 		*server_root_cert;								//Armazena o certificado digital para consumir serviços no servidor WiseHome
extern const char 		*server_cert;									//Armazena o certificado digital para criar o servidor no dispositivo
extern const char 		*server_key;									//Chave privada do servidor HTTP do dispositivo



const int       		pinSDClock 						= 14;
const int       		pinSDLatch 						= 15;
const int       		pinEDData 						= 12;
const int       		pinSDData 						= 13;
const int       		pinTriac1 						= 5;
const int       		pinTriac2 						= 16;
const int       		pinIR 							= 2;
const int       		pinIR_in 						= 0;
const int       		interruptPin 					= 4;  			//mudei de 2 para 4 para testarmos o zero cross signal
const gpio_inttype_t 	int_type 						= GPIO_INTTYPE_EDGE_NEG;		//Configuração do tipo da interrupçao (BORDA DE DESCIDA)

struct memoryreg_t 		deviceMemo;

unsigned char			globalKey[]						= { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32 };
unsigned char 			globalIV[] 						= { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36 };

uint8_t					iAnalogPointer_ant				= 0;	//Referencia anterior de leitura da ADC
uint8_t					iAnalogPointer					= 1;	//Referencia de leitura da ADC
//uint32_t				adcArray[100];							//Array contendo a leitura de consumo referente a ADC referenciada em iAnalogPointer
char					*arrayConsumo;
char					*arrayConsumoBuffer;
float					dConsumo 						= 0;

struct 					device_t SD1;
struct 					device_t SD2;
struct 					device_t SD3;
struct 					device_t SD4;
struct 					device_t SD5;
struct 					device_t SD6;
struct 					device_t SD7;
struct 					device_t SD8;

struct 					device_t Triac1;
struct 					device_t Triac2;

struct 					device_t ED1;
struct 					device_t ED2;
struct 					device_t ED3;
struct 					device_t ED4;
struct 					device_t ED5;
struct 					device_t ED6;
struct 					device_t ED7;
struct 					device_t ED8;

struct 					device_t EA1;
struct 					device_t EA2;
struct 					device_t EA3;
struct 					device_t EA4;
struct 					device_t EA5;
struct 					device_t EA6;
struct 					device_t EA7;
struct 					device_t EA8;




//QueueHandle_t 			queue_taskControl 				= NULL;				//QUEUE responsável por enviar mensagens a taskSentinel
//QueueHandle_t 			queue_execControl 				= NULL;				//QUEUE responsável pela execução dos comandos

SemaphoreHandle_t			xSemaforo_spiffs			= NULL;				//MUTEX
SemaphoreHandle_t			xSemaforo_taskReiniciar		= NULL;				//BINARY
//SemaphoreHandle_t 		xSemaforo_taskCreateWiFi		= NULL;			//BINARY
//SemaphoreHandle_t 		xSemaforo_taskFactory			= NULL;			//BINARY
//SemaphoreHandle_t 		xSemaforo_taskCheckCloudTLS		= NULL;			//BINARY
//SemaphoreHandle_t 		xSemaforo_taskInicializarIOs	= NULL;			//BINARY
//SemaphoreHandle_t 		xSemaforo_taskTestOutputs		= NULL;			//BINARY
//SemaphoreHandle_t 		xSemaforo_taskServidorTLS		= NULL;			//BINARY

TaskHandle_t 			taskRecuperarConfig_hdl;
TaskHandle_t 			taskCreateWiFi_hdl;
TaskHandle_t 			taskInicializarIOs_hdl;
TaskHandle_t			taskReadWriteIOs_hdl;
TaskHandle_t			taskTestOutputs_hdl;
TaskHandle_t 			taskServidorTLS_hdl;
TaskHandle_t 			taskServidorHTTP_hdl;
TaskHandle_t 			taskFactory_hdl;
TaskHandle_t 			taskCheckCloudTLS_hdl;
TaskHandle_t 			taskCheckCloud_hdl;
TaskHandle_t 			taskGravarConfig_hdl;
TaskHandle_t 			taskReiniciar_hdl;
TaskHandle_t 			taskEventos_hdl;
TaskHandle_t 			taskSentinela_hdl;
TaskHandle_t 			taskhttp_server_alexa_hdl;
TaskHandle_t 			taskmcast_hdl;
TaskHandle_t			tftp_client_task_hdl;


volatile uint8_t					flag_RecuperarConfig			= 0;
volatile uint8_t					flag_CreateWiFi						= 0;
volatile uint8_t					flag_Factory							= 0;
volatile uint8_t					flag_CheckCloud						= 0;
volatile uint8_t					flag_InicializarIOs				= 0;
volatile uint8_t					flag_ReadWriteIOs					= 0;
volatile uint8_t					flag_TestOutputs					= 0;
volatile uint8_t					flag_ServidorTLS					= 0;
volatile uint8_t					flag_ServidorHTTP					= 0;
volatile uint8_t					flag_GravarConfig					= 0;
volatile uint8_t					flag_Reiniciar						= 0;
volatile uint8_t					flag_ReceivingHTTP				= 0;
volatile uint8_t					flag_RequisitarGravacao		= 0;
volatile uint8_t					flag_AfterSave						= 0;
volatile uint8_t					flag_IniciarZerocross			= 0;
volatile uint8_t					flag_Eventos							= 0;
volatile uint8_t					flag_OTA									= 0;
volatile uint8_t					flag_BOOT									= 0;
volatile uint8_t					flag_ModoInicializacao		= 0;
volatile uint16_t					flag_ConnectFail 					= 0;
volatile char *						flag_OTA_SHA							= "0123456789012345678901234567890123456789012345678901234567890123";
volatile char *						flag_OTA_FILE							= "0123456789012345678901234567890123456789012345678901234567890123";
volatile char *						flag_OTA_SERVER						= "0123456789012345678901234567890123456789012345678901234567890123";


volatile bool					bFlag_Seguranca					= false;


/********************************************************************************************************/
/*                                           DECLARAR INTERFACES                                        */
/********************************************************************************************************/
static void zerocross_handler(uint8_t gpio_num);
static const char *getMCADD(void);
static void showMessage(const char *sTask, const char *sFunc, const char *sTipo, const char *sMessage);
static void showMemoryRAMStatus();
static void setDevice(device_t *DV, char *sNome, int iEntsai, bool invert);
static float getVPP();
static float getACS712();


//static void vApplicationIdleHook( void );
static void taskRecuperarConfig(void *arg);
static void taskCreateWiFi(void *arg);
static void taskInicializarIOs(void *arg);
static void taskReadWriteIOs(void *arg);
static void taskTestOutputs(void *arg);
static void taskServidorTLS(void *arg);
static void taskServidorHTTP(void *arg);
static void taskFactory(void *arg);
static void taskCheckCloudTLS(void *arg);
static void taskCheckCloud(void *arg);
static void taskGravarConfig(void *arg);
static void taskReiniciar(void *arg);
static void taskEventos(void *arg);
//static void taskFormatSpiffs(void *arg);
static void taskSentinela(void *arg);






/********************************************************************************************************/
/*                                       INCLUDES DE BIBLIOTECAS PRIVADAS                               */
/********************************************************************************************************/
#include "ssid_config.h"
#include "include/globalAccess.h"
#include "include/wisehome_utils.h"
#include "include/stringLib.h"
#include "include/utils_crypto.h"
#include "include/mapeamento.h"
#include "include/armazenamento.h"
#include "include/acs712.h"
//#include "include/irremote.c"
#include "exec/exec_utils.h"

//#include "exec/ota.h"

//UPNP e ALEXA
// #include "include/upnp.h"  //ok
// #include "include/upnp.c"  //ok
// #include "include/lwipopts.h" //ok
// #include "include/httpd_alexa.h" //ok
// #include "include/httpd_alexa.c" //ok




/********************************************************************************************************/
/*                                           INCLUDES DAS TASKS                                        */
/********************************************************************************************************/
#include "tasks/taskRecuperarConfig.h"
#include "tasks/taskCreateWiFi.h"
#include "tasks/taskInicializarIOs.h"
#include "tasks/taskReadWriteIOs.h"
#include "tasks/taskTestOutputs.h"
#include "tasks/taskServidorTLS.h"
#include "tasks/taskServidorHTTP.h"
#include "tasks/taskFactory.h"
#include "tasks/taskCheckCloudTLS.h"
#include "tasks/taskCheckCloud.h"
#include "tasks/taskGravarConfig.h"
#include "tasks/taskReiniciar.h"
#include "tasks/taskEventos.h"
//#include "tasks/taskRefreshOutput.h"
//#include "tasks/taskSetup.h"
//#include "tasks/taskFormatSpiffs.h"
#include "tasks/taskSentinela.h"
// #include "tasks/taskUpnp.h"
#include "tasks/taskOTA.h"





/********************************************************************************************************/
/*                                     FUNCAO DE LIBERACAO DE MEMORIA                                   */
/********************************************************************************************************/
void vApplicationIdleHook(void)
{
    //printf("Passei no IDLE\n");


}


/********************************************************************************************************/
/*                                           FUNCAO O ZEROCROSS                                         */
/********************************************************************************************************/
static void zerocross_handler(uint8_t gpio_num)
{
    //Codificar disparos do timer aqui
	printf("Zerocross...\n");

}


void user_init(void)
{
	//Inicializar algumas variáveis
	arrayConsumo = (char *)malloc(1024);
	arrayConsumoBuffer = (char *)malloc(10);

	//Configurar CPU
	sdk_system_update_cpu_freq(160);
	uart_set_baud(0, 115200);


	//Configurações iniciais de rede
	sdk_wifi_set_opmode(STATION_MODE);
	sdk_wifi_station_set_auto_connect(false);
	struct sdk_station_config config = {
		.ssid 		= WIFI_SSID,
		.password 	= WIFI_PASS
	};
	sdk_wifi_station_set_config(&config);


	//Criar os Semáforos
	xSemaforo_dataBus				= xSemaphoreCreateMutex();
	xSemaforo_spiffs				= xSemaphoreCreateMutex();
	xSemaforo_taskReiniciar			= xSemaphoreCreateBinary();

	//Criar Queue
	//queue_taskControl = xQueueCreate(10, 128);
	//queue_execControl = xQueueCreate(20, 128);
	//ota_tftp_init_server(TFTP_PORT);

	printf("############## VICTORY FIRMWARE ===> GOD RULZ!! ###################\n");

	rboot_config conf = rboot_get_config();
	printf("\r\n\r\nOTA Basic demo.\r\nCurrently running on flash slot %d / %d.\r\n\r\n",
				 conf.current_rom, conf.count);

	printf("Image addresses in flash:\r\n");
	for(int i = 0; i <conf.count; i++) {
			printf("%c%d: offset 0x%08x\r\n", i == conf.current_rom ? '*':' ', i, conf.roms[i]);
	}
	//Inicializar tarefa principal
	xTaskCreate(&taskSentinela      , "taskSentinela"      , 384 , NULL, 5, &taskSentinela_hdl);		//256




}

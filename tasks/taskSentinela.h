/* *******************************************************************************************************

 TASK		: taskSentinela
 OBJETIVO	: Tarefa responsavel por checar o funcionamento do dispositivo

*********************************************************************************************************/
void taskSentinela(void *arg)
{

	printf("\n\n\n****************************************************\n\n");
	printf(" TASK        : taskSentinela\n");
	printf(" OBJETIVO    : Tarefa responsavel por checar o funcionamento do dispositivo\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");

	char queueContent[128];
	struct memoryreg_t localDevice;


	uint8_t localFlag_RecuperarConfig		= 0;
	uint8_t localFlag_CreateWiFi				= 0;
	uint8_t localFlag_InicializarIOs		= 0;
	uint8_t localFlag_ReadWriteIOs			= 0;
	uint8_t localFlag_TestOutputs				= 0;
	uint8_t localFlag_ServidorTLS				= 0;
	uint8_t localFlag_ServidorHTTP			= 0;
	uint8_t localFlag_Factory						= 0;
	uint8_t localFlag_CheckCloud				= 0;
	uint8_t localFlag_GravarConfig			= 0;
	uint8_t localFlag_Reiniciar					= 0;
	uint8_t localFlag_ReceivingHTTP			= 0;
	uint8_t localFlag_AfterSave					= 0;
	uint8_t localFlag_IniciarZerocross	= 0;
	uint8_t localFlag_Eventos						= 0;
	uint8_t localFlag_OTA								= 0;
	uint16_t localFlag_ConnectFail				= 0;

	bool    localFlag_Seguranca					= false;

	//Verificar BOOT
	if (flag_BOOT==0)
	{
		//Ler a CONFIG
		xTaskCreate(&taskRecuperarConfig, "taskRecuperarConfig", 2048, NULL, 2, &taskRecuperarConfig_hdl);
		while(localFlag_RecuperarConfig!=2)
		{
			printf("lendo config...\n");
			get_Flag_RecuperarConfig(&localFlag_RecuperarConfig);
			wait(10);
		}

		get_DeviceMemo(&localDevice);

		//Checar modo Factory => 0
		if (strcmp(localDevice.GUID, "")==0)
		{
			xTaskCreate(&taskCreateWiFi, "taskCreateWiFi", 1024, NULL, 2, &taskCreateWiFi_hdl);
			while(localFlag_CreateWiFi!=2)
			{
				printf("aguardando wifi...\n");
				get_Flag_CreateWiFi(&localFlag_CreateWiFi);
				wait(10);
			}

			xTaskCreate(&taskFactory, "taskFactory", 4096, NULL, 2, &taskFactory_hdl);
			for(;;)
			{
				wait(10);
			}
		}

		flag_BOOT = 1;
	}

	//Loop funcional
	for (;;)
	{

		checkconnection();
		//wait(1); tirado em 23/10
		get_DeviceMemo(&localDevice);
		get_Flag_RecuperarConfig(&localFlag_RecuperarConfig	);
		get_Flag_CreateWiFi(&localFlag_CreateWiFi);
		get_Flag_InicializarIOs(&localFlag_InicializarIOs);
		get_Flag_ReadWriteIOs(&localFlag_ReadWriteIOs);
		get_Flag_TestOutputs(&localFlag_TestOutputs);
		get_Flag_ServidorTLS(&localFlag_ServidorTLS);
		get_Flag_ServidorHTTP(&localFlag_ServidorHTTP);
		get_Flag_Factory(&localFlag_Factory);
		get_Flag_CheckCloud(&localFlag_CheckCloud);
		get_Flag_GravarConfig(&localFlag_GravarConfig);
		get_Flag_Reiniciar(&localFlag_Reiniciar);
		get_Flag_ReceivingHTTP(&localFlag_ReceivingHTTP);
		get_Flag_Seguranca(&localFlag_Seguranca);
		get_Flag_AfterSave(&localFlag_AfterSave);
		get_Flag_IniciarZerocross(&localFlag_IniciarZerocross);
		get_Flag_Eventos(&localFlag_Eventos);
		get_Flag_ConnectFail(&localFlag_ConnectFail);
		get_Flag_OTA(&localFlag_OTA);



	 	//printf("localFlag_CreateWiFi= %d\n", localFlag_CreateWiFi);

		//Inicializar WiFi
		if ((localFlag_CreateWiFi==0)||(localFlag_CreateWiFi==5))
		{
			printf("==== taskCreateWiFi ====\n");
			set_Flag_CreateWiFi(1);

			if (localFlag_CreateWiFi==5) {
							if (localFlag_ServidorHTTP==2) {
									printf("==== Apagando tarefa  taskServidorHTTP_hdl====\n");
									vTaskDelete(taskServidorHTTP_hdl);
									printf("====  tarefa  taskServidorHTTP_hdl Apagada ====\n");
									set_Flag_ServidorHTTP(0);
									wait(1000);
							}
							if (localFlag_CheckCloud==2) {
									printf("==== Apagando tarefa  taskCheckCloud_hdl ====\n");
									vTaskDelete(taskCheckCloud_hdl);
									printf("==== tarefa  taskCheckCloud_hdl apagada====\n");
									wait(10);
									// do {
									// 	wait(10);
									// 	get_Flag_ReceivingHTTP(&localFlag_ReceivingHTTP);
									// 	printf("esperando liberar Flag_ReceivingHTTP=0 que esta Flag= %d \n", localFlag_ReceivingHTTP);
									// } while(localFlag_ReceivingHTTP==1);
									// printf("==== Apagando tarefa taskhttp_server_alexa_hdl ====\n");
									// vTaskDelete(taskhttp_server_alexa_hdl);
									// printf("==== tarefa taskhttp_server_alexa_hdl apagada ====\n");
									// wait(10);
									// printf("==== Apagando tarefa taskmcast_hdl ====\n");
									// //udp_remove();
									// vTaskDelete(taskmcast_hdl);
									// printf("==== tarefa taskmcast_hdl apagada ====\n");
									// //wait(10);
									set_Flag_CheckCloud(0);
									wait(1000);

							}
			}
			printf("==== Criando tarefa WiFi ====\n");
			xTaskCreate(&taskCreateWiFi, "taskCreateWiFi", 1024, NULL, 2, &taskCreateWiFi_hdl);
			while(localFlag_CreateWiFi!=2)
			{
				printf("aguardando wifi...\n");
				get_Flag_CreateWiFi(&localFlag_CreateWiFi);
				wait(10);
			}
		}

		//
		// printf("localFlag_CreateWiFi= %d\n", localFlag_CreateWiFi);
		//
		// printf("localFlag_ServidorHTTP= %d\n", localFlag_ServidorHTTP);
		// printf("localFlag_AfterSave= %d\n", localFlag_AfterSave);
		// printf("localFlag_Reiniciar= %d\n", localFlag_Reiniciar);
		//
		// printf("localDevice.device_state= %d\n", localDevice.device_state);
		//printf("localFlag_Seguranca=  %s\n", localFlag_Seguranca ? "true" : "false");



		//Inicializar Servidor HTTP
		if ((localFlag_ServidorHTTP==0)&&
			(localFlag_AfterSave==0)&&
			(localFlag_Reiniciar!=1)&&
			(
				(localDevice.device_state != '2') ||		//Em configuracao
				((localDevice.device_state == '2')&&(localFlag_Seguranca))
			)
		   )
		{
			printf("==== taskServidorHTTP ====\n");
			set_Flag_ServidorHTTP(1);
			xTaskCreate(&taskServidorHTTP, "taskServidorHTTP", 4096, NULL, 2, &taskServidorHTTP_hdl);

			while(localFlag_ServidorHTTP!=2)
			{
				printf("aguardando Servidor HTTP...\n");
				get_Flag_ServidorHTTP(&localFlag_ServidorHTTP);
				wait(10);
			}
		}

		// printf("localFlag_ServidorHTTP= %d\n", localFlag_ServidorHTTP);
		// printf("localFlag_AfterSave= %d\n", localFlag_AfterSave);
		// printf("localFlag_Reiniciar= %d\n", localFlag_Reiniciar);
		//
		// printf("localDevice.device_state= %d\n", localDevice.device_state);
		// printf("localFlag_Seguranca=%s\n", localFlag_Seguranca ? "true" : "false");
		//
		// printf("localFlag_InicializarIOs= %d\n", localFlag_InicializarIOs);

		//Inicializar IOs
		if ((localFlag_InicializarIOs==0)&&
			(localDevice.device_state == '2')&&
			(localFlag_AfterSave==0)&&
			(localFlag_Reiniciar!=1))
		{
			printf("==== taskInicializarIOs ====\n");
			set_Flag_InicializarIOs(1);
			xTaskCreate(&taskInicializarIOs, "taskInicializarIOs", 256, NULL, 2, &taskInicializarIOs_hdl);

			while(localFlag_InicializarIOs!=2)
			{
				printf("aguardando InicializarIOs...\n");
				get_Flag_InicializarIOs(&localFlag_InicializarIOs);
				wait(10);
			}
		}

		if (localFlag_OTA==5)
		{
			printf("==== taskOTA ====\n");
			if (localFlag_ServidorHTTP==2) {
					printf("==== Apagando tarefa  taskServidorHTTP_hdl====\n");
					vTaskDelete(taskServidorHTTP_hdl);
					printf("====  tarefa  taskServidorHTTP_hdl Apagada ====\n");
					set_Flag_ServidorHTTP(0);
					wait(1000);
			}
			if (localFlag_CheckCloud==2) {
					printf("==== Apagando tarefa  taskCheckCloud_hdl ====\n");
					vTaskDelete(taskCheckCloud_hdl);
					printf("==== tarefa  taskCheckCloud_hdl apagada====\n");
					wait(10);
					// do {
					// 	wait(10);
					// 	get_Flag_ReceivingHTTP(&localFlag_ReceivingHTTP);
					// 	printf("esperando liberar Flag_ReceivingHTTP=0 que esta Flag= %d \n", localFlag_ReceivingHTTP);
					// } while(localFlag_ReceivingHTTP==1);
					// printf("==== Apagando tarefa taskhttp_server_alexa_hdl ====\n");
					// vTaskDelete(taskhttp_server_alexa_hdl);
					// printf("==== tarefa taskhttp_server_alexa_hdl apagada ====\n");
					// wait(10);
					// printf("==== Apagando tarefa taskmcast_hdl ====\n");
					// //udp_remove();
					// vTaskDelete(taskmcast_hdl);
					// printf("==== tarefa taskmcast_hdl apagada ====\n");
					// //wait(10);
					set_Flag_CheckCloud(0);
					wait(1000);

			}

			//set_Flag_OTA(6);
			//ota_tftp_init_server(TFTP_PORT);
			//xTaskCreate(&tftp_client_task, "tftp_client", 2048, NULL, 2, &tftp_client_task_hdl);

			while(localFlag_OTA!=7)
			{
				printf("aguardando OTA...\n");
				//get_Flag_OTA(&localFlag_OTA);
				wait(10);
			}
		}


		// printf("localFlag_ServidorHTTP= %d\n", localFlag_ServidorHTTP);
		// printf("localFlag_AfterSave= %d\n", localFlag_AfterSave);
		// printf("localFlag_Reiniciar= %d\n", localFlag_Reiniciar);
		//
		// printf("localDevice.device_state= %d\n", localDevice.device_state);
		// printf("localFlag_Seguranca=%s\n", localFlag_Seguranca ? "true" : "false");
		//
		// printf("localFlag_InicializarIOs= %d\n", localFlag_InicializarIOs);
		// printf("localFlag_ReadWriteIOs= %d\n", localFlag_ReadWriteIOs);

		//ReadWrite IOs
		if ((localFlag_InicializarIOs==2)&&
			(localFlag_ReadWriteIOs==0)&&
			(localFlag_AfterSave==0))
		{
			printf("==== taskReadWriteIOs ====\n");

			set_Flag_ReadWriteIOs(1);
			xTaskCreate(&taskReadWriteIOs, "taskReadWriteIOs", 256, NULL, 2, &taskReadWriteIOs_hdl);

			while(localFlag_ReadWriteIOs!=2)
			{
				printf("aguardando ReadWriteIOs...\n");
				get_Flag_ReadWriteIOs(&localFlag_ReadWriteIOs);
				wait(10);
			}

		}

		// printf("\n\n\n\n\n\n\n\nANTES DE CHECAR CLOUD \n");
		// printf("localFlag_CheckCloud= %d\n", localFlag_CheckCloud);
		// printf("localDevice.device_state= %d\n", localDevice.device_state);
		// printf("localFlag_AfterSave= %d\n", localFlag_AfterSave);
		// printf("localFlag_Reiniciar= %d\n", localFlag_Reiniciar);
		// printf("localFlag_Seguranca=%s\n", localFlag_Seguranca ? "true" : "false");
		//
		// printf("localFlag_ServidorHTTP= %d\n", localFlag_ServidorHTTP);
		// printf("localFlag_InicializarIOs= %d\n", localFlag_InicializarIOs);
		// printf("localFlag_ReadWriteIOs= %d\n", localFlag_ReadWriteIOs);
		//
		// printf("(localFlag_Seguranca) %d\n", localFlag_Seguranca);
		//Acesso a Cloud
		if ((localFlag_CheckCloud==0)&&
			(localDevice.device_state == '2')&&
			(localFlag_AfterSave==0)&&
			(localFlag_Reiniciar!=1)&&
			(!localFlag_Seguranca)
		)
		{
			printf("\n==== taskCheckCloud ====\n");
			wait(10);
			set_Flag_CheckCloud(1);
			xTaskCreate(&taskCheckCloud, "taskCheckCloud", 4096, NULL, 2, &taskCheckCloud_hdl);

			while(localFlag_CheckCloud!=2)
			{
				printf("\naguardando CheckCloud...\n");
				get_Flag_CheckCloud(&localFlag_CheckCloud);
				wait(10);
			}
			// xTaskCreate(&alexa_httpd_task, "alexa_httpd_task", 1024, NULL, 2,  &taskhttp_server_alexa_hdl);
			// printf("\naguardando alexa_httpd_task...\n");
			// wait(10);
	    // xTaskCreate(&mcast_task, "mcast_task", 1024, NULL, 2, taskmcast_hdl);
			// printf("\naguardando mcast_task...\n");
			// wait(10);
			// // cria tarefa resposta httpd
			//testarmos
			// criar xxxxxpto


		}




		// printf("localFlag_GravarConfig era pra ser 3 ==> %d\n", localFlag_GravarConfig);
		// printf("localFlag_ServidorHTTP era pra ser 3 ==> %d\n", localFlag_ServidorHTTP);
		// printf("localFlag_CheckCloud era pra ser 0 ==> %d\n", localFlag_CheckCloud);
		// printf("localFlag_AfterSave era pra ser 1 ==> %d\n", localFlag_AfterSave);
		// printf("localFlag_Reiniciar era pra ser 1 ==> %d\n", localFlag_Reiniciar);





		// gravar config
		if ((localFlag_GravarConfig==3)&&
				((localFlag_ServidorHTTP==0)||(localFlag_ServidorHTTP==3))&&
				(localFlag_CheckCloud==0)//&&
				//(localFlag_AfterSave==1)
				)
				{
					printf("==== taskGravarConfig ====\n");
					set_Flag_GravarConfig(1);
					xTaskCreate(&taskGravarConfig, "taskGravarConfig", 3072, NULL, 2, &taskGravarConfig_hdl);
				}


		 // printf("localFlag_Reiniciar era pra ser 1 ==> %d\n", localFlag_Reiniciar);
		 // printf("localFlag_AfterSave era pra ser 0 ==> %d\n", localFlag_AfterSave);
		 //  printf("localFlag_GravarConfig era pra ser 2 ==> %d\n", localFlag_GravarConfig);
			//  printf("localFlag_GravarConfig era pra ser != 3 ==> %d\n", localFlag_GravarConfig);
			//  printf("localFlag_ReceivingHTTP ou =0 %d\n", localFlag_ReceivingHTTP);





		//Reiniciar
		if ((localFlag_Reiniciar==1)&&
			(localFlag_AfterSave==0)&&
			(localFlag_GravarConfig!=3)&&
			((localFlag_ReceivingHTTP==0)||(localFlag_GravarConfig==2))
		  )
		{
			set_Flag_Reiniciar(0);
			xTaskCreate(&taskReiniciar, "taskReiniciar", 256, NULL, 2, &taskReiniciar_hdl);

			//showMemoryRAMStatus();
		}



		wait(100);
	}

	printf(" taskSentinela -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	vTaskDelete(taskSentinela_hdl);

}

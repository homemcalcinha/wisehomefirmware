/* *******************************************************************************************************

 TASK		: taskGravarConfig
 OBJETIVO	: Serializa e armazena os dados de configuracao

*********************************************************************************************************/
#include <string.h>

static void taskGravarConfig(void *arg)
{
	printf("\n\n\n****************************************************\n\n");
	printf(" FUNCTION    : taskGravarConfig\n");
	printf(" OBJETIVO    : Serializa e armazena os dados de configuracao\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");


printf("linha 1\n");
	struct memoryreg_t localDevice;
printf("linha 2\n");
	get_DeviceMemo(&localDevice);

printf("linha 3\n");
	char *sJson; // 				= (char *)malloc(1500);
printf("linha 4\n");
	char *sJsonCrypt 			= (char *)malloc(1025);

printf("linha 5\n");
	unsigned char *oGlobalKey 	= (unsigned char *)malloc(33);
printf("linha 6\n");
	unsigned char *oGlobalIV 	= (unsigned char *)malloc(17);

printf("linha 7\n");
	get_globalKey(&oGlobalKey);
printf("linha 8\n");
	get_globalIV(&oGlobalIV);
printf("linha 9\n");

	//bzero(sJson, 1499);
printf("linha 10\n");
	//bzero(sJsonCrypt, 1499);
printf("linha 11\n");
	serializar_DeviceMemo(&sJson);

printf("linha 12\n");

	int iSize = vEncrypt(&sJsonCrypt, sJson, globalKey, globalIV);

	sJsonCrypt[iSize] = 0;
	//sJsonCrypt[iSize+1] = 0;
printf("linha 13\n");
	sJsonCrypt = append(sJsonCrypt, "|");

	unsigned char *kbrax 	= (unsigned char *)malloc(strlen(sJsonCrypt)+1);

	kbrax 	= (char*)left(sJsonCrypt,strlen(sJsonCrypt));

	printf("kbrax: %s#############\n", kbrax);





printf("linha 14\n");


	printf("taskGravarConfig DEPOIS [%d]: %s\n", strlen(sJsonCrypt), sJsonCrypt);

	if( xSemaforo_spiffs != NULL )
	{
		if( xSemaphoreTake( xSemaforo_spiffs, portMAX_DELAY) )
		{
			esp_spiffs_init();
			printf("linha 15 - checar o erro\n");
			showMemoryRAMStatus();

			if (esp_spiffs_mount() != SPIFFS_OK) {
				printf("linha 16\n");
				showMessage("taskGravarConfig", "Montar SPIFFS", "ERRO", "Falha ao montar a SPIFFS");
			}

printf("linha 17\n");
			int fd = open("config.cfg", O_WRONLY|O_CREAT, 0);
printf("linha 18\n");
			if (fd < 0) {
				printf("linha 19\n");
				showMessage("taskGravarConfig", "Criar config.cfg", "ERRO", "Falha ao criar o arquivo config.cfg");
			}
			else
			{
				//write(fd, sJsonCrypt, strlen(sJsonCrypt));
printf("linha 20\n");
				//SPIFFS_write(&fs, (spiffs_file)fd, (char*)left(sJsonCrypt, iSize+5), iSize+5);
				SPIFFS_write(&fs, (spiffs_file)fd, kbrax, strlen(kbrax));

printf("linha 21\n");
			/*
				SPIFFS_write(&fs, (spiffs_file)fd, (char*)"|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ", 1000);
			*/

printf("linha 22\n");

			}
printf("linha 23\n");
			close(fd);
printf("linha 24\n");
			SPIFFS_unmount(&fs);
printf("linha 25\n");
			esp_spiffs_deinit();


			xSemaphoreGive( xSemaforo_spiffs);
		}
	}

	printf(" TERMINO EM : %d ms\n", xTaskGetTickCount());



taskGravarConfig_exit:

printf("linha 26\n");
	free(sJson);
printf("linha 27\n");
	free(sJsonCrypt);
printf("linha 28\n");
	free(oGlobalKey);
printf("linha 29\n");
	free(oGlobalIV);
printf("linha 30\n");

	set_Flag_GravarConfig(2);
	set_Flag_Reiniciar(1);
	set_Flag_AfterSave(0);
	set_Flag_ReceivingHTTP(0);

	

	vTaskDelete(taskGravarConfig_hdl);
}

/* *******************************************************************************************************
 
 TASK		: taskInicializarIOs
 OBJETIVO	: Tarefa responsavel inicializar as entradas e saidas do dispositivo
 
*********************************************************************************************************/
static void taskInicializarIOs(void *arg)
{
	
	//for (;;)
	//{
	//	if( xSemaforo_taskInicializarIOs != NULL )
	//	{
	//		if( xSemaphoreTake( xSemaforo_taskInicializarIOs, portMAX_DELAY) )
	//		{

				printf("\n\n\n****************************************************\n\n");
				printf(" TASK        : taskInicializarIOs\n");
				printf(" OBJETIVO    : Tarefa responsavel inicializar as entradas e saidas do dispositivo\n\n");
				printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
				printf("***************************************************\n\n");
				
				if( xSemaforo_dataBus != NULL )
				{
					if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
					{
						gpio_enable(pinSDClock, GPIO_OUTPUT); 							//Clock utilizado pelo SN74HC595 e SN74HC165
						gpio_enable(pinSDLatch, GPIO_OUTPUT); 							//LATCH utilizado pelo SN74HC595 e LOAD utilizado pelo SN74HC165
						gpio_enable(pinEDData, GPIO_INPUT);   							//Serial de dados de leitura do SN74HC165
						gpio_enable(pinSDData, GPIO_OUTPUT);  							//Serial de dados de escrita do SN74HC595
						gpio_enable(pinTriac1, GPIO_OUTPUT);  							//Triac 1
						gpio_enable(pinTriac2, GPIO_OUTPUT);  							//Triac 2
						gpio_enable(pinTriac2, GPIO_OUTPUT); 							//Triac 2
						gpio_enable(pinIR, GPIO_OUTPUT);
						gpio_enable(interruptPin, GPIO_OUTPUT);
						
						xSemaphoreGive( xSemaforo_dataBus);
					}
				}
				
				setDevice(&SD1, "RELE 1", 1, true);									//SD1 - RELE 1
				setDevice(&SD2, "RELE 2", 1, true);									//SD2 - RELE 2
				setDevice(&SD3, "RELE 3", 1, true);									//SD3 - RELE 3
				setDevice(&SD4, "SD4-LIVRE", 1, true);								//SD4 - LIVRE
				setDevice(&SD5, "SD5-LIVRE", 1, true);								//SD5 - LIVRE
				setDevice(&SD6, "SD6-S1", 1, true);									//SD6 - ENDEREÇAMENTO DAS ENTRADAS (S1)
				setDevice(&SD7, "SD7-S3", 1, true);									//SD7 - ENDEREÇAMENTO DAS ENTRADAS (S3)
				setDevice(&SD8, "SD8-S2", 1, true);									//SD8 - ENDEREÇAMENTO DAS ENTRADAS (S2)
				
				setDevice(&ED1, "ED-1", 1, false);									//ED1 - RELE 1 - A
				setDevice(&ED2, "ED-2", 1, false);									//ED2 - RELE 2 - C
				setDevice(&ED3, "ED-3", 1, false);									//ED3 - RELE 3 - B
				setDevice(&ED4, "ED-4", 1, false);									//ED4 - TRIAC 1 - E
				setDevice(&ED5, "ED-5", 1, false);									//ED5 - TRIAC 2 - D
				setDevice(&ED6, "ED-6", 1, false);									//ED6 - E1 - F
				setDevice(&ED7, "ED-7", 1, false);									//ED7 - E2 - G
				setDevice(&ED8, "ED-8", 1, false);									//ED8 - E3 - H
				
				setDevice(&EA1, "EA-1", 1, false);									//EA1 - Leitura 1 - A0 [SD6=0 / SD8=0 / SD7=0] - Triac 1
				setDevice(&EA2, "EA-2", 1, false);									//EA2 - Leitura 2 - A1 [SD6=0 / SD8=0 / SD7=1] - Porta Analógica disponivel
				setDevice(&EA3, "EA-3", 1, false);									//EA3 - Leitura 3 - A7 [SD6=1 / SD8=1 / SD7=1] - Triac 2
				setDevice(&EA4, "EA-4", 1, false);									//EA4 - Leitura 4 - A3 [SD6=0 / SD8=1 / SD7=1] - Rele 1
				setDevice(&EA5, "EA-5", 1, false);									//EA5 - Leitura 5 - A4 [SD6=1 / SD8=0 / SD7=0] - Rele 3
				setDevice(&EA6, "EA-6", 1, false);									//EA6 - Leitura 6 - A5 [SD6=1 / SD8=0 / SD7=1] - Rele 2
				setDevice(&EA7, "EA-7", 1, false);									//EA7 - Leitura 7 - A2 [SD6=0 / SD8=1 / SD7=0] - Sensor Temperatura 1
				setDevice(&EA8, "EA-8", 1, false);									//EA8 - Leitura 8 - A6 [SD6=1 / SD8=1 / SD7=0] - Sensor Temperatura 2
				
	//			xTaskCreate(&taskReadWriteIOs, "taskReadWriteIOs", 256, NULL, 2, &taskReadWriteIOs_hdl);		//1024
	//			
	//			ExecutarTask("taskTestOutputs=>ON");
	//			goto taskInicializarIOs_exit;
	//		}
	//		
	//	}
	//	
	//	wait(100);
	//}

//taskInicializarIOs_exit:
	
	//taskInicializarIOs_hdl = NULL;
	printf(" taskInicializarIOs -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	set_Flag_InicializarIOs(2);
	vTaskDelete(taskInicializarIOs_hdl);
	
	
	
}


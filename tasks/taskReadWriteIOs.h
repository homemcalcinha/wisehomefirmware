/* *******************************************************************************************************

 TASK		: taskReadWriteIOs
 OBJETIVO	: Tarefa responsavel por atualizar as entradas e saidas do dispositivo

*********************************************************************************************************/
static void taskReadWriteIOs(void *arg)
{

	for (;;)
	{
		if( xSemaforo_dataBus != NULL )
		{
			if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
			{
				//printf("antes de escrever digital  %d\n", xTaskGetTickCount());

				writeDigitalOutput();
				//printf("apos  de escrever digital  %d\n", xTaskGetTickCount());

				//Executar leitura quando o ponteiro de entrada analogica for alterado
				if (iAnalogPointer != iAnalogPointer_ant)
				{
					//printf("comecou em  %d\n", xTaskGetTickCount());
					iAnalogPointer_ant = iAnalogPointer;
					getACS712();
					printf("Lendo EA %d\n", iAnalogPointer);
				}


				//refreshAnalogs();		//Executa a leitura em todas as portas analogicas (Sem uso atualmente)

				//Eventos
				//printf("comecou dispathEvents em  %d\n", xTaskGetTickCount());
				dispathEvents();
				//printf("terminou dispathEvents em  %d\n", xTaskGetTickCount());
				executarEventos();
				//printf("terminou executarEventos em  %d\n", xTaskGetTickCount());


				flag_ReadWriteIOs = 2;

				xSemaphoreGive( xSemaforo_dataBus);
			}
		}
		wait(100); //mudada em 23/10
	}

	//taskReadWriteIOs_hdl = NULL;
	printf(" taskReadWriteIOs -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	vTaskDelete(taskReadWriteIOs_hdl);


}

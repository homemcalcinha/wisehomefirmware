/* *******************************************************************************************************
 
 TASK		: taskReiniciar
 OBJETIVO	: Desmonta o dispositivo para ser reiniciado
 
*********************************************************************************************************/
static void taskReiniciar(void *arg)
{
	
	//for(;;)
	//{
	//	if( xSemaforo_taskReiniciar != NULL )
	//	{
	//		if( xSemaphoreTake( xSemaforo_taskReiniciar, portMAX_DELAY) )
	//		{
				printf("\n\n\n****************************************************\n\n");
				printf(" TASK        : taskReiniciar\n");
				printf(" OBJETIVO    : Desmonta o dispositivo para ser reiniciado\n\n");
				printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
				printf("***************************************************\n\n");
				
	
	
				printf(" taskReiniciar -> TERMINO EM : %d ms\n", xTaskGetTickCount());

				set_Flag_RecuperarConfig(0);
				
				sdk_system_restart();
	//		}
	//	}
	//	wait(10);
	//}
				
	vTaskDelete(taskReiniciar_hdl);
	
}


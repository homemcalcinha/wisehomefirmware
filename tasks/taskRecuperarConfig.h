/* *******************************************************************************************************
 
 TASK		: taskRecuperarConfig
 OBJETIVO	: Recupera dados de configuracao da SPIFFS
 
*********************************************************************************************************/
static void taskRecuperarConfig(void *arg)
{
	bool bFlag = false;
	const int buf_size = 1024;
	uint8_t buf[buf_size];
	struct memoryreg_t localDevice;
	unsigned char *oGlobalKey = (unsigned char *)malloc(32);
	unsigned char *oGlobalIV = (unsigned char *)malloc(16);
	
	
	//for(;;)
	//{
		//if( xSemaforo_taskRecuperarConfig != NULL )
		//{
		//	if( xSemaphoreTake( xSemaforo_taskRecuperarConfig, portMAX_DELAY) )
		//	{
				printf("\n\n\n****************************************************\n\n");
				printf(" TASK        : taskRecuperarConfig\n");
				printf(" OBJETIVO    : Recupera dados de configuracao da SPIFFS\n\n");
				printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
				printf("***************************************************\n\n");
				
				//Armazenar os códigos de Chip ID e Flash ID
				localDevice.flash_id = sdk_spi_flash_get_id();
				localDevice.chip_id = sdk_system_get_chip_id();
				
				
				get_globalKey(&oGlobalKey);
				get_globalIV(&oGlobalIV);
					
				
	if( xSemaforo_spiffs != NULL )
	{
		if( xSemaphoreTake( xSemaforo_spiffs, portMAX_DELAY) )
		{
				esp_spiffs_init();
				wait(50);
				if (esp_spiffs_mount() != SPIFFS_OK) {
					showMessage("taskRecuperarConfig", "Montar SPIFFS", "ERRO", "Falha ao montar a SPIFFS");
				}
				wait(50);
				
				int fd = open("config.cfg", O_RDONLY);
				if (fd < 0) {
					showMessage("taskRecuperarConfig", "Abrir config.cfg", "ERRO", "Falha ao abrir o arquivo config.cfg");
				}
				else
				{
					bzero(buf, buf_size);
					read(fd, buf, buf_size);
					//printf("DADOS: %s tamanho: %d \n", buf, (strlen(buf)));
					//int iSize = (position(buf, "  "));
					int iSize = (chPosition(buf, '|'));
					
					if (iSize<0) iSize = strlen(buf);
					
					char *sCrypted = (char *)malloc(buf_size);
					sCrypted = left(buf, iSize);
					printf("\n\n\n sCrypted [%d]: %s\n\n\n", iSize, sCrypted);
					
					
					//char *sDecrypted = decrypt(sCrypted, oGlobalKey, oGlobalIV);
					char *sDecrypted = (char *)malloc(buf_size);
					vDecrypt(&sDecrypted, sCrypted, oGlobalKey, oGlobalIV);
					printf("\n\n\n VOLTA: %s\n\n\n", sDecrypted);
					
					if (desserializar_DeviceMemo(&localDevice, sDecrypted))
					{
						//set_DeviceMemo(localDevice);
						//print_DeviceMemo(localDevice);
						bFlag = true;
					}
					
					free(sDecrypted);
					free(sCrypted);
				}
				
				close(fd);	
				SPIFFS_unmount(&fs);  
				

				//goto taskRecuperarConfig_exit;
				
				
		//	}
			xSemaphoreGive(xSemaforo_spiffs);

		}
	}
	
	//printf("RecuperarConfig: bFlag = %d\n", bFlag);
	
	//Preencher variável global
	//if (bFlag)
	//{
		set_DeviceMemo(localDevice);
		print_DeviceMemo(localDevice);
	//}
	
taskRecuperarConfig_exit:
	
	free(oGlobalKey);
	free(oGlobalIV);
	
	
	printf(" taskRecuperarConfig -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	//taskRecuperarConfig_hdl = NULL;
	set_Flag_RecuperarConfig(2);
	vTaskDelete(taskRecuperarConfig_hdl);
	
}


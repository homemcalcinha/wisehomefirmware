/* *******************************************************************************************************

 TASK		: taskCreateWiFi
 OBJETIVO	: Criar Access Poit e conectar-se a rede WiFi configurada

*********************************************************************************************************/
void taskCreateWiFi(void *arg)
{

	char cCh;
	struct memoryreg_t localDevice;


	printf("\n\n\n****************************************************\n\n");
	printf(" TASK        : taskCreateWiFi\n");
	printf(" OBJETIVO    : Criar Access Poit e conectar-se a rede WiFi configurada\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");


	get_DeviceMemo(&localDevice);


	//================ CHECK FACTORY MODE ==================
	if (strcmp(localDevice.GUID, "")==0)
	{
		printf("\n\n**************** MODO FACTORY **********************\n\n");
		print_DeviceMemo(localDevice);

		int wifi_alive = 0;
		uint8_t status = 0;
		uint8_t retries = 30;
		struct ip_info sta_ip;
		IP4_ADDR(&sta_ip.ip, 0, 0, 0, 0);
		IP4_ADDR(&sta_ip.gw, 0, 0, 0, 0);
		IP4_ADDR(&sta_ip.netmask, 0, 0, 0, 0);
		sdk_wifi_set_ip_info(1, &sta_ip);

		sdk_wifi_station_connect();
		status = sdk_wifi_station_get_connect_status();

        while ((status != STATION_GOT_IP) && (retries)) {
            status = sdk_wifi_station_get_connect_status();
            printf("%s: tarefa __func__, status = %d\n\r", __func__, status);
            if (status == STATION_WRONG_PASSWORD) {
                printf("WiFi: wrong password\n\r");
								set_Flag_Seguranca(true);
                break;
            } else if (status == STATION_NO_AP_FOUND) {
                printf("WiFi: AP not found\n\r");
								set_Flag_Seguranca(true);
                break;
            } else if (status == STATION_CONNECT_FAIL) {
                printf("WiFi: connection failed\r\n");
								set_Flag_Seguranca(true);
                break;
            }
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            --retries;
        }

        while ((status = sdk_wifi_station_get_connect_status())
                == STATION_GOT_IP) {
            if (wifi_alive == 0) {
                printf("WiFi: Connected\n\r");
                wifi_alive = 1;

								goto taskCreateWiFi_exit;
            }
            vTaskDelay(500 / portTICK_PERIOD_MS);
        }

		wait(500);

		goto taskCreateWiFi_exit;
	}
	//=======================================================




	struct ip_info ap_ip;

	IP4_ADDR(&ap_ip.ip, 192, 168, 4, 1);
	IP4_ADDR(&ap_ip.gw, 192, 168, 4, 1);
	IP4_ADDR(&ap_ip.netmask, 255, 255, 255, 0);
	sdk_wifi_set_ip_info(1, &ap_ip);




	//================ REDE AP ==================

	//Senha da rede AP
	bzero(localDevice.pass_ap, 50);
	memcpy(localDevice.pass_ap, "senha12345", 10);

	if ((localDevice.device_state == '0')||(localDevice.device_state == '1'))
	{
		if (localDevice.device_state == '0')
		{
			printf("\n\n**************** MODO Device State 0 **********************\n\n");

			bzero(localDevice.ssid_ap, 50);
			sdk_wifi_set_opmode(SOFTAP_MODE);
			sprintf(localDevice.ssid_ap, "Wisehome#%d", sdk_system_get_chip_id());


		}
		else if (localDevice.device_state == '1')
		{
			printf("\n\n**************** MODO Device State 1 **********************\n\n");

			bzero(localDevice.ssid_ap, 50);
			sdk_wifi_set_opmode(SOFTAP_MODE);
			sprintf(localDevice.ssid_ap, "Wisehome$%d", sdk_system_get_chip_id());
		}
		//else
		//{
		//	bzero(localDevice.ssid_ap, 50);
		//	sdk_wifi_set_opmode(STATIONAP_MODE);
		//	sprintf(localDevice.ssid_ap, "Wisehome?%d", sdk_system_get_chip_id());
		//}


		struct sdk_softap_config ap_config_0 = {.ssid 				= ((char *)localDevice.ssid_ap),
												.ssid_hidden 		= 0,
												.channel 			= 3,
												.ssid_len 			= strlen((char *)localDevice.ssid_ap),
												.authmode 			= AUTH_WPA_WPA2_PSK,
												.password 			= ((char *)localDevice.pass_ap),
												.max_connection 	= 3,
												.beacon_interval 	= 100,
											};
		memcpy(ap_config_0.ssid, ((char *)localDevice.ssid_ap), strlen((char *)localDevice.ssid_ap));
		memcpy(ap_config_0.password, ((char *)localDevice.pass_ap), strlen((char *)localDevice.pass_ap));


		sdk_wifi_softap_set_config(&ap_config_0);
		wait(200);

		ip_addr_t first_client_ip;
		IP4_ADDR(&first_client_ip, 192, 168, 4, 2);
		dhcpserver_start(&first_client_ip, 4);
		dhcpserver_set_dns(&ap_ip.ip);
		dhcpserver_set_router(&ap_ip.ip);
	}

	//================ STATION MODE ==================
	if (localDevice.device_state == '2')
	{
		printf("\n\n**************** MODO Device State 2 **********************\n\n");

		sdk_wifi_set_opmode(STATION_MODE);
		struct ip_info sta_ip;
		IP4_ADDR(&sta_ip.ip, 0, 0, 0, 0);
		IP4_ADDR(&sta_ip.gw, 0, 0, 0, 0);
		IP4_ADDR(&sta_ip.netmask, 0, 0, 0, 0);
		sdk_wifi_set_ip_info(1, &sta_ip);

		struct sdk_station_config config = {
			.ssid 		= localDevice.ssid_sta,
			.password 	= localDevice.pass_sta,
		};


		memcpy(config.ssid    , ((char *)localDevice.ssid_sta), (strlen((char *)localDevice.ssid_sta)) );
		memcpy(config.password, ((char *)localDevice.pass_sta), (strlen((char *)localDevice.pass_sta)) );
		sdk_wifi_station_set_auto_connect(false); //adiconado para não se reconectar
		sdk_wifi_station_set_config(&config);
		sdk_wifi_station_connect();


		//Aguardar resultado da rede
		uint8_t status = 0;
		uint8_t retries = 1000;

		while (status != STATION_GOT_IP && retries) {
			status = sdk_wifi_station_get_connect_status();
			if(status == STATION_WRONG_PASSWORD) {
				printf("WiFi: wrong password\n");
				set_Flag_Seguranca(true);
				break;
			} else if(status == STATION_NO_AP_FOUND) {
				printf("WiFi Station: AP not found\n");
				set_Flag_Seguranca(true);
				break;
			} else if(status == STATION_CONNECT_FAIL) {
				printf("WiFi: connection failed\n");
				set_Flag_Seguranca(true);
				break;
			}
			wait(500);
			retries--;
			printf("STATUS: %d  retries:%d\n", status, retries);
		}
		if (status == STATION_GOT_IP) {
			printf("WiFi: connected\n");
			set_Flag_Seguranca(false);
		}
		else
		{
			//REDE DE SEGURANÇA
			IP4_ADDR(&ap_ip.ip, 192, 168, 4, 1);
			IP4_ADDR(&ap_ip.gw, 192, 168, 4, 1);
			IP4_ADDR(&ap_ip.netmask, 255, 255, 255, 0);
			sdk_wifi_set_ip_info(1, &ap_ip);

			bzero(localDevice.ssid_ap, 50);
			sdk_wifi_set_opmode(SOFTAP_MODE);
			sprintf(localDevice.ssid_ap, "Wisehome?%d", sdk_system_get_chip_id());

			bzero(localDevice.pass_ap, 50);
			memcpy(localDevice.pass_ap, "senha12345", 10);

			struct sdk_softap_config ap_config_0 = {.ssid 				= ((char *)localDevice.ssid_ap),
													.ssid_hidden 		= 0,
													.channel 			= 3,
													.ssid_len 			= strlen((char *)localDevice.ssid_ap),
													.authmode 			= AUTH_WPA_WPA2_PSK,
													.password 			= ((char *)localDevice.pass_ap),
													.max_connection 	= 3,
													.beacon_interval 	= 100,
												};
			memcpy(ap_config_0.ssid, ((char *)localDevice.ssid_ap), strlen((char *)localDevice.ssid_ap));
			memcpy(ap_config_0.password, ((char *)localDevice.pass_ap), strlen((char *)localDevice.pass_ap));

			sdk_wifi_station_set_auto_connect(false);
			sdk_wifi_softap_set_config(&ap_config_0);
			wait(200);

			ip_addr_t first_client_ip;
			IP4_ADDR(&first_client_ip, 192, 168, 4, 2);
			dhcpserver_start(&first_client_ip, 4);
			dhcpserver_set_dns(&ap_ip.ip);
			dhcpserver_set_router(&ap_ip.ip);

			set_Flag_Seguranca(true);
		}



	}



	set_DeviceMemo(localDevice);

taskCreateWiFi_exit:


	//taskCreateWiFi_hdl = NULL;
	set_Flag_CreateWiFi(2);
	printf(" taskCreateWiFi -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	vTaskDelete(taskCreateWiFi_hdl);


}

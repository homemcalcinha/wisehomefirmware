  /* *******************************************************************************************************

 TASK		: taskCheckCloud
 OBJETIVO	: Verificar se existe conteudo na Cloud

*********************************************************************************************************/


static void taskCheckCloud(void *pvParameters)
{
	bool    ChecalocalFlag_Seguranca					= false;
	//checkconnection();
	set_Flag_CheckCloud(2);

	struct memoryreg_t localDevice;
	get_DeviceMemo(&localDevice);

	get_Flag_Seguranca(&ChecalocalFlag_Seguranca);

	uint8_t sairdatarefa = 0;

 	struct command_t localCommand;
	uint8_t checaFlag_GravarConfig				= 0;
	printf("\n\n\n****************************************************\n\n");
	printf(" TASK        : taskCheckCloud\n");
	printf(" OBJETIVO    : Verificar se existe conteudo na Cloud \n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");
	//printf("**********Flag Segurança: %B *******************\n\n", ChecalocalFlag_Seguranca);




	   int ret, iJsonUncrypted = 0, len = 0, iPos = 0, i = 0, iVirg = 0;
    int recbytes;
    int sin_size;
    int sta_socket;
	bool bFlagUtil = false;
	bool bSD1 = false;
	bool bSD2 = false;
	bool bSD3 = false;
	uint8_t iPorta = 0;
	uint16_t ifalhasconexao = 0;
	uint8_t iEA = 0;
	uint8_t iEA_ant = 0;
	float dConsumo = 0;

    struct sockaddr_in remote_ip;
    char *buffer1 = (char *)zalloc(1025);
    char *buffer2 = (char *)zalloc(1025);
	char *sRetorno = (char *)zalloc(4);
	char *sTemp = (char *)malloc(32);

	char *oGlobalKey = (char *) zalloc(33);
	char *oGlobalIV = (char *) zalloc(33);

	bzero(oGlobalKey, 33);
	memcpy(oGlobalKey, localDevice.key, 32);
	//checkconnection();


	int idns_fail=0;
	ip_addr_t target_ip;

	if (ChecalocalFlag_Seguranca) {
		printf("Tive que sair da tarefa CheckCloud porque segurança estava ativada L 62\n");
		goto taskCheckCloud_exit;
	}


	taskCheckCloud_issecured:
			get_Flag_Seguranca(&ChecalocalFlag_Seguranca);

	if (ChecalocalFlag_Seguranca) {
			sairdatarefa++;
			if (sairdatarefa>10){
						set_Flag_CheckCloud(0);
						sairdatarefa=0;
						wait(100);
						printf("sai da tarefa - taskCheckCloud_hdl\n");
						vTaskDelete(taskCheckCloud_hdl);
			}
}
else {
		if (sairdatarefa>10){
				set_Flag_CreateWiFi(5);
				wait(100);
				//set_Flag_CheckCloud(0);
				sairdatarefa=0;
				//printf("sai da tarefa - taskCheckCloud_hdl\n");
				while (1) {
						wait(1);
				}
		}
}


	idns_fail=0;
	if 	(((unsigned char)((target_ip.addr & 0x000000ff) >> 0))!=191) {
	do {




				idns_fail++;
				//printf("DNS linha 68 falhas %d aqui\n", idns_fail);
        ret = netconn_gethostbyname(WEB_SERVER_CLOUD, &target_ip);
				if (ret) break;

				wait(3000);
				//printf("DNS linha 73 falhas %d aqui\n", idns_fail);
				//checkconnection();
				get_Flag_Seguranca(&ChecalocalFlag_Seguranca);
				if (idns_fail>3) {
					 //printf("DNS linha 77 falhas %d aqui\n", idns_fail);
					 wait(10);
					 set_Flag_Seguranca(true);
					 wait(10);
					 get_Flag_Seguranca(&ChecalocalFlag_Seguranca);
				}
				if (ChecalocalFlag_Seguranca) {
						printf("Tive que sair da tarefa CheckCloud porque segurança estava ativada\n");
						goto taskCheckCloud_exit;
				}
    } while(ret);

		}

    // printf("get target IP is %d.%d.%d.%d\n", (unsigned char)((target_ip.addr & 0x000000ff) >> 0),
    //                                          (unsigned char)((target_ip.addr & 0x0000ff00) >> 8),
    //                                          (unsigned char)((target_ip.addr & 0x00ff0000) >> 16),
    //                                          (unsigned char)((target_ip.addr & 0xff000000) >> 24));

		if 	(((unsigned char)((target_ip.addr & 0x000000ff) >> 0))==165){

							//printf("tudo 165\n");
							get_Flag_ConnectFail(&ifalhasconexao);
							ifalhasconexao++;
							set_Flag_ConnectFail(ifalhasconexao);
							//set_Flag_CreateWiFi(0);
							wait(1000);
							goto taskCheckCloud_exit;
				}

    while (1) {

		//checkconnection();
		if (sairdatarefa>10){
							//set_Flag_Seguranca(true);
							set_Flag_CreateWiFi(5);
							wait(100);
							//set_Flag_CheckCloud(0);
							sairdatarefa=0;
							//printf("sai da tarefa - taskCheckCloud_hdl\n");
							while (1) {
								wait(1);
							}

				}

		get_Flag_GravarConfig(&checaFlag_GravarConfig);
		get_Flag_Seguranca(&ChecalocalFlag_Seguranca);

		if (ChecalocalFlag_Seguranca) {
			printf("Tive que sair da tarefa CheckCloud porque segurança estava ativada L 87\n");
			goto taskCheckCloud_exit;
		}

        //showMemoryRAMStatus();

		//***************  ADC ****************
		//Chavear a porta analógica para a próxima leitura

		//bSD1 	= get_SD_state(1);
		//bSD2 	= get_SD_state(2);
		//bSD3 	= get_SD_state(3);

		printf("Antes de Ler as portas %d\n", xTaskGetTickCount());

		get_AnalogPointerAnt(&iEA);

		if      (iEA==3) 			// De TRIAC 2 para RELE  3
		{
			iPorta = 4;
			set_AnalogPointer(4);
		}
		else if (iEA==4)
		{
			iPorta = 3; // porta 3 rele 2 EA4
			set_AnalogPointer(6);	// De RELE  3 para RELE  ??
		}
		else if (iEA==6)
		{
			iPorta = 2;
			set_AnalogPointer(5);	// De RELE  2 para RELE  3
		}
		else if (iEA==5)
		{
			iPorta = 1;  // porta 1 rele 1 EA 5
			set_AnalogPointer(1);	// De RELE  3 para TRIAC 1
		}
		else if (iEA==1)
		{
			iPorta = 7;
			set_AnalogPointer(3);	// De TRIAC 1 para TRIAC 2
		}


		//printf("Apos  de Ler as portas %d\n", xTaskGetTickCount());


		//{"source" : "%.36s", "destiny" : "%.36s", "port": %d, "consumo": %d, "Adc" : %d}"
		bzero(buffer1,1025);
		get_GlobalConsumo(&dConsumo);

		// printf("Apos  de Ler o CONSUMO %d\n", xTaskGetTickCount());

		sprintf(buffer1, JSON_ADC_DATA, localDevice.GUID, localDevice.GUID, iPorta, arrayConsumo, dConsumo);

		printf("JSON: %s\n", buffer1);



		//printf("HORA: %d", sdk_system_get_rtc_time());
		//**************************************


		sta_socket = socket(AF_INET, SOCK_STREAM, 0);

        if (-1 == sta_socket) {

            //close(sta_socket);
						get_Flag_ConnectFail(&ifalhasconexao);
						ifalhasconexao++;
						set_Flag_ConnectFail(ifalhasconexao);

            printf("socket fail !\r\n");
            //continue;
						goto taskCheckCloud_exit;
        }

        //printf("socket ok!\r\n");



				// printf("Antes de config IP para o socket %d\n", xTaskGetTickCount());



        bzero(&remote_ip, sizeof(struct sockaddr_in));
        remote_ip.sin_family = AF_INET;
        remote_ip.sin_addr.s_addr = target_ip.addr; //inet_addr("http://whc.hereit.com.br/api/queue/GetQueueDevice/");
        remote_ip.sin_port = htons(80);

				wait(10); // acrescentado em 23/10
				// printf("Antes de abrir o socket %d\n", xTaskGetTickCount());

        if(0 != connect(sta_socket,(struct sockaddr *)(&remote_ip),sizeof(struct sockaddr)))
        {
            close(sta_socket);

						get_Flag_ConnectFail(&ifalhasconexao);
						//printf("\n\nifalhasconexao = %d\n\n", ifalhasconexao);
						ifalhasconexao++;
						set_Flag_ConnectFail(ifalhasconexao);
						sairdatarefa++;
						wait(50);
					//	printf("connect fail! ifalhasconexao = %d\n\n", ifalhasconexao);
						//get_Flag_ConnectFail(&ifalhasconexao);
						//printf("connect fail! atualizada ifalhasconexao = %d\n\n", ifalhasconexao);

						goto taskCheckCloud_exit;
        }

				//printf("connect ok!\r\n");


				// printf("Criacao do conteudo a ser enviado no POST%d\n", xTaskGetTickCount());

//*************

		//Criacao do conteudo a ser enviado no POST
		bzero(buffer2, 1024);
		memcpy(buffer2, buffer1, strlen(buffer1));
		bzero(buffer1, 1024);
        //Escrever a requisição no servidor
        //printf("Tarefa 'taskCheckCloudTLS': Escrevendo no servidor ...");
		len = sprintf((char *) buffer1,
				POSTCLOUDMASK,
				"http://whc.hereit.com.br/api/queue/GetQueueDevice/",
				"whc.hereit.com.br",
				localDevice.GUID,
				strlen(buffer2),
				buffer2
				);
		//printf("\n\nCONTEUDO ENVIO:\n\n %s\n\n", buffer1);

		//wait(50); mudado em 23/10
				// printf("Indo Escrever no Soquete %d\n", xTaskGetTickCount());


        //printf(buffer1);
        if (write(sta_socket,buffer1,strlen(buffer1)+1) < 0) {
            close(sta_socket);
						get_Flag_ConnectFail(&ifalhasconexao);
						ifalhasconexao++;
						set_Flag_ConnectFail(ifalhasconexao);
            printf("send fail\n");
            //free(buffer1);
						goto taskCheckCloud_exit;
        }
				else {         //printf("send success\n");
					//ifalhasconexao = 0;
					//set_Flag_ConnectFail(ifalhasconexao);
				}
				//printf("connect ok!\r\n");

				//free(buffer1);
        //free(buffer2);

				//wait(10); // 100 -> mudado em 23/10

        //char *recv_buf = (char *)malloc(1024);
        bzero(buffer1, 1024);
        bzero(buffer2, 1024);

        int r;
				//printf("Apos de comunicar com a cloud %d\n", xTaskGetTickCount());

				const struct timeval timeout = { 1, 0 }; /* 3 second timeout */
				setsockopt (sta_socket, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout) );

        do {
            bzero(buffer1, 1024);
            r = read(sta_socket, buffer1, 1024);
            if(r > 0) {
                //printf("%s", buffer1);
								memcpy(buffer2, buffer1, strlen(buffer1));
            }
						printf("preso no loop lendo resposta do socket");


        } while(r > 0);

				//printf("Apos receber dados da  comunicar com a cloud %d\n", xTaskGetTickCount());


		//******** Separar conteúdo do Header *********
        //printf("COMPLETO: %s \n\n", buffer2);
		bFlagUtil = false;
		bzero(buffer1, 1024);
		int j = 0;
		for (i = 3; i < strlen(buffer2); i++)
		{
			if (bFlagUtil)
			{
				buffer1[j] = (char)buffer2[i];
				j++;
			}

			if (buffer2[(i-3)] == 13 && buffer2[(i-2)] == 10 && buffer2[(i-1)] == 13 && buffer2[i] == 10)
			{
				bFlagUtil = true;
				printf("POS: %d\n\n", i);
			}


		}
		buffer1[j] = 0;
		//***********************************************
		if (buffer1) printf("UTIL: %s \n\n", buffer1);

		//Código de retorno do serviço
		bzero(sRetorno, 4);
		sprintf(sRetorno, "%.3s", (buffer1+2));
		printf("RETORNO: %s\n", sRetorno);


		//Entrar somente se resultado for sucesso (301) - Tem comandos a serem decriptografados
		if (strcmp(sRetorno, "301")==0)
		{
			//IV
			bzero(sTemp, 32);
			bzero(oGlobalIV, 17);

			sprintf(sTemp, "%.24s", (buffer1+18));
			vDecode_Base64(&oGlobalIV, sTemp);

			printf("oGlobalIV B64: %s\n", sTemp);
			printf("oGlobalIV\n");
			print_hex(oGlobalIV, 16);


			//Chave
			printf("oGlobalKey: %s\n\n", oGlobalKey);


			//Comando
			bzero(buffer2, 1024);
			sprintf(buffer2, "%s", (buffer1+84));
			buffer2[(strlen(buffer2)-2)] = 0;			//Caracter de fim de string para cortar os dois últimos caracteres => "]
			printf("COMANDO CRYPT: %s\n\n", buffer2);


			//Decriptografar
			bzero(buffer1, 1024);
			vDecrypt(&buffer1, buffer2, oGlobalKey, oGlobalIV );
			printf("COMANDO DECRYPT: %s\n\n", buffer1);

			//Executar comando
			if (desserializar_deviceCommand(&localCommand, (char *)buffer1))
			{
				executar_deviceCommand(localCommand);
			}


		}




		//Mostrar resultado da leitura EA7
		//printf("LEITURA EA7: %d\n", get_EA_value(7));


		showMemoryRAMStatus();


 		//free(buffer1);
        //free(buffer2);

		close(sta_socket);


		if  (checaFlag_GravarConfig==3) goto taskCheckCloud_exit;

		wait(10);

		ifalhasconexao =0;
		set_Flag_ConnectFail(ifalhasconexao);
    }




taskCheckCloud_exit:

		//printf("SAI DA CLOUD\n");

		goto taskCheckCloud_issecured;

		set_Flag_Seguranca(true);
		//set_Flag_CheckCloud(0);


		wait(5);
 		free(buffer1);
		wait(5);
    free(buffer2);
		wait(5);
 		free(sRetorno);
		wait(5);
 		free(sTemp);
		wait(5);
 		free(oGlobalKey);
		wait(5);
 		free(oGlobalIV);
		wait(5);
 		goto taskCheckCloud_issecured;

		//vTaskDelete(taskCheckCloud_hdl);

}

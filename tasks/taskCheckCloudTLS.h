/* *******************************************************************************************************
 
 TASK		: taskCheckCloudTLS
 OBJETIVO	: Verificar se existe conteudo na Cloud
 
*********************************************************************************************************/
static void taskCheckCloudTLS(void *pvParameters)
{
	
	set_Flag_CheckCloud(2);

	struct memoryreg_t localDevice;
	get_DeviceMemo(&localDevice);
	
	
	printf("\n\n\n****************************************************\n\n");
	printf(" TASK        : taskCheckCloudTLS\n");
	printf(" OBJETIVO    : Verificar se existe conteudo na Cloud \n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");

	
 	struct command_t localCommand;
    int ret, iJsonUncrypted = 0, len = 0, iPos = 0, i = 0, iVirg = 0;
    uint32_t flags;
	unsigned char buf[1024];								//ok
	char *JsonUncrypted = (char *)malloc(1024);				//ok
	unsigned char *JsonCrypted = (char *)malloc(1024);		//ok
    const char *pers = "ssl_client1";
	char *Teste66 = (char *)zalloc(1024);
	char **tokens;
//	char resultCode[3];
//	char resultDesc[10];
	unsigned char *oGlobalKey = (unsigned char *)malloc(33);
	unsigned char *oGlobalIV = (unsigned char *)malloc(33);
	bool bFinalizar = false;


	printf("\nTarefa 'taskCheckCloudTLS': iniciada...\n");
	
	//Cria��o das vari�veis de objeto de contexto
    printf("\nTarefa 'taskCheckCloudTLS': Criando objetos de contexto ...");
    mbedtls_entropy_context 	entropy;
    mbedtls_ctr_drbg_context 	ctr_drbg;
    mbedtls_ssl_context 		ssl;
    mbedtls_x509_crt 			cacert;
    mbedtls_ssl_config 			conf;
    mbedtls_net_context 		server_fd;
    printf(" OK.\n");

    //Inicializar os objetos da MBEDTLS
    printf("Tarefa 'taskCheckCloudTLS': Inicializando objetos de contexto ...");
    mbedtls_ssl_init(&ssl);
    mbedtls_x509_crt_init(&cacert);
    mbedtls_ctr_drbg_init(&ctr_drbg);
    mbedtls_ssl_config_init(&conf);
    mbedtls_entropy_init(&entropy);
    if((ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy,
                                    (const unsigned char *) pers,
                                    strlen(pers))) != 0)
    {
        printf(" Falha\n  ! mbedtls_ctr_drbg_seed retornou o erro %d\n", ret);
		vTaskSuspend(taskCheckCloudTLS_hdl);
    }
    printf(" OK.\n");

    
	//Inicializando o certificado
    printf("Tarefa 'taskCheckCloudTLS': Carregando o certificado ...");
    ret = mbedtls_x509_crt_parse(&cacert, (uint8_t*)server_root_cert, strlen(server_root_cert)+1);
    if(ret < 0)
    {
        printf(" Falha\n  !  mbedtls_x509_crt_parse retornou o erro -0x%x\n\n", -ret);
        goto taskCheckCloudTLS_exit;
    }
   
    if((ret = mbedtls_ssl_set_hostname(&ssl, WEB_SERVER_CLOUD)) != 0) // O Hostname configurado aqui deve ser compat�vel com o certificado do servidor
    {
        printf(" Falha\n  ! mbedtls_ssl_set_hostname retornou o erro %d\n\n", ret);
        goto taskCheckCloudTLS_exit;
    }
    printf(" OK.\n");

	
	
    //Inicializar a estrutura da comunica��o SSL/TLS
    printf("Tarefa 'taskCheckCloudTLS': Configurando a estrutura da comunicacao SSL/TLS ...");

    if((ret = mbedtls_ssl_config_defaults(&conf,
                                          MBEDTLS_SSL_IS_CLIENT,
                                          MBEDTLS_SSL_TRANSPORT_STREAM,
                                          MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
    {
        printf(" Falha\n  ! mbedtls_ssl_config_defaults retornou o erro %d\n\n", ret);
        goto taskCheckCloudTLS_exit;
    }
    printf(" OK.\n");

    printf("Tarefa 'taskCheckCloudTLS': Autenticando o servidor SSL/TLS ...");
    mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_OPTIONAL);
    mbedtls_ssl_conf_ca_chain(&conf, &cacert, NULL);
    mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
    
    printf(" OK.\n");
    if((ret = mbedtls_ssl_setup(&ssl, &conf)) != 0)
    {
		/* OPTIONAL is not optimal for security, in this example it will print
		   a warning if CA verification fails but it will continue to connect.
		*/
        printf(" Falha\n  ! mbedtls_ssl_setup retornou o erro %d\n\n", ret);
        goto taskCheckCloudTLS_exit;
    }
	
	
	
	//Resolver o DNS do servidor
    printf("Tarefa 'taskCheckCloudTLS': Aguardando pelo servidor de DNS... ");
    err_t dns_err;
    ip_addr_t host_ip;
    do {
        vTaskDelay(500 / portTICK_PERIOD_MS);
        dns_err = netconn_gethostbyname(WEB_SERVER_CLOUD, &host_ip);
    } while(dns_err != ERR_OK);
    printf(" OK.\n");
	
	
	//Inicio do loop da rotina de envio de mensagens
     while(1) {
		mbedtls_net_init(&server_fd);
        //printf("Tarefa 'taskCheckCloudTLS': Conectando-se a %s:%s...", WEB_SERVER_CLOUD, WEB_PORT);
		showMemoryRAMStatus();
		
        if((ret = mbedtls_net_connect(&server_fd, WEB_SERVER_CLOUD,
                                      WEB_PORT, MBEDTLS_NET_PROTO_TCP)) != 0)
        {
            printf(" Falha.\n  ! mbedtls_net_connect retornou o erro %d\n\n", ret);
            goto taskCheckCloudTLS_exit;
        }
		printf(" OK.\n");
		
		
		
        //Handshake
        printf("Tarefa 'taskCheckCloudTLS': Executando o handshake SSL/TLS...");
        mbedtls_ssl_set_bio(&ssl, &server_fd, mbedtls_net_send, mbedtls_net_recv, NULL);
        while((ret = mbedtls_ssl_handshake(&ssl)) != 0)
        {
            if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                printf(" Falha.\n  ! mbedtls_ssl_handshake retornou o erro -0x%x\n\n", -ret);
                goto taskCheckCloudTLS_exit;
            }
        }
		printf(" OK.\n");
    
        //Verificar o certificado do servidor
        printf("Tarefa 'taskCheckCloudTLS': Verificando o certificado X.509 do servidor...");
        if((flags = mbedtls_ssl_get_verify_result(&ssl)) != 0)
        {
            char vrfy_buf[512];
            printf(" Falha.\n");
            mbedtls_x509_crt_verify_info(vrfy_buf, sizeof(vrfy_buf), "  ! ", flags);
            printf("%s\n", vrfy_buf);
        }
        else printf(" OK.\n");
    
		
		//Criacao do conteudo a ser enviado no POST
		bzero(JsonCrypted, 1024);
		memcpy(JsonCrypted, "oi", 2);
        //Escrever a requisi��o no servidor
        printf("Tarefa 'taskCheckCloudTLS': Escrevendo no servidor ...");
		len = sprintf((char *) buf, 
				POSTCLOUDMASK,
				"https://whc.hereit.com.br/api/queue/GetQueueDevice/",
				"whc.hereit.com.br",
				localDevice.GUID,
				strlen(JsonCrypted),
				JsonCrypted
				);
		printf("\n\nCONTEUDO ENVIO (COM CRIPT):\n\n %s\n\n", buf); 
		
		//Enviar conteudo
        while((ret = mbedtls_ssl_write(&ssl, buf, len)) <= 0)
        {
            if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                printf(" Falha.\n  ! mbedtls_ssl_write retornou o erro %d\n\n", ret);
                goto taskCheckCloudTLS_exit;
            }
        }
    
        len = ret;
        printf(" OK.\n%d bytes escritos\n", len);
    
		
		
        //Ler a resposta HTTP
        printf("Tarefa 'taskCheckCloudTLS': Lendo a resposta do servidor ...");
		
		//struct communication_t resposta = (struct communication_t)zalloc(sizeof(struct communication_t));
		
        //do
        //{
            len = sizeof(buf) - 1;
            memset(buf, 0, sizeof(buf));
            ret = mbedtls_ssl_read(&ssl, buf, len);
    
            if(!(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE))
            {
				if(ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) {
					ret = 0;
					goto taskCheckCloudTLS_exit;
				}
    
				if(ret < 0)
				{
					printf("Falha.\n  ! mbedtls_ssl_read retornou o erro %d\n\n", ret);
					goto taskCheckCloudTLS_exit;
				}
    
				if(ret == 0)
				{
					//goto taskCheckCloudTLS_exit;
				}
			}
            len = ret;
			
			
            //printf(" OK.\n%d bytes lidos\n", len);
            printf("\nCONTEUDO RECEBIDO (COMPLETO):\n\n%s", (char *) buf);
			
			//Decriptografar mensagem
			
			iPos = position((char *)buf, "[");
			i = 0;
			iVirg = 0;
			if (iPos>0)
			{
				for (i = 0; i < (len - iPos); i++)
				{
					if (buf[(iPos + i)] == ',') iVirg = i;
					Teste66[i] = buf[(iPos + i)];
					
					//if ((i >= 2)&&(i <= 4)) resultCode[(i - 2)] = buf[(iPos + i)];
					//else if ((i >= 8)&&(i <= 14)) resultDesc[(i - 8)] = buf[(iPos + i)];
				}
				
				//printf("\n\nCODIGO: %s RESULTADO: %s ", resultCode, resultDesc);
				
				//printf("\n\nCONTEUDO UTIL: %s\n\n", Teste66);
				
				tokens = str_split(Teste66, ',');
				
				bzero(oGlobalIV, 33);
				bzero(JsonCrypted, 1024);
				
				if (tokens)
				{
					for (i = 0; *(tokens + i); i++)
					{
						printf("{%d}array=[%s]\n", i, *(tokens + i));
						
						//IV
						if (i==2)
						{
							memcpy(oGlobalIV, (*(tokens + i)+1), 24);
							
							//sprintf(oGlobalIV, "%s", *(tokens + i));
							printf("\noGlobalIV: %s [%d]\n", oGlobalIV, strlen(oGlobalIV) );
						}
						
						if (i==4)
						{
							
							memcpy(JsonCrypted, (*(tokens + i)+1), (strlen(*(tokens + i))-3) );
							printf("DADOS: %s [%d]\n", JsonCrypted, strlen(JsonCrypted));
							
							
							//Decriptografar mensagem
							bzero(JsonUncrypted, 1024);
							unsigned char currentKey[32];
							unsigned char *currentIV = (unsigned char *)malloc(17);
							
							bzero(currentIV, 17);
							vDecode_Base64(&currentIV, oGlobalIV);

							for (int j = 0; j < 32; j++) currentKey[j] = (unsigned char)localDevice.key[j];
							
							vDecrypt(&JsonUncrypted, JsonCrypted, currentKey, currentIV );
							
							if (desserializar_deviceCommand(&localCommand, (char *)JsonUncrypted))
							{
								executar_deviceCommand(localCommand);
							}
							
//get_globalKey(&oGlobalKey);
//get_globalIV(&oGlobalIV);
//vDecrypt(&JsonUncrypted, JsonCrypted, oGlobalKey, oGlobalIV );
							
							printf("\n\n**************** DECRIPT *********************\n");
							printf("currentKey: %.32s [%d]\n", currentKey, strlen(currentKey));
							printf("currentIV: %s [%d]\n", currentIV, strlen(currentIV));
							print_hex(currentIV, 16);
							printf("DECRYPT: %s [%d]\n", JsonUncrypted, strlen(JsonUncrypted));
							printf("\n***********************************************\n");
							
							free(currentIV);
							//bFinalizar = true;
						}
						
						
						free(*(tokens + i));
					}
					free(tokens);
					
				}
				
			}
		
		
		
        mbedtls_ssl_session_reset(&ssl);
        mbedtls_net_free(&server_fd);
		
		if (bFinalizar) goto taskCheckCloudTLS_exit;

		
		wait(1000);
	}
	
taskCheckCloudTLS_exit:
	
	free(JsonUncrypted);
	free(JsonCrypted);
	free(Teste66);
	free(oGlobalKey);
	free(oGlobalIV);
	
	printf(" taskCheckCloudTLS -> TERMINO EM : %d ms\n", xTaskGetTickCount());
 	
	set_Flag_CheckCloud(0);
	
	vTaskDelete(taskCheckCloudTLS_hdl);
		
		
}


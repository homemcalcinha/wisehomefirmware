/* *******************************************************************************************************
 
 TASK		: taskEventos
 OBJETIVO	: Tarefa responsavel por disparar os eventos solicitados
 
*********************************************************************************************************/
static void taskEventos(void *arg)
{
	
	printf("\n\n\n****************************************************\n\n");
	printf(" TASK        : taskEventos\n");
	printf(" OBJETIVO    : Tarefa responsavel por disparar os eventos solicitados\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");
	
	set_Flag_Eventos(2);
	
	
	for (;;)
	{
		//if( xSemaforo_dataBus != NULL )
		//{
		//	if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		//	{
				//********** EVT_ON         ***********
				if (ED1.evt_on)
				{
					printf("taskEventos => EVT_ON em ED1\n");
					ED1.evt_on = false;
				}
				if (ED2.evt_on)
				{
					printf("taskEventos => EVT_ON em ED2\n");
					ED2.evt_on = false;
				}
				if (ED3.evt_on)
				{
					printf("taskEventos => EVT_ON em ED3\n");
					ED3.evt_on = false;
				}
				if (ED4.evt_on)
				{
					printf("taskEventos => EVT_ON em ED4\n");
					ED4.evt_on = false;
				}
				if (ED5.evt_on)
				{
					printf("taskEventos => EVT_ON em ED5\n");
					ED5.evt_on = false;
				}
				if (ED6.evt_on)
				{
					printf("taskEventos => EVT_ON em ED6\n");
					ED6.evt_on = false;
				}
				if (ED7.evt_on)
				{
					printf("taskEventos => EVT_ON em ED7\n");
					ED7.evt_on = false;
				}
				if (ED8.evt_on)
				{
					printf("taskEventos => EVT_ON em ED8\n");
					ED8.evt_on = false;
				}
					
				if (SD1.evt_on)
				{
					printf("taskEventos => EVT_ON em SD1\n");
					SD1.evt_on = false;
				}
				if (SD2.evt_on)
				{
					printf("taskEventos => EVT_ON em SD2\n");
					SD2.evt_on = false;
				}
				if (SD3.evt_on)
				{
					printf("taskEventos => EVT_ON em SD3\n");
					SD3.evt_on = false;
				}
				if (SD4.evt_on)
				{
					printf("taskEventos => EVT_ON em SD4\n");
					SD4.evt_on = false;
				}
				if (SD5.evt_on)
				{
					printf("taskEventos => EVT_ON em SD5\n");
					SD5.evt_on = false;
				}
				if (SD6.evt_on)
				{
					printf("taskEventos => EVT_ON em SD6\n");
					SD6.evt_on = false;
				}
				if (SD7.evt_on)
				{
					printf("taskEventos => EVT_ON em SD7\n");
					SD7.evt_on = false;
				}
				if (SD8.evt_on)
				{
					printf("taskEventos => EVT_ON em SD8\n");
					SD8.evt_on = false;
				}
				
				
				//********** EVT_OFF        ***********
				if (ED1.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED1\n");
					ED1.evt_off = false;
				}
				if (ED2.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED2\n");
					ED2.evt_off = false;
				}
				if (ED3.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED3\n");
					ED3.evt_off = false;
				}
				if (ED4.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED4\n");
					ED4.evt_off = false;
				}
				if (ED5.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED5\n");
					ED5.evt_off = false;
				}
				if (ED6.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED6\n");
					ED6.evt_off = false;
				}
				if (ED7.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED7\n");
					ED7.evt_off = false;
				}
				if (ED8.evt_off)
				{
					printf("taskEventos => EVT_OFF em ED8\n");
					ED8.evt_off = false;
				}
					
				if (SD1.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD1\n");
					SD1.evt_off = false;
				}
				if (SD2.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD2\n");
					SD2.evt_off = false;
				}
				if (SD3.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD3\n");
					SD3.evt_off = false;
				}
				if (SD4.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD4\n");
					SD4.evt_off = false;
				}
				if (SD5.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD5\n");
					SD5.evt_off = false;
				}
				if (SD6.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD6\n");
					SD6.evt_off = false;
				}
				if (SD7.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD7\n");
					SD7.evt_off = false;
				}
				if (SD8.evt_off)
				{
					printf("taskEventos => EVT_OFF em SD8\n");
					SD8.evt_off = false;
				}
				
				
				
				//********** EVT_CHANGE     ***********
				if (ED1.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED1\n");
					ED1.evt_change = false;
				}
				if (ED2.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED2\n");
					ED2.evt_change = false;
				}
				if (ED3.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED3\n");
					ED3.evt_change = false;
				}
				if (ED4.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED4\n");
					ED4.evt_change = false;
				}
				if (ED5.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED5\n");
					ED5.evt_change = false;
				}
				if (ED6.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED6\n");
					ED6.evt_change = false;
				}
				if (ED7.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED7\n");
					ED7.evt_change = false;
				}
				if (ED8.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em ED8\n");
					ED8.evt_change = false;
				}
					
				if (SD1.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD1\n");
					SD1.evt_change = false;
				}
				if (SD2.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD2\n");
					SD2.evt_change = false;
				}
				if (SD3.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD3\n");
					SD3.evt_change = false;
				}
				if (SD4.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD4\n");
					SD4.evt_change = false;
				}
				if (SD5.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD5\n");
					SD5.evt_change = false;
				}
				if (SD6.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD6\n");
					SD6.evt_change = false;
				}
				if (SD7.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD7\n");
					SD7.evt_change = false;
				}
				if (SD8.evt_change)
				{
					printf("taskEventos => EVT_CHANGE em SD8\n");
					SD8.evt_change = false;
				}
				
				
				
				//********** EVT_CLICK_UP   ***********
				if (ED1.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED1\n");
					ED1.evt_click_up = false;
				}
				if (ED2.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED2\n");
					ED2.evt_click_up = false;
				}
				if (ED3.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED3\n");
					ED3.evt_click_up = false;
				}
				if (ED4.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED4\n");
					ED4.evt_click_up = false;
				}
				if (ED5.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED5\n");
					ED5.evt_click_up = false;
				}
				if (ED6.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED6\n");
					ED6.evt_click_up = false;
				}
				if (ED7.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED7\n");
					ED7.evt_click_up = false;
				}
				if (ED8.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em ED8\n");
					ED8.evt_click_up = false;
				}
					
				if (SD1.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD1\n");
					SD1.evt_click_up = false;
				}
				if (SD2.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD2\n");
					SD2.evt_click_up = false;
				}
				if (SD3.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD3\n");
					SD3.evt_click_up = false;
				}
				if (SD4.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD4\n");
					SD4.evt_click_up = false;
				}
				if (SD5.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD5\n");
					SD5.evt_click_up = false;
				}
				if (SD6.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD6\n");
					SD6.evt_click_up = false;
				}
				if (SD7.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD7\n");
					SD7.evt_click_up = false;
				}
				if (SD8.evt_click_up)
				{
					printf("taskEventos => EVT_CLICK_UP em SD8\n");
					SD8.evt_click_up = false;
				}
				
				
				
				//********** EVT_CLICK_DOWN ***********
				if (ED1.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED1\n");
					ED1.evt_click_down = false;
				}
				if (ED2.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED2\n");
					ED2.evt_click_down = false;
				}
				if (ED3.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED3\n");
					ED3.evt_click_down = false;
				}
				if (ED4.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED4\n");
					ED4.evt_click_down = false;
				}
				if (ED5.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED5\n");
					ED5.evt_click_down = false;
				}
				if (ED6.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED6\n");
					ED6.evt_click_down = false;
				}
				if (ED7.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED7\n");
					ED7.evt_click_down = false;
				}
				if (ED8.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em ED8\n");
					ED8.evt_click_down = false;
				}
					
				if (SD1.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD1\n");
					SD1.evt_click_down = false;
				}
				if (SD2.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD2\n");
					SD2.evt_click_down = false;
				}
				if (SD3.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD3\n");
					SD3.evt_click_down = false;
				}
				if (SD4.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD4\n");
					SD4.evt_click_down = false;
				}
				if (SD5.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD5\n");
					SD5.evt_click_down = false;
				}
				if (SD6.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD6\n");
					SD6.evt_click_down = false;
				}
				if (SD7.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD7\n");
					SD7.evt_click_down = false;
				}
				if (SD8.evt_click_down)
				{
					printf("taskEventos => EVT_CLICK_DOWN em SD8\n");
					SD8.evt_click_down = false;
				}
				
		//	}
		//}
		

		wait(100);
	}
	
	printf(" taskEventos -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	set_Flag_Eventos(0);
	vTaskDelete(taskEventos_hdl);
	
	
}
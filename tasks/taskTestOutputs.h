/* *******************************************************************************************************
 
 TASK		: taskTestOutputs
 OBJETIVO	: Tarefa responsavel por testar as saidas do dispositivo
 
*********************************************************************************************************/
static void taskTestOutputs(void *arg)
{
    //bool bFlag = false;
	//get_Flag_TestOutputs(&bFlag);
	//
	//if (!bFlag) vTaskSuspend(taskTestOutputs_hdl);
	
	set_Flag_TestOutputs(2);
	
	bool bInverter = false;
	
	for (;;)
	{
		for (int i = 1; i <= 8; i++)
		{
			set_SD_state(i, bInverter);
			wait(500);
		
		}
		bInverter = !bInverter;
	}
	
	//taskReadWriteIOs_hdl = NULL;
	printf(" taskTestOutputs -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	vTaskDelete(taskReadWriteIOs_hdl);
	
	
}
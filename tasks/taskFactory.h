/* *******************************************************************************************************

 TASK		: taskFactory
 OBJETIVO	: Verifica e registra o primeiro contato com a Cloud de Factory e registra o GUID do dispositivo

*********************************************************************************************************/
static void taskFactory(void *pvParameters)
{
		const char			oGlobalKey[]						= { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32 };
		const char 			oGlobalIV[] 						= { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36 };
		set_Flag_CheckCloud(2);
    char *buffer1 			= (char *)malloc(1025);								//ok
    char *buffer2			= (char *)malloc(1025);								//ok
	//char **tokens;
	int i = 0, iVirg = 0, iPos = 0, len = 0;

	struct memoryreg_t localDevice;
	get_DeviceMemo(&localDevice);
//	get_globalKey(&oGlobalKey);
//	get_globalIV(&oGlobalIV);

	//printf("\n\n\n  IV %s   \n\n\n", oGlobalIV);
	if ((strcmp(localDevice.GUID, ""))!=0)
	{
		printf("Tarefa 'taskFactory': GUID registrado.\n\n");
		printf(" taskRecuperarConfig -> TERMINO EM : %d ms\n", xTaskGetTickCount());

		free(buffer1);
		free(buffer2);

		set_Flag_Factory(2);
		goto taskFactory_exit;
		taskFactory_hdl = NULL;
		vTaskDelete(NULL);
	}


	printf("\n\n\n****************************************************\n\n");
	printf(" TASK        : taskFactory\n");
	printf(" OBJETIVO    : Verifica e registra o primeiro \n");
	printf("               contato com a Cloud de Factory e \n");
	printf("               registra o GUID do dispositivo\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");

    int recbytes, ret;
    int sin_size;
    int sta_socket;
    //char recv_buf[1460];

    struct sockaddr_in remote_ip;
	ip_addr_t target_ip;
	do {
        ret = netconn_gethostbyname(WEB_SERVER_FACTORY, &target_ip);
    } while(ret);
    printf("get target IP is %d.%d.%d.%d\n", (unsigned char)((target_ip.addr & 0x000000ff) >> 0),
                                             (unsigned char)((target_ip.addr & 0x0000ff00) >> 8),
                                             (unsigned char)((target_ip.addr & 0x00ff0000) >> 16),
                                             (unsigned char)((target_ip.addr & 0xff000000) >> 24));


	wait(100);

    //while (1) {
        //showMemoryRAMStatus();

		sta_socket = socket(AF_INET, SOCK_STREAM, 0);

        if (-1 == sta_socket) {
            close(sta_socket);
            printf("socket fail !\r\n");
            //continue;
						goto taskFactory_exit;
        }
        //printf("socket ok!\r\n");

        bzero(&remote_ip, sizeof(struct sockaddr_in));
        remote_ip.sin_family = AF_INET;
        remote_ip.sin_addr.s_addr = target_ip.addr;
        remote_ip.sin_port = htons(80);

        if(0 != connect(sta_socket,(struct sockaddr *)(&remote_ip),sizeof(struct sockaddr)))
        {
            close(sta_socket);
            printf("connect fail!\r\n");
						goto taskFactory_exit;
        }
        //printf("connect ok!\r\n");



		//Montar JSON de registro
        printf("Tarefa 'taskFactory': Montando o texto JSON ...");
				//iJsonUncrypted =

				sprintf((char *) buffer1,
				JSON_FACTORY_SCHEMA,
				"2018-01-01T13:38:00",					//Manufacturing_Date
				1,										//Lote_Number
				"SERIAL123",                            //Serial_Number
				1,                                      //Factory
				"SMD123",                               //SMD_Line
				sdk_system_get_chip_id(),               //Chip_ID
				sdk_spi_flash_get_id(),                 //Flash_ID
				getMCADD()                              //MacAddress
				);
		printf("\n\nCONTEUDO ENVIO (SEM CRIPT):\n\n %s\n\n", buffer1);

		//Criptografar conteudo
        printf("Tarefa 'taskFactory': Criptografar JSON ...");
		int iSize = vEncrypt(&buffer2, buffer1, globalKey, globalIV);
		buffer2[iSize] = 0;


        //Montar e envelopar pacote
        printf("Tarefa 'taskFactory': Montar e envelopar pacote ...");
		bzero(buffer1, 1024);
		sprintf((char *) buffer1,
				POSTMASK,
				"http://wisehomefactory.hereit.com.br/api/device_manufacturing/Register/",
				"wisehomefactory.hereit.com.br",
				"MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTI=",
				"MTIzNDU2Nzg5MDEyMzQ1Ng==",
				strlen(buffer2),
				buffer2
				);
		buffer1[strlen(buffer1)]= 0;
		printf("\n\nCONTEUDO ENVIO (COM CRIPT):\n\n %s\n\n", buffer1);

		wait(50);

	//ENVIANDO...
        if (write(sta_socket,buffer1,strlen(buffer1)+1) < 0) {
            close(sta_socket);
            printf("send fail\n");
						goto taskFactory_exit;
        }

        printf("send success - keko passou aki\n");

				wait(500);

		//RECEBENDO...
        //static char recv_buf[1024];
				printf("\n\n\n\n Aguardando resposta\n\n");
//				printf("%s", sta_socket);


				char *sRetorno = (char *) zalloc(4);
				char *sTemp = (char *)malloc(32);

				showMemoryRAMStatus();
				wait(10);
        //bzero(buffer1, 1024);

				char *buffer3			= (char *)zalloc(1025);


        int xpto;
        //do {
            xpto = read(sta_socket, buffer3, 1024);

						wait(10);
            if(xpto > 0) {
                //printf("%s", bufferKeko);
            }
        //} while(xpto > 0);


				printf("Se for recebido é igual a 0  = %d\n", xpto);

				showMemoryRAMStatus();
				//memcpy(buffer2, buffer1, strlen(buffer1));
				printf("\n\nrecbuffer = %s\n", buffer3);

				free(buffer2);
				//memcpy(buffer2, buffer1, strlen(buffer1));

				bzero(buffer1, 1024);
				printf("recbuffer3 = %s\n", buffer3);

				showMemoryRAMStatus();

				//******** Separar conteúdo do Header *********

				printf("COMPLETO: %s \n\n", buffer3);

				bool bFlagUtil = false;

				int j = 0;
				for (i = 3; i < strlen(buffer3); i++)
				{
						if (bFlagUtil)
						{
								buffer1[j] = (char)buffer3[i];
								j++;
						}

						if (buffer3[(i-3)] == 13 && buffer3[(i-2)] == 10 && buffer3[(i-1)] == 13 && buffer3[i] == 10)
						{
								bFlagUtil = true;
								printf("POS: %d\n\n", i);
						}


				}
				buffer1[j] = 0;
				//***********************************************
				if (buffer1) printf("UTIL: %s \n\n", buffer1);

				//Código de retorno do serviço
				bzero(sRetorno, 4);
				sprintf(sRetorno, "%.3s", (buffer1+2));
				printf("RETORNO: %s\n", sRetorno);

				//Entrar somente se resultado for sucesso (301) - Tem comandos a serem decriptografados
				if (strcmp(sRetorno, "401")==0)
				{
						//IV
						bzero(sTemp, 32);
						print_hex(oGlobalIV, 16);
						//Chave
						printf("oGlobalKey: %s\n\n", oGlobalKey);
						//IV
						printf("oGlobalIV: %s\n\n", oGlobalIV);

						//Comando
						bzero(buffer3, 1024);

						sprintf(buffer3, "%s", right(buffer1,(strlen(buffer1)-18)));

						//sprintf(buffer3, "%s", (buffer1+84));
						buffer3[(strlen(buffer3)-2)] = 0;			//Caracter de fim de string para cortar os dois últimos caracteres => "]
						printf("COMANDO CRYPT: %s\n\n", buffer3);

						showMemoryRAMStatus();
						//Decriptografar
						//bzero(buffer1, 1024);
						vDecrypt(&buffer1, buffer3, oGlobalKey, oGlobalIV );
						printf("COMANDO DECRYPT: %s\n\n", buffer1);


						showMemoryRAMStatus(); //memoria okay.

						free(buffer3);
						//free(oGlobalKey);
						//free(oGlobalIV);


						showMemoryRAMStatus();

						//if (buffer1)
						//{
								//wait(1000);
								//set_Flag_ServidorHTTP(1);
								//struct memoryreg_t wisedev;
								printf("\n\n\nJsonUncrypted2: %s\n\n\n", buffer1);
								//desserializar_DeviceMemo(&wisedev, buffer1);
								//set_DeviceMemo(localDevice);
								char *ttt 			= (char *)malloc(1025);								//ok
								char *ppp			= (char *)malloc(1025);
								char *KekoJson = (char *)malloc(1025);
								ttt = left(buffer1,strlen(buffer1)-3);
								ttt = right(ttt, strlen(ttt)-55);


								ppp = left(buffer1,46);
								ppp = right(ppp, strlen(ppp)-10);



								printf("GUIID %s\n", ppp);
								printf("\n\nKEYY  %s\n", ttt);

								sprintf(KekoJson, JSON_DEVICEMEMO_SCHEMA, ppp, ttt, "", "","","",'0');


							//	"{\"GUID\":\"%.36s\",\"key\":\"%.32s\",\"ssid_sta\":\"%s\",\"pass_sta\":\"%s\",\"ssid_ap\":\"%s\",\"pass_ap\":\"%s\",\"device_state\":\"%c\"}"


																//serializar_DeviceMemo(&sJson);
																printf("eu sou o Json:%s\n", KekoJson);
																char *sJsonCrypt 			= (char *)malloc(1025);
																int iSize = vEncrypt(&sJsonCrypt, KekoJson, globalKey, globalIV);
																sJsonCrypt[iSize] = 0;
																//sJsonCrypt[iSize+1] = 0;
																printf("linha 13\n");
																sJsonCrypt = append(sJsonCrypt, "|");
																sJsonCrypt[iSize+1]= 0;

																wait(30);
																esp_spiffs_init();
																if (esp_spiffs_mount() != SPIFFS_OK) {
																	printf("linha 16\n");
																	showMessage("taskGravarConfig", "Montar SPIFFS", "ERRO", "Falha ao montar a SPIFFS");
																}
																showMemoryRAMStatus();
																printf("linha 4\n");
																printf("\nsituacao do cryt:\n%s", sJsonCrypt);
																int fd = open("config.cfg", O_WRONLY|O_CREAT, 0);
																SPIFFS_write(&fs, (spiffs_file)fd, sJsonCrypt, strlen(sJsonCrypt));
																close(fd);
																printf("linha 24\n");
																SPIFFS_unmount(&fs);
																printf("linha 25\n");
																esp_spiffs_deinit();
																sdk_system_restart();


								//set_Flag_ServidorHTTP(1);
								//localDevice.device_state = '0';
								//set_Flag_ServidorHTTP(1);
								//set_DeviceMemo(localDevice);
								//printf("acabei\n");
						//}

				}
				else {
						goto taskFactory_exit;
				}

		showMemoryRAMStatus();


		close(sta_socket);


		printf("eu fechei o socket\n");


taskFactory_exit:
	free(buffer1);
	free(buffer3);
	set_Flag_Factory(2);

	printf(" taskFactory -> TERMINO EM : %d ms\n", xTaskGetTickCount());

	vTaskDelete(taskFactory_hdl);

}

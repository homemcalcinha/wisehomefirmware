/* *******************************************************************************************************
 
 TASK		: taskServidorTLS
 OBJETIVO	: Criar o servidor TLS
 
*********************************************************************************************************/
static void taskServidorTLS(void *pvParameters)
{
 	set_Flag_ServidorTLS(2);
   
	bool bFlag = false;
	int successes = 0, failures = 0, ret;
    unsigned char buf_rec[1024];
	unsigned char buf[256];
	int len = 0, i = 0, j = 0;
	struct command_t localCommand;
	
	printf("\n\n\n****************************************************\n\n");
	printf(" FUNCTION    : taskServidorTLS\n");
	printf(" OBJETIVO    : Criar o servidor TLS\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");
	
	
    printf("TLS server task starting...\n");

    const char *pers = "tls_server";

    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ssl_context ssl;
    mbedtls_x509_crt srvcert;
    mbedtls_pk_context pkey;
    mbedtls_ssl_config conf;
    mbedtls_net_context server_ctx;

    /*
     * 0. Initialize the RNG and the session data
     */
    mbedtls_ssl_init(&ssl);
    mbedtls_x509_crt_init(&srvcert);
    mbedtls_pk_init( &pkey );
    mbedtls_ctr_drbg_init(&ctr_drbg);
    printf("\n  . Seeding the random number generator...");

    mbedtls_ssl_config_init(&conf);

    mbedtls_entropy_init(&entropy);
    if((ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy,
                                    (const unsigned char *) pers,
                                    strlen(pers))) != 0)
    {
        printf(" failed\n  ! mbedtls_ctr_drbg_seed returned %d\n", ret);
        abort();
    }

    printf(" ok\n");

    /*
     * 0. Initialize certificates
     */
    printf("  . Loading the server certificate ...");

    ret = mbedtls_x509_crt_parse(&srvcert, (uint8_t*)server_cert, strlen(server_cert)+1);
    if(ret < 0)
    {
        printf(" failed\n  !  mbedtls_x509_crt_parse returned -0x%x\n\n", -ret);
        abort();
    }

    printf(" ok (%d skipped)\n", ret);

    printf("  . Loading the server private key ...\n\n");
    ret = mbedtls_pk_parse_key(&pkey, (uint8_t *)server_key, strlen(server_key)+1, NULL, 0);
    if(ret != 0)
    {
        printf(" failed\n ! mbedtls_pk_parse_key returned - 0x%x\n\n", -ret);
        vTaskSuspend(taskServidorTLS_hdl);
		abort();
    }

    printf(" ok\n");

    /*
     * 2. Setup stuff
     */
    printf("  . Setting up the SSL/TLS structure...");

    if((ret = mbedtls_ssl_config_defaults(&conf,
                                          MBEDTLS_SSL_IS_SERVER,
                                          MBEDTLS_SSL_TRANSPORT_STREAM,
                                          MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
    {
        printf(" failed\n  ! mbedtls_ssl_config_defaults returned %d\n\n", ret);
        goto taskServidorTLS_exit;
    }

    printf(" ok\n");

    mbedtls_ssl_conf_ca_chain(&conf, srvcert.next, NULL);
    if( ( ret = mbedtls_ssl_conf_own_cert( &conf, &srvcert, &pkey ) ) != 0 )
    {
        printf( " failed\n  ! mbedtls_ssl_conf_own_cert returned %d\n\n", ret );
        abort();
    }

    mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
#ifdef MBEDTLS_DEBUG_C
    mbedtls_debug_set_threshold(DEBUG_LEVEL);
    mbedtls_ssl_conf_dbg(&conf, my_debug, stdout);
#endif

    if((ret = mbedtls_ssl_setup(&ssl, &conf)) != 0)
    {
        printf(" failed\n  ! mbedtls_ssl_setup returned %d\n\n", ret);
        goto taskServidorTLS_exit;
    }

    while(1) {
printf("FreeHeapSize TOPO WHILE => %d bytes\n", xPortGetFreeHeapSize());		

        mbedtls_net_context client_ctx;
        mbedtls_net_init(&server_ctx);
        mbedtls_net_init(&client_ctx);
        printf("top of loop, free heap = %u\n", xPortGetFreeHeapSize());

        /*
         * 1. Start the connection
         */
        ret = mbedtls_net_bind(&server_ctx, NULL, PORT, MBEDTLS_NET_PROTO_TCP);
        if(ret != 0)
        {
            printf(" failed\n  ! mbedtls_net_bind returned %d\n\n", ret);
            goto taskServidorTLS_exit;
        }

        printf(" ok\n");

        ret=mbedtls_net_accept(&server_ctx, &client_ctx, NULL, 0 , NULL);
        if( ret != 0 ){
            printf(" Failed to accept connection. Restarting.\n");
            mbedtls_net_free(&server_ctx);
            continue;
        }

        mbedtls_ssl_set_bio(&ssl, &client_ctx, mbedtls_net_send, mbedtls_net_recv, NULL);

        /*
         * 4. Handshake
         */
        printf("  . Performing the SSL/TLS handshake...");

        while((ret = mbedtls_ssl_handshake(&ssl)) != 0)
        {
printf("FreeHeapSize TOPO WHILE HANDSHAKE => %d bytes\n", xPortGetFreeHeapSize());		
            if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                printf(" failed\n  ! mbedtls_ssl_handshake returned -0x%x HEAP: %d \n\n", -ret, xPortGetFreeHeapSize());
                //goto taskServidorTLS_exit;
                goto taskServidorTLS_continuar;
            }
        }

taskServidorTLS_continuar:

        printf("Handshake ok [%d]\n", ret);


        /*
         * 3. Write the GET request
         */
        printf("  > Writing status to new client... ");

        struct sockaddr_in peer_addr;
        socklen_t peer_addr_len = sizeof(struct sockaddr_in);
        getpeername(client_ctx.fd, (struct sockaddr *)&peer_addr, &peer_addr_len);
        
	   //read
		do
		{
			len = sizeof( buf_rec ) - 1;
			memset( buf_rec, 0, sizeof( buf_rec ) );
			ret = mbedtls_ssl_read( &ssl, buf_rec, len );

			printf( "READ ret: [%d] \n", ret );
			
			if( ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE )
				continue;

			if( ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY )
				break;
			
			// CONDICIONAL PARA PASSAR ATÉ O KEKO COLOCAR UM CERTIFICADO VÁLIDO
			if ( ret == MBEDTLS_SSL_ALERT_MSG_CERT_REVOKED || ret == MBEDTLS_SSL_ALERT_MSG_UNSUPPORTED_CERT )
				break;
			//-----------------------------------------------------------------
			
			
			if( ret < 0 )
			{
				printf( "failed\n  ! mbedtls_ssl_read returned %d\n\n", ret );
				break;
			}

			if( ret == 0 )
			{
				printf( "\n\nEOF\n\n" );
				break;
			}

			len = ret;
			
printf("FreeHeapSize CRIANDO sJsonUtil => %d bytes\n", xPortGetFreeHeapSize());		
			
			//char sJsonUtil[1024];
			char *sJsonUtil = (char *)malloc(1024);
			bzero(sJsonUtil, sizeof(sJsonUtil));
printf("FreeHeapSize DEPOIS DE CRIAR sJsonUtil => %d bytes\n", xPortGetFreeHeapSize());		
			
			bool bFlagUtil = false;
			j = 0;
			
			printf( "\n--------- CONTEUDO -----------\n" );
			for (i = 3; i < len; i++)
			{
				//printf("%d: %c [%02x] BOOL: %i \n", i, sTeste[i], sTeste[i], bFlagUtil);
				if (bFlagUtil) 
				{
					sJsonUtil[j] = (char)buf_rec[i];
					j++;
					printf("%c", buf_rec[i]);
				}
				
				if (buf_rec[(i-3)] == 13 && buf_rec[(i-2)] == 10 && buf_rec[(i-1)] == 13 && buf_rec[i] == 10)
					bFlagUtil = true;
				
			}
			printf( "\n------------------------------\n" );
			
			if (desserializar_deviceCommand(&localCommand, (char *)sJsonUtil))
			{
				printf("\n\nRECEBIDO: \n%s\n\n", sJsonUtil);
				executar_deviceCommand(localCommand);
			}
printf("FreeHeapSize ANTES DE LIBERAR sJsonUtil => %d bytes\n", xPortGetFreeHeapSize());		
			free(sJsonUtil);
printf("FreeHeapSize DEPOIS DE LIBERAR sJsonUtil => %d bytes\n", xPortGetFreeHeapSize());		
			
			break;	// CONDICIONAL PARA PASSAR ATÉ O KEKO COLOCAR UM CERTIFICADO VÁLIDO
		}
		while( 1 );
		
		
		//goto taskServidorTLS_exit;
		
		
		//Write
        len = sprintf((char *) buf, "Hello, client " IPSTR ":%d\nFree heap size is %d bytes\n",
                          IP2STR((ip4_addr_t *)&peer_addr.sin_addr.s_addr),
                          peer_addr.sin_port, xPortGetFreeHeapSize());
        
		while((ret = mbedtls_ssl_write(&ssl, buf, len)) <= 0)
        {
            if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                printf(" failed\n  ! mbedtls_ssl_write returned %d\n\n", ret);
                goto taskServidorTLS_exit;
            }
        }

		
		
		
        len = ret;
        ret = 0;
        printf(" %d bytes written. Closing socket on client.\n\n%s", len, (char *) buf);

        mbedtls_ssl_close_notify(&ssl);

    taskServidorTLS_exit:
	
        mbedtls_ssl_session_reset(&ssl);
        mbedtls_net_free(&client_ctx);
        mbedtls_net_free(&server_ctx);

        if(ret != 0)
        {
            char error_buf[100];
            mbedtls_strerror(ret, error_buf, 100);
            printf("\n\nLast error was: %d - %s\n\n", ret, error_buf);
            failures++;
        } else {
            successes++;
        }

        printf("\n\nsuccesses = %d failures = %d\n", successes, failures);
        printf("Waiting for next client...\n");
		
		
		
		
    }
	
	printf(" taskServidorTLS -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	
	vTaskDelete(taskServidorTLS_hdl);
	//taskServidorTLS_hdl = NULL;
}


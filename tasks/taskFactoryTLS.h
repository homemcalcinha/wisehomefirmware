/* *******************************************************************************************************
 
 TASK		: taskFactory
 OBJETIVO	: Verifica e registra o primeiro contato com a Cloud de Factory e registra o GUID do dispositivo
 
*********************************************************************************************************/
void taskFactory(void *pvParameters)
{
	

	struct memoryreg_t localDevice;
	get_DeviceMemo(&localDevice);
	
	
	printf("\n\n\n****************************************************\n\n");
	printf(" TASK        : taskFactory\n");
	printf(" OBJETIVO    : Verifica e registra o primeiro \n");
	printf("               contato com a Cloud de Factory e \n");
	printf("               registra o GUID do dispositivo\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");
	
	
    int successes = 0, failures = 0, ret, iJsonUncrypted = 0, len = 0, iPos = 0, i = 0, iVirg = 0;
    uint32_t flags;
    //unsigned char buf[1024];								//ok
	//unsigned char JsonUncrypted[1024];						//ok
    char *buffer1 			= (char *)malloc(1024);								//ok
    char *buffer2			= (char *)malloc(1024);								//ok
	//char *JsonUncrypted = (char *)malloc(1024);						//ok
	//unsigned char *JsonCrypted;								//ok
	//unsigned char *JsonUncrypted2;
    const char *pers = "ssl_client1";
	//const char *key = "12345678901234567890123456789012";
	//const char *iv = "1234567890123456";
	unsigned char *oGlobalKey = (unsigned char *)malloc(32);
	unsigned char *oGlobalIV = (unsigned char *)malloc(16);
	//char *Teste66 = (char *)zalloc(1024);
	char **tokens;
	char resultCode[3];
	char resultDesc[10];
    
	get_globalKey(&oGlobalKey);
	get_globalIV(&oGlobalIV);
	
	
	
	if ((strcmp(localDevice.GUID, ""))!=0)
	{
		
		//bzero(JsonUncrypted, 1024);
		//memcpy(JsonUncrypted, "{\"GUID\":\"\",\"key\":\"\",\"ssid_sta\":\"\",\"pass_sta\":\"\",\"ssid_ap\":\"\",\"pass_ap\":\"\",\"device_state\":\"0\" }", 94);
		//
		//JsonCrypted = (encrypt(JsonUncrypted, key, iv));
		//printf("DECRIPTOGRAFADO: %s\nCRIPTOGRAFADO: %s \n\n", JsonUncrypted, JsonCrypted);
		
		
		printf("Tarefa 'taskFactory': GUID registrado.\n\n");
		printf(" taskRecuperarConfig -> TERMINO EM : %d ms\n", xTaskGetTickCount());

		free(buffer1);
		free(buffer2);
		free(oGlobalKey);
		free(oGlobalIV);
		set_Flag_Factory(2);
		taskFactory_hdl = NULL;
		vTaskDelete(NULL);
	}
	
	
	
	printf("\nTarefa 'taskFactory': iniciada...\n");
	
	//Cria��o das vari�veis de objeto de contexto
    printf("\nTarefa 'taskFactory': Criando objetos de contexto ...");
    mbedtls_entropy_context 	entropy;
    mbedtls_ctr_drbg_context 	ctr_drbg;
    mbedtls_ssl_context 		ssl;
    mbedtls_x509_crt 			cacert;
    mbedtls_ssl_config 			conf;
    mbedtls_net_context 		server_fd;
    printf(" OK.\n");

    //Inicializar os objetos da MBEDTLS
    printf("Tarefa 'taskFactory': Inicializando objetos de contexto ...");
    mbedtls_ssl_init(&ssl);
    mbedtls_x509_crt_init(&cacert);
    mbedtls_ctr_drbg_init(&ctr_drbg);
    mbedtls_ssl_config_init(&conf);
    mbedtls_entropy_init(&entropy);
    if((ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy,
                                    (const unsigned char *) pers,
                                    strlen(pers))) != 0)
    {
        printf(" Falha\n  ! mbedtls_ctr_drbg_seed retornou o erro %d\n", ret);
        abort();
    }
    printf(" OK.\n");

    
	//Inicializando o certificado
    printf("Tarefa 'taskFactory': Carregando o certificado ...");
    ret = mbedtls_x509_crt_parse(&cacert, (uint8_t*)server_root_cert, strlen(server_root_cert)+1);
    if(ret < 0)
    {
        printf(" Falha\n  !  mbedtls_x509_crt_parse retornou o erro -0x%x\n\n", -ret);
        abort();
    }
   
    if((ret = mbedtls_ssl_set_hostname(&ssl, WEB_SERVER)) != 0) // O Hostname configurado aqui deve ser compat�vel com o certificado do servidor
    {
        printf(" Falha\n  ! mbedtls_ssl_set_hostname retornou o erro %d\n\n", ret);
        abort();
    }
    printf(" OK.\n");

	
	
    //Inicializar a estrutura da comunica��o SSL/TLS
    printf("Tarefa 'taskFactory': Configurando a estrutura da comunicacao SSL/TLS ...");

    if((ret = mbedtls_ssl_config_defaults(&conf,
                                          MBEDTLS_SSL_IS_CLIENT,
                                          MBEDTLS_SSL_TRANSPORT_STREAM,
                                          MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
    {
        printf(" Falha\n  ! mbedtls_ssl_config_defaults retornou o erro %d\n\n", ret);
        goto taskFactory_exit;
    }
    printf(" OK.\n");

    printf("Tarefa 'taskFactory': Autenticando o servidor SSL/TLS ...");
    mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_OPTIONAL);
    mbedtls_ssl_conf_ca_chain(&conf, &cacert, NULL);
    mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
    
    printf(" OK.\n");
    if((ret = mbedtls_ssl_setup(&ssl, &conf)) != 0)
    {
		/* OPTIONAL is not optimal for security, in this example it will print
		   a warning if CA verification fails but it will continue to connect.
		*/
        printf(" Falha\n  ! mbedtls_ssl_setup retornou o erro %d\n\n", ret);
        goto taskFactory_exit;
    }
	
	
	
	//Resolver o DNS do servidor
    printf("Tarefa 'taskFactory': Aguardando pelo servidor de DNS... ");
    err_t dns_err;
    ip_addr_t host_ip;
    do {
        vTaskDelay(500 / portTICK_PERIOD_MS);
        dns_err = netconn_gethostbyname(WEB_SERVER, &host_ip);
    } while(dns_err != ERR_OK);
    printf(" OK.\n");
	
	
	//Inicio do loop da rotina de envio de mensagens
        mbedtls_net_init(&server_fd);
        printf("Tarefa 'taskFactory': Conectando-se a %s:%s...", WEB_SERVER, WEB_PORT);
    
        if((ret = mbedtls_net_connect(&server_fd, WEB_SERVER,
                                      WEB_PORT, MBEDTLS_NET_PROTO_TCP)) != 0)
        {
            printf(" Falha.\n  ! mbedtls_net_connect retornou o erro %d\n\n", ret);
            goto taskFactory_exit;
        }
		printf(" OK.\n");
		
		
		
        //Handshake
        printf("Tarefa 'taskFactory': Executando o handshake SSL/TLS...");
        mbedtls_ssl_set_bio(&ssl, &server_fd, mbedtls_net_send, mbedtls_net_recv, NULL);
        while((ret = mbedtls_ssl_handshake(&ssl)) != 0)
        {
            if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                printf(" Falha.\n  ! mbedtls_ssl_handshake retornou o erro -0x%x\n\n", -ret);
                goto taskFactory_exit;
            }
        }
		printf(" OK.\n");
    
        //Verificar o certificado do servidor
        printf("Tarefa 'taskFactory': Verificando o certificado X.509 do servidor...");
        if((flags = mbedtls_ssl_get_verify_result(&ssl)) != 0)
        {
            char vrfy_buf[512];
            printf(" Falha.\n");
            mbedtls_x509_crt_verify_info(vrfy_buf, sizeof(vrfy_buf), "  ! ", flags);
            printf("%s\n", vrfy_buf);
        }
        else printf(" OK.\n");
    
			
        printf("Tarefa 'taskFactory': Montando o texto JSON ...");
		iJsonUncrypted = sprintf((char *) buffer1, 
				JSON_FACTORY_SCHEMA,
				"2018-01-01T13:38:00",					//Manufacturing_Date
				1,										//Lote_Number
				"SERIAL123",                            //Serial_Number
				1,                                      //Factory
				"SMD123",                               //SMD_Line
				sdk_system_get_chip_id(),               //Chip_ID
				sdk_spi_flash_get_id(),                 //Flash_ID
				getMCADD()                              //MacAddress
				);
		printf("\n\nCONTEUDO ENVIO (SEM CRIPT):\n\n %s\n\n", buffer1); 
		
		
		
		
		
		//memcpy(JsonUncrypted, "Testando...", 11);
		
        
        //Escrever a requisi��o no servidor
        printf("Tarefa 'taskFactory': Criptografar JSON ...");
		//JsonCrypted = (encrypt(JsonUncrypted, oGlobalKey, oGlobalIV));
		int iSize = vEncrypt(&buffer2, buffer1, globalKey, globalIV);
		buffer2[iSize] = 0;
		
		
        //Escrever a requisi��o no servidor
        printf("Tarefa 'taskFactory': Escrevendo no servidor ...");
		bzero(buffer1, 1024);
		len = sprintf((char *) buffer1, 
				POSTMASK,
				"https://wisehomefactory.hereit.com.br/api/device_manufacturing/Register/",
				"wisehomefactory.hereit.com.br",
				"MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTI=",
				"MTIzNDU2Nzg5MDEyMzQ1Ng==",
				strlen(buffer2),
				buffer2
				);
		printf("\n\nCONTEUDO ENVIO (COM CRIPT):\n\n %s\n\n", buffer1); 
    
        while((ret = mbedtls_ssl_write(&ssl, buffer1, len)) <= 0)
        {
            if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                printf(" Falha.\n  ! mbedtls_ssl_write retornou o erro %d\n\n", ret);
                goto taskFactory_exit;
            }
        }
    
        len = ret;
        printf(" OK.\n%d bytes escritos\n", len);
    
		
		
        //Ler a resposta HTTP
        printf("Tarefa 'taskFactory': Lendo a resposta do servidor ...");
		
		//struct communication_t resposta = (struct communication_t)zalloc(sizeof(struct communication_t));
		
        //do
        //{
            bzero(buffer1, 1024);
			len = 1023;
			//len = sizeof(buffer1) - 1;
            //memset(buffer1, 0, sizeof(buffer1));
            ret = mbedtls_ssl_read(&ssl, buffer1, len);
    
            if(!(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE))
            {
				if(ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) {
					ret = 0;
					goto taskFactory_exit;
				}
    
				if(ret < 0)
				{
					printf("Falha.\n  ! mbedtls_ssl_read retornou o erro %d\n\n", ret);
					goto taskFactory_exit;
				}
    
				if(ret == 0)
				{
					//goto taskFactory_exit;
				}
			}
            len = ret;
			
			
            printf(" OK.\n%d bytes lidos\n", len);
            printf("\nCONTEUDO RECEBIDO (COMPLETO):\n\n%s", (char *) buffer1);
			
			wait(1000);
			//Decriptografar mensagem
			
			bzero(buffer2, 1024);
			iPos = position((char *)buffer1, "[");
			i = 0;
			iVirg = 0;
			if (iPos>0)
			{
				for (i = 0; i < (len - iPos); i++)
				{
					if (buffer1[(iPos + i)] == ',') iVirg = i;
					buffer2[i] = buffer1[(iPos + i)];
					
					//if ((i >= 2)&&(i <= 4)) resultCode[(i - 2)] = buffer1[(iPos + i)];
					//else if ((i >= 8)&&(i <= 14)) resultDesc[(i - 8)] = buffer1[(iPos + i)];
				}
				
				//printf("\n\nCODIGO: %s RESULTADO: %s ", resultCode, resultDesc);
				
				//printf("\n\nCONTEUDO UTIL: %s\n\n", Teste66);
				
				tokens = str_split(buffer2, ',');

				if (tokens)
				{
					for (i = 0; *(tokens + i); i++)
					{
						//printf("{%d}array=[%s]\n", i, *(tokens + i));
						
						if (i==2)
						{
							bzero(buffer1, 1024);
							sprintf(buffer1, "%s", *(tokens + i));
							if (buffer1)
							{
								buffer1 = substring(buffer1, 1, (strlen(buffer1)-3));
								printf("\n\n\nJsonCrypted: %s\n\n\n", buffer1);
								
								//JsonUncrypted2 = decrypt(JsonCrypted, key, iv);
								bzero(buffer2, 1024);
								vDecrypt(&buffer2, buffer1, oGlobalKey, oGlobalIV);
								
								if (buffer2) 
								{
									printf("\n\n\nJsonUncrypted2: %s\n\n\n", buffer2);
									
									desserializar_DeviceMemo(&localDevice, buffer2);
									localDevice.device_state = '0';
									set_DeviceMemo(localDevice);
									
									//taskGravarConfig(NULL);
									//set_Flag_RequisitarGravacao(1);
								}
							}
						}
						
						free(*(tokens + i));
					}
					//printf("\n");
					free(tokens);
					
				}
				
				//printf("ENTROU!!!!");
			}
			
			
        //} while(1);
        
		//Decriptografar mensagem
		//printf("SIZE: %d\n", strlen(buffer1));
		//char *Teste66 = strstr((char *)buffer1, "\n\n");
		//printf("SOMENTE DADOS UTEIS: %s\n\n\n", strstr((char *)buffer1, "\n\n") );
			
		
taskFactory_exit:
		
		mbedtls_ssl_close_notify(&ssl);
		mbedtls_ssl_session_reset(&ssl);
		mbedtls_net_free(&server_fd);
		
		free(buffer1);
		free(buffer2);
		free(oGlobalKey);
		free(oGlobalIV);
		
		
		//set_Flag_Reiniciar(1);
		vTaskDelete(taskFactory_hdl);
}


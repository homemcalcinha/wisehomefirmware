#include "stringLib.h"
#include <assert.h>

/* ************************************************************************************************* */
// Função			: left
// Objetivo			: Retorna a parte esquerda de uma string de acordo com o número de caracteres informado
// Parâmetros		: char *origem, int iCh
// Retorno			: char *
/* ************************************************************************************************* */

char *left(const char* origem, int iCh)
{
	char *retorno;
	
	if (iCh >= strlen(origem))
	{
		retorno	= (char*)malloc(strlen(origem)+1);
		strcpy(retorno, origem);
		return retorno;
	}
	
	retorno	= (char*)malloc(iCh+1);
	for (int leftI = 0; leftI < iCh; leftI++)
	{
		retorno[leftI] = origem[leftI];
	}
	
	retorno[iCh] = '\0';
	return retorno;
}

/* ************************************************************************************************* */
// Função			: right
// Objetivo			: Retorna a parte direira de uma string de acordo com o número de caracteres informado
// Parâmetros		: char *origem, int iCh
// Retorno			: char *
/* ************************************************************************************************* */

char *right(const char* origem, int iCh)
{
	char *retorno;
	
	if (iCh >= strlen(origem))
	{
		retorno	= (char*)malloc(strlen(origem)+1);
		strcpy(retorno, origem);
		return retorno;
	}
	
	retorno	= (char*)malloc(iCh+1);
	for (int rightI = (strlen(origem)-iCh); rightI < strlen(origem); rightI++)
	{
		retorno[(rightI - (strlen(origem)-iCh))] = origem[rightI];
	}
	retorno[iCh] = '\0';
	return retorno;
}

/* ************************************************************************************************* */
// Função			: substring
// Objetivo			: Retorna uma sequencia de caracteres de acordo com o início e a quantidade de caracteres informado
// Parâmetros		: char *origem, int iStart, int iCount
// Retorno			: char *
/* ************************************************************************************************* */

char *substring(const char* origem, int iStart, int iCount)
{
	char *retorno;
	
	if ((iStart+iCount) > strlen(origem))
	{
		retorno	= (char*)malloc(1);
		retorno[0] = '\0';
		return retorno;
	}
	
	retorno	= (char*)malloc(iCount+1);
	
	for (int iCh = iStart; iCh < (iStart+iCount); iCh++)
	{
		retorno[(iCh-iStart)] = origem[iCh];
	}
	
	
	retorno[iCount] = '\0';
	return retorno;
}

/* ************************************************************************************************* */
// Função			: vSubstring
// Objetivo			: Retorna uma sequencia de caracteres de acordo com o início e a quantidade de caracteres informado
// Parâmetros		: char *origem, int iStart, int iCount
// Retorno			: char *
/* ************************************************************************************************* */

void vSubstring(const char *origem, char **destino, int iStart, int iCount)
{
printf("ORIGEM: %s\n", origem);

print_hex(origem, sizeof(origem));


	if (iCount <= 0)
	{
printf("vSubstring 1\n");
		return;
	}
printf("vSubstring 2\n");
	int iTam = (strlen(origem));
printf("vSubstring 3 (iTam = %d)\n", iTam);
	char *retorno = (char *)malloc(iTam+1);
printf("vSubstring 4\n");
	bzero(retorno, iTam+1);
	
printf("vSubstring 5\n");
	memcpy(*retorno, origem, iTam);
printf("vSubstring 6\n");
	bzero(*destino, sizeof(*destino));
	
printf("vSubstring 7\n");
	if ((iStart+iCount) > strlen(origem))
	{
printf("vSubstring 8\n");
		memcpy(*destino, retorno, strlen(retorno) );
printf("vSubstring 9\n");
		free(retorno);
printf("vSubstring 10\n");
		return;
	}
	
printf("vSubstring 11\n");
	
	for (int iCh = iStart; iCh < (iStart+iCount); iCh++)
	{
		retorno[(iCh-iStart)] = origem[iCh];
	}
	retorno[iCount] = 0;
	
	memcpy(*destino, retorno, strlen(retorno) );
	
	free(retorno);
	return;
}

/* ************************************************************************************************* */
// Função			: position
// Objetivo			: Retorna a posicao de um determinado texto dentro de outra string
// Parâmetros		: char *texto, char *procura
// Retorno			: int
/* ************************************************************************************************* */

int position(const char *texto, const char *procura)
{
	
	char *tmp = strstr(texto, procura);	
	
	if (!tmp)
		return -1;
	else return (strlen(texto)-strlen(tmp));
	
}

int chPosition(const char *texto, const char cCh)
{
	
	char *tmp = strchr(texto, cCh);	
	
	if (!tmp)
		return -1;
	else return (int)(tmp - texto);
	
}

/* ************************************************************************************************* */
// Função			: append
// Objetivo			: Concatena duas strings
// Parâmetros		: char *texto1, char *texto2
// Retorno			: char *
/* ************************************************************************************************* */

char *append(const char *texto1, const char* texto2)
{
	char *retorno;
	retorno	= (char*)malloc(strlen(texto1)+strlen(texto2)+1);
	
	strcpy(retorno, texto1);
	strcat(retorno, texto2);
	
	retorno[(strlen(texto1)+strlen(texto2)+1)] = '\0';
	return retorno;
}

/* ************************************************************************************************* */
// Função			: insert
// Objetivo			: insere uma sequencia de caracteres em outra string na posicao informada
// Parâmetros		: char *texto, char *novo, int iPos
// Retorno			: char *
/* ************************************************************************************************* */

char *insert(const char *texto, const char *novo, int iPos)
{
	char *retorno;
	
	if (iPos > strlen(texto)) return '\0';
	
	if (iPos == strlen(texto)) return append(texto, novo);
	
	retorno	= (char*)malloc(strlen(texto)+strlen(novo)+1);
	
	strcpy(retorno, left(texto, iPos));
	strcat(retorno, novo);
	strcat(retorno, right(texto, (strlen(texto)-iPos) ));
	
	return retorno;
}

/* ************************************************************************************************* */
// Função			: replace
// Objetivo			: Substritui uma sequencia de caracteres por outra em uma determinada string 
// Parâmetros		: char *texto, char *antigo, char *novo
// Retorno			: char *
/* ************************************************************************************************* */

char *replace(const char* texto, const char *antigo, const char *novo)
{
	char *retorno;
	char *direita;
	char *buffer;
	int i = 0;

	direita = (char *)malloc(strlen(texto));
	retorno = (char *)malloc(strlen(texto));
	buffer = (char *)malloc(strlen(texto));
	
	strcpy(buffer, texto);
	retorno[0] = '\0';
	
	direita = strstr(buffer, antigo);
	
	if (!direita)
	{
		strcpy(retorno, buffer);
		return retorno;
	}
	
	while(direita)
	{
		strcpy(retorno, (left(buffer, (strlen(buffer) - strlen(direita)) )));
		
		if (strlen(direita)==strlen(antigo))
		{
			return retorno;
		}
		strcat(retorno, novo);
		strcat(retorno, substring(direita, (strlen(antigo)), (strlen(direita)-strlen(antigo)) ));
		
		strcpy(buffer, retorno);
		direita = strstr(buffer, antigo);
		i++;
		
	}

	return retorno;
}


/* ************************************************************************************************* */
// Função			: compare
// Objetivo			: Compara duas strings e retorna se é menor, igual ou maior
// Parâmetros		: char *texto1, char *texto2
// Retorno			: int (texto1 < texto2 => -1 // texto1 == texto2 => 0 // texto1 > texto2 => 1)
/* ************************************************************************************************* */

int compare(const char *texto1, const char *texto2)
{
	int iTam1 = strlen(texto1);
	int iTam2 = strlen(texto2);
	int iTotal = iTam1;
	
	if (iTam1 == 0 && iTam2 > 0) return -1;
	if (iTam2 == 0 && iTam1 > 0) return  1;
	
	if (strcmp(texto1, texto2) == 0) return 0;
	
	//Armazena menor tamanho
	if (iTam2 < iTam1) iTotal = iTam2;
	
	for (int i = 0; i < iTotal; i++)
	{
		if (texto1[i]<texto2[i]) return -1;
		if (texto1[i]>texto2[i]) return  1;
	}
	
	return (iTam1 < iTam2 ) ? -1 : 1;
	
}

bool startswith(const char *texto, const char *starts)
{
	if ((strlen(starts))>(strlen(texto))) return false;
	
	return (position(texto, starts) == 0) ? true : false;
}

bool endswith(const char *texto, const char *ends)
{
	if ((strlen(ends))>(strlen(texto))) return false;
	
	return (compare( (right(texto, strlen(ends))), ends) == 0) ? true : false;
}

char *between(const char *texto, const char *chStart, char const *chEnd)
{
	char *retorno;
	char *buffer1;
	char *buffer2;
	retorno = (char *)malloc(strlen(texto));
	buffer1 = (char *)malloc(strlen(texto));
	buffer2 = (char *)malloc(strlen(texto));
	
	buffer1 = strstr(texto, chStart);
	if (!buffer1) 
	{
		strcpy(retorno, "");
		return retorno;
	}
	
	buffer2 = strstr(buffer1, chEnd);
	if (!buffer2) 
	{
		strcpy(retorno, "");
		return retorno;
	}

	retorno = substring(buffer1, 1, ((strlen(buffer1)) - (strlen(buffer2)) - 1));
	
	return retorno;
}

char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}

char *trim(const char *texto){
    int i,j=0;
    char temp[strlen(texto)];
    
    // lado esquerdo
    for(i=0;i<strlen(texto) && texto[i]==' ';i++);
    strncpy(temp, texto+i, strlen(texto));
 
    // lado direito
    for(i=strlen(texto);texto[i]==' ' || texto[i]=='\0';i--,j++);
    temp[strlen(temp)-j+1] = '\0';
    
    texto = temp;
    return texto;
}

char *remove_spaces(char *texto)
{

	int i, j;
	char *saida;

	j = 0;
	saida = malloc(strlen(texto)+1);
	
	for (i = 0; texto[i]; i++)
		if (texto[i] != ' ')
		     saida[j++] = texto[i];
	
	saida[j] = 0;

	return saida;	
}

void* memcpy_index(void *s1, const void *s2, size_t index, size_t n) {
  s2 = ((char*)s2)+index;
  return memcpy(s1, s2,n);
}
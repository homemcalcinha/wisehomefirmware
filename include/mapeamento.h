static void setDevice(device_t *DV, char *sNome, int iEntsai, bool invert)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			strcpy((*DV).nome, sNome);										//Nome da porta
			(*DV).entsai					= iEntsai;						//0 = Entrada; 1 = Saída; 2 = Entrada/Saída
			(*DV).digital_value				= false;						//Valor digital corrente da entrada / saída
			(*DV).digital_before			= false;						//Valor digital anterior da entrada / saída
			(*DV).digital_invert			= invert;						//Configura a inversão dos valores da porta
			(*DV).analog_value				= 0;							//Valor analógico corrente da entrada/saída
			(*DV).analog_before				= 0;							//Valor analógico anterior da entrada/saída
			(*DV).evt_on					= false;						//dispara o evento ON
			(*DV).evt_off					= false;						//dispara o evento OFF
			(*DV).evt_change				= false;						//dispara o evento CHANGE
			(*DV).evt_click_up				= false;						//dispara o evento CLICK UP
			(*DV).evt_click_down			= false;						//dispara o evento CLICK DOWN

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void writeDigitalOutput()
{
	//printf(" writeDigitalOutput -> INICIO EM : %d ms\n", xTaskGetTickCount());

		gpio_write(pinSDLatch, LOW);

		//SD6 - S1 - QH
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD6.digital_value ^ SD6.digital_invert ));
		gpio_write(pinSDClock, HIGH);

		//SD8 - S2 - QG
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD8.digital_value ^ SD8.digital_invert ));
		gpio_write(pinSDClock, HIGH);

		//SD7 - S3 - QF
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD7.digital_value ^ SD7.digital_invert ));
		gpio_write(pinSDClock, HIGH);

		//SD5 - LIVRE - QE
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD5.digital_value ^ SD5.digital_invert ));
		gpio_write(pinSDClock, HIGH);

		//SD3 - RELE 3 - QD
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD3.digital_value ^ SD3.digital_invert ));
		gpio_write(pinSDClock, HIGH);

		//SD4 - LIVRE - QC
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD4.digital_value ^ SD4.digital_invert ));
		gpio_write(pinSDClock, HIGH);


		//SD1 - RELE 1 - QB
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD1.digital_value ^ SD1.digital_invert ));
		gpio_write(pinSDClock, HIGH);


		//SD2 - RELE 2 - QA
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDData, (SD2.digital_value ^ SD2.digital_invert ));
		gpio_write(pinSDClock, HIGH);



		//finaliza o fluxo dos dados e envia os 8 bits ao 74595.
		//Ao mesmo tempo, abre leitura no 74165
		gpio_write(pinSDLatch, HIGH);

		//ENTRADAS DIGITAIS

		//ED8 - E3 - H
		ED8.digital_value = (gpio_read(pinEDData) ^ ED8.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);

		//ED7 - E2 - G
		ED7.digital_value = (gpio_read(pinEDData) ^ ED7.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);

		//ED6 - E1 - F
		ED6.digital_value = (gpio_read(pinEDData) ^ ED6.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);

		//ED4 - TRIAC 1 - E
		ED4.digital_value = (gpio_read(pinEDData) ^ ED4.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);

		//ED5 - TRIAC 2 - D
		ED5.digital_value = (gpio_read(pinEDData) ^ ED5.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);

		//ED2 - RELE 2 - C
		ED2.digital_value = (gpio_read(pinEDData) ^ ED2.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);

		//ED3 - RELE 3 - B
		ED3.digital_value = (gpio_read(pinEDData) ^ ED3.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);

		//ED1 - RELE 1 - A
		ED1.digital_value = (gpio_read(pinEDData) ^ ED1.digital_invert );
		gpio_write(pinSDClock, LOW);
		gpio_write(pinSDClock, HIGH);


}


static void seekAnalogInput(uint8_t iAI)
{
	switch (iAI)
	{
		case 1:
			SD6.digital_value = false;
			SD8.digital_value = false;
			SD7.digital_value = false;
			writeDigitalOutput();
			break;
		case 2:
			SD6.digital_value = false;
			SD8.digital_value = false;
			SD7.digital_value = true;
			writeDigitalOutput();
			break;
		case 3:
			SD6.digital_value = true;
			SD8.digital_value = true;
			SD7.digital_value = true;
			writeDigitalOutput();
			break;
		case 4:
			SD6.digital_value = false;
			SD8.digital_value = true;
			SD7.digital_value = true;
			writeDigitalOutput();
			break;
		case 5:
			SD6.digital_value = true;
			SD8.digital_value = false;
			SD7.digital_value = false;
			writeDigitalOutput();
			break;
		case 6:
			SD6.digital_value = true;
			SD8.digital_value = false;
			SD7.digital_value = true;
			writeDigitalOutput();
			break;
		case 7:
			SD6.digital_value = false;
			SD8.digital_value = true;
			SD7.digital_value = false;
			writeDigitalOutput();
			break;
		case 8:
			SD6.digital_value = true;
			SD8.digital_value = true;
			SD7.digital_value = false;
			writeDigitalOutput();
			break;
	}
}

static void refreshAnalogs()
{
	//Entrada Analógica - Leitura 1 - A0 [SD6=0 / SD8=0 / SD7=0] - Triac 1
	SD6.digital_value = false;
	SD8.digital_value = false;
	SD7.digital_value = false;
	writeDigitalOutput();
	EA1.analog_value = sdk_system_adc_read();


	//Entrada Analógica - Leitura 2 - A1 [SD6=0 / SD8=0 / SD7=1] - Porta Analógica disponivel
	SD6.digital_value = false;
	SD8.digital_value = false;
	SD7.digital_value = true;
	writeDigitalOutput();
	EA2.analog_value = sdk_system_adc_read();

	//Entrada Analógica - Leitura 7 - A2 [SD6=0 / SD8=1 / SD7=0] - Sensor Temperatura 1
	SD6.digital_value = false;
	SD8.digital_value = true;
	SD7.digital_value = false;
	writeDigitalOutput();
	EA7.analog_value = sdk_system_adc_read();

	//Entrada Analógica - Leitura 4 - A3 [SD6=0 / SD8=1 / SD7=1] - Rele 1
	SD6.digital_value = false;
	SD8.digital_value = true;
	SD7.digital_value = true;
	writeDigitalOutput();
	EA4.analog_value = sdk_system_adc_read();

	//Entrada Analógica - Leitura 5 - A4 [SD6=1 / SD8=0 / SD7=0] - Rele 3
	SD6.digital_value = true;
	SD8.digital_value = false;
	SD7.digital_value = false;
	writeDigitalOutput();
	EA5.analog_value = sdk_system_adc_read();

	//Entrada Analógica - Leitura 6 - A5 [SD6=1 / SD8=0 / SD7=1] - Rele 2
	SD6.digital_value = true;
	SD8.digital_value = false;
	SD7.digital_value = true;
	writeDigitalOutput();
	EA6.analog_value = sdk_system_adc_read();

	//Entrada Analógica - Leitura 8 - A6 [SD6=1 / SD8=1 / SD7=0] - Sensor Temperatura 2
	SD6.digital_value = true;
	SD8.digital_value = true;
	SD7.digital_value = false;
	writeDigitalOutput();
	EA8.analog_value = sdk_system_adc_read();


	//Entrada Analógica - Leitura 3 - A7 [SD6=1 / SD8=1 / SD7=1] - Triac 2
	SD6.digital_value = true;
	SD8.digital_value = true;
	SD7.digital_value = true;
	writeDigitalOutput();
	EA3.analog_value = sdk_system_adc_read();

}



static void dispathEvents()
{
	//********** EVT_ON ***********
	if ((ED1.digital_value != ED1.digital_before) && (ED1.digital_value)) ED1.evt_on = true;
	if ((ED2.digital_value != ED2.digital_before) && (ED2.digital_value)) ED2.evt_on = true;
	if ((ED3.digital_value != ED3.digital_before) && (ED3.digital_value)) ED3.evt_on = true;
	if ((ED4.digital_value != ED4.digital_before) && (ED4.digital_value)) ED4.evt_on = true;
	if ((ED5.digital_value != ED5.digital_before) && (ED5.digital_value)) ED5.evt_on = true;
	if ((ED6.digital_value != ED6.digital_before) && (ED6.digital_value)) ED6.evt_on = true;
	if ((ED7.digital_value != ED7.digital_before) && (ED7.digital_value)) ED7.evt_on = true;
	if ((ED8.digital_value != ED8.digital_before) && (ED8.digital_value)) ED8.evt_on = true;

	if ((SD1.digital_value != SD1.digital_before) && (SD1.digital_value)) SD1.evt_on = true;
	if ((SD2.digital_value != SD2.digital_before) && (SD2.digital_value)) SD2.evt_on = true;
	if ((SD3.digital_value != SD3.digital_before) && (SD3.digital_value)) SD3.evt_on = true;
	if ((SD4.digital_value != SD4.digital_before) && (SD4.digital_value)) SD4.evt_on = true;
	if ((SD5.digital_value != SD5.digital_before) && (SD5.digital_value)) SD5.evt_on = true;
	//if ((SD6.digital_value != SD6.digital_before) && (SD6.digital_value)) SD6.evt_on = true;
	//if ((SD7.digital_value != SD7.digital_before) && (SD7.digital_value)) SD7.evt_on = true;
	//if ((SD8.digital_value != SD8.digital_before) && (SD8.digital_value)) SD8.evt_on = true;

	//********** EVT_OFF ***********
	if ((ED1.digital_value != ED1.digital_before) && (!ED1.digital_value)) ED1.evt_off = true;
	if ((ED2.digital_value != ED2.digital_before) && (!ED2.digital_value)) ED2.evt_off = true;
	if ((ED3.digital_value != ED3.digital_before) && (!ED3.digital_value)) ED3.evt_off = true;
	if ((ED4.digital_value != ED4.digital_before) && (!ED4.digital_value)) ED4.evt_off = true;
	if ((ED5.digital_value != ED5.digital_before) && (!ED5.digital_value)) ED5.evt_off = true;
	if ((ED6.digital_value != ED6.digital_before) && (!ED6.digital_value)) ED6.evt_off = true;
	if ((ED7.digital_value != ED7.digital_before) && (!ED7.digital_value)) ED7.evt_off = true;
	if ((ED8.digital_value != ED8.digital_before) && (!ED8.digital_value)) ED8.evt_off = true;

	if ((SD1.digital_value != SD1.digital_before) && (!SD1.digital_value)) SD1.evt_off = true;
	if ((SD2.digital_value != SD2.digital_before) && (!SD2.digital_value)) SD2.evt_off = true;
	if ((SD3.digital_value != SD3.digital_before) && (!SD3.digital_value)) SD3.evt_off = true;
	if ((SD4.digital_value != SD4.digital_before) && (!SD4.digital_value)) SD4.evt_off = true;
	if ((SD5.digital_value != SD5.digital_before) && (!SD5.digital_value)) SD5.evt_off = true;
	//if ((SD6.digital_value != SD6.digital_before) && (!SD6.digital_value)) SD6.evt_off = true;
	//if ((SD7.digital_value != SD7.digital_before) && (!SD7.digital_value)) SD7.evt_off = true;
	//if ((SD8.digital_value != SD8.digital_before) && (!SD8.digital_value)) SD8.evt_off = true;

	//********** EVT_CHANGE ***********
	if (ED1.digital_value != ED1.digital_before) ED1.evt_change = true;
	if (ED2.digital_value != ED2.digital_before) ED2.evt_change = true;
	if (ED3.digital_value != ED3.digital_before) ED3.evt_change = true;
	if (ED4.digital_value != ED4.digital_before) ED4.evt_change = true;
	if (ED5.digital_value != ED5.digital_before) ED5.evt_change = true;
	if (ED6.digital_value != ED6.digital_before) ED6.evt_change = true;
	if (ED7.digital_value != ED7.digital_before) ED7.evt_change = true;
	if (ED8.digital_value != ED8.digital_before) ED8.evt_change = true;

	if (SD1.digital_value != SD1.digital_before) SD1.evt_change = true;
	if (SD2.digital_value != SD2.digital_before) SD2.evt_change = true;
	if (SD3.digital_value != SD3.digital_before) SD3.evt_change = true;
	if (SD4.digital_value != SD4.digital_before) SD4.evt_change = true;
	if (SD5.digital_value != SD5.digital_before) SD5.evt_change = true;
	//if (SD6.digital_value != SD6.digital_before) SD6.evt_change = true;
	//if (SD7.digital_value != SD7.digital_before) SD7.evt_change = true;
	//if (SD8.digital_value != SD8.digital_before) SD8.evt_change = true;

	//********** EVT_CLICK_UP ***********
	if ((ED1.digital_value != ED1.digital_before) && (ED1.digital_value)) ED1.evt_click_up = true;
	if ((ED2.digital_value != ED2.digital_before) && (ED2.digital_value)) ED2.evt_click_up = true;
	if ((ED3.digital_value != ED3.digital_before) && (ED3.digital_value)) ED3.evt_click_up = true;
	if ((ED4.digital_value != ED4.digital_before) && (ED4.digital_value)) ED4.evt_click_up = true;
	if ((ED5.digital_value != ED5.digital_before) && (ED5.digital_value)) ED5.evt_click_up = true;
	if ((ED6.digital_value != ED6.digital_before) && (ED6.digital_value)) ED6.evt_click_up = true;
	if ((ED7.digital_value != ED7.digital_before) && (ED7.digital_value)) ED7.evt_click_up = true;
	if ((ED8.digital_value != ED8.digital_before) && (ED8.digital_value)) ED8.evt_click_up = true;

	//********** EVT_CLICK_DOWN ***********
	if ((ED1.digital_value != ED1.digital_before) && (!ED1.digital_value)) ED1.evt_click_down = true;
	if ((ED2.digital_value != ED2.digital_before) && (!ED2.digital_value)) ED2.evt_click_down = true;
	if ((ED3.digital_value != ED3.digital_before) && (!ED3.digital_value)) ED3.evt_click_down = true;
	if ((ED4.digital_value != ED4.digital_before) && (!ED4.digital_value)) ED4.evt_click_down = true;
	if ((ED5.digital_value != ED5.digital_before) && (!ED5.digital_value)) ED5.evt_click_down = true;
	if ((ED6.digital_value != ED6.digital_before) && (!ED6.digital_value)) ED6.evt_click_down = true;
	if ((ED7.digital_value != ED7.digital_before) && (!ED7.digital_value)) ED7.evt_click_down = true;
	if ((ED8.digital_value != ED8.digital_before) && (!ED8.digital_value)) ED8.evt_click_down = true;


	//Atualizar valores
	ED1.digital_before = ED1.digital_value;
	ED2.digital_before = ED2.digital_value;
	ED3.digital_before = ED3.digital_value;
	ED4.digital_before = ED4.digital_value;
	ED5.digital_before = ED5.digital_value;
	ED6.digital_before = ED6.digital_value;
	ED7.digital_before = ED7.digital_value;
	ED8.digital_before = ED8.digital_value;

	SD1.digital_before = SD1.digital_value;
	SD2.digital_before = SD2.digital_value;
	SD3.digital_before = SD3.digital_value;
	SD4.digital_before = SD4.digital_value;
	SD5.digital_before = SD5.digital_value;
	SD6.digital_before = SD6.digital_value;
	SD7.digital_before = SD7.digital_value;
	SD8.digital_before = SD8.digital_value;

}

static void executarEventos()
{
	//********** EVT_ON         ***********
	if (ED1.evt_on)
	{
		printf("taskEventos => EVT_ON em ED1\n");
		ED1.evt_on = false;
	}
	if (ED2.evt_on)
	{
		printf("taskEventos => EVT_ON em ED2\n");
		ED2.evt_on = false;
	}
	if (ED3.evt_on)
	{
		printf("taskEventos => EVT_ON em ED3\n");
		ED3.evt_on = false;
	}
	if (ED4.evt_on)
	{
		printf("taskEventos => EVT_ON em ED4\n");
		ED4.evt_on = false;
	}
	if (ED5.evt_on)
	{
		printf("taskEventos => EVT_ON em ED5\n");
		ED5.evt_on = false;
	}
	if (ED6.evt_on)
	{
		printf("taskEventos => EVT_ON em ED6\n");
		ED6.evt_on = false;
	}
	if (ED7.evt_on)
	{
		printf("taskEventos => EVT_ON em ED7\n");
		ED7.evt_on = false;
	}
	if (ED8.evt_on)
	{
		printf("taskEventos => EVT_ON em ED8\n");
		ED8.evt_on = false;
	}

	if (SD1.evt_on)
	{
		printf("taskEventos => EVT_ON em SD1\n");
		SD1.evt_on = false;
	}
	if (SD2.evt_on)
	{
		printf("taskEventos => EVT_ON em SD2\n");
		SD2.evt_on = false;
	}
	if (SD3.evt_on)
	{
		printf("taskEventos => EVT_ON em SD3\n");
		SD3.evt_on = false;
	}
	if (SD4.evt_on)
	{
		printf("taskEventos => EVT_ON em SD4\n");
		SD4.evt_on = false;
	}
	if (SD5.evt_on)
	{
		printf("taskEventos => EVT_ON em SD5\n");
		SD5.evt_on = false;
	}
	if (SD6.evt_on)
	{
		printf("taskEventos => EVT_ON em SD6\n");
		SD6.evt_on = false;
	}
	if (SD7.evt_on)
	{
		printf("taskEventos => EVT_ON em SD7\n");
		SD7.evt_on = false;
	}
	if (SD8.evt_on)
	{
		printf("taskEventos => EVT_ON em SD8\n");
		SD8.evt_on = false;
	}


	//********** EVT_OFF        ***********
	if (ED1.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED1\n");
		ED1.evt_off = false;
	}
	if (ED2.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED2\n");
		ED2.evt_off = false;
	}
	if (ED3.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED3\n");
		ED3.evt_off = false;
	}
	if (ED4.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED4\n");
		ED4.evt_off = false;
	}
	if (ED5.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED5\n");
		ED5.evt_off = false;
	}
	if (ED6.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED6\n");
		ED6.evt_off = false;
	}
	if (ED7.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED7\n");
		ED7.evt_off = false;
	}
	if (ED8.evt_off)
	{
		printf("taskEventos => EVT_OFF em ED8\n");
		ED8.evt_off = false;
	}

	if (SD1.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD1\n");
		SD1.evt_off = false;
	}
	if (SD2.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD2\n");
		SD2.evt_off = false;
	}
	if (SD3.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD3\n");
		SD3.evt_off = false;
	}
	if (SD4.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD4\n");
		SD4.evt_off = false;
	}
	if (SD5.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD5\n");
		SD5.evt_off = false;
	}
	if (SD6.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD6\n");
		SD6.evt_off = false;
	}
	if (SD7.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD7\n");
		SD7.evt_off = false;
	}
	if (SD8.evt_off)
	{
		printf("taskEventos => EVT_OFF em SD8\n");
		SD8.evt_off = false;
	}



	//********** EVT_CHANGE     ***********
	if (ED1.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED1\n");
		ED1.evt_change = false;
		//Alterar estado do SD1
		SD1.digital_value = !SD1.digital_value;
	}
	if (ED2.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED2\n");
		ED2.evt_change = false;
		//Alterar estado do SD2
		SD2.digital_value = !SD2.digital_value; //keko
	}
	if (ED3.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED3\n");
		ED3.evt_change = false;
		//Alterar estado do SD3
		SD3.digital_value = !SD3.digital_value;
	}
	if (ED4.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED4\n");
		ED4.evt_change = false;
	}
	if (ED5.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED5\n");
		ED5.evt_change = false;
	}
	if (ED6.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED6\n");
		ED6.evt_change = false;
	}
	if (ED7.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED7\n");
		ED7.evt_change = false;
	}
	if (ED8.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em ED8\n");
		ED8.evt_change = false;
	}

	if (SD1.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD1\n");
		SD1.evt_change = false;
	}
	if (SD2.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD2\n");
		SD2.evt_change = false;
	}
	if (SD3.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD3\n");
		SD3.evt_change = false;
	}
	if (SD4.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD4\n");
		SD4.evt_change = false;
	}
	if (SD5.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD5\n");
		SD5.evt_change = false;
	}
	if (SD6.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD6\n");
		SD6.evt_change = false;
	}
	if (SD7.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD7\n");
		SD7.evt_change = false;
	}
	if (SD8.evt_change)
	{
		printf("taskEventos => EVT_CHANGE em SD8\n");
		SD8.evt_change = false;
	}



	//********** EVT_CLICK_UP   ***********
	if (ED1.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED1\n");
		ED1.evt_click_up = false;

		//Alterar estado do SD1
		//SD1.digital_value = !SD1.digital_value;
	}
	if (ED2.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED2\n");
		ED2.evt_click_up = false;

		//Alterar estado do SD2
		//SD2.digital_value = !SD2.digital_value;
	}
	if (ED3.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED3\n");
		ED3.evt_click_up = false;

		//Alterar estado do SD3
		//SD3.digital_value = !SD3.digital_value;
	}
	if (ED4.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED4\n");
		ED4.evt_click_up = false;
	}
	if (ED5.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED5\n");
		ED5.evt_click_up = false;
	}
	if (ED6.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED6\n");
		ED6.evt_click_up = false;
	}
	if (ED7.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED7\n");
		ED7.evt_click_up = false;
	}
	if (ED8.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em ED8\n");
		ED8.evt_click_up = false;
	}

	if (SD1.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD1\n");
		SD1.evt_click_up = false;
	}
	if (SD2.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD2\n");
		SD2.evt_click_up = false;
	}
	if (SD3.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD3\n");
		SD3.evt_click_up = false;
	}
	if (SD4.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD4\n");
		SD4.evt_click_up = false;
	}
	if (SD5.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD5\n");
		SD5.evt_click_up = false;
	}
	if (SD6.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD6\n");
		SD6.evt_click_up = false;
	}
	if (SD7.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD7\n");
		SD7.evt_click_up = false;
	}
	if (SD8.evt_click_up)
	{
		printf("taskEventos => EVT_CLICK_UP em SD8\n");
		SD8.evt_click_up = false;
	}



	//********** EVT_CLICK_DOWN ***********
	if (ED1.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED1\n");
		ED1.evt_click_down = false;
	}
	if (ED2.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED2\n");
		ED2.evt_click_down = false;
	}
	if (ED3.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED3\n");
		ED3.evt_click_down = false;
	}
	if (ED4.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED4\n");
		ED4.evt_click_down = false;
	}
	if (ED5.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED5\n");
		ED5.evt_click_down = false;
	}
	if (ED6.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED6\n");
		ED6.evt_click_down = false;
	}
	if (ED7.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED7\n");
		ED7.evt_click_down = false;
	}
	if (ED8.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em ED8\n");
		ED8.evt_click_down = false;
	}

	if (SD1.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD1\n");
		SD1.evt_click_down = false;
	}
	if (SD2.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD2\n");
		SD2.evt_click_down = false;
	}
	if (SD3.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD3\n");
		SD3.evt_click_down = false;
	}
	if (SD4.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD4\n");
		SD4.evt_click_down = false;
	}
	if (SD5.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD5\n");
		SD5.evt_click_down = false;
	}
	if (SD6.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD6\n");
		SD6.evt_click_down = false;
	}
	if (SD7.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD7\n");
		SD7.evt_click_down = false;
	}
	if (SD8.evt_click_down)
	{
		printf("taskEventos => EVT_CLICK_DOWN em SD8\n");
		SD8.evt_click_down = false;
	}

}

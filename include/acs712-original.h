
static float getVPP()
{
	float result;
	uint32_t readValue;
	uint32_t readValueAnt = 0;
	
	uint32_t maxValue = 0;
	uint32_t minValue = 1024;
	uint64_t iLeituras = 70;
	unsigned long k = 0;
	uint32_t iInversoes = 0;
	int bSobe = 0;
	int bSobeAnt = 0;
	unsigned long iTempo = 0;
	unsigned long iCounter = 0;
	seekAnalogInput(iAnalogPointer);

	bzero(arrayConsumo, 1024);
	sprintf(arrayConsumo, "%s", "\"[");
	
	
	//****************************************
	uint32_t start_ccount, ccount;
    uint32_t delay_ccount = sdk_os_get_cpu_frequency() * 16666; // 2.000k
	
    RSR(start_ccount, ccount); // start_ccount = 27.555k
	iCounter = start_ccount + (delay_ccount / 200);
	
	//Primeira Leitura
	readValue =  sdk_system_adc_read();
	sprintf(arrayConsumoBuffer, "%d", readValue);
	strcat(arrayConsumo, arrayConsumoBuffer);
	
    do {
        RSR(ccount, ccount);
		
		//Média
		readValue = ((readValue + sdk_system_adc_read()) / 2);
		
		if (readValue > maxValue)
		{
			maxValue = readValue;
		}
		if (readValue < minValue)
		{
			minValue = readValue;
		}
		
		if (ccount >= (iCounter) )
		{
			sprintf(arrayConsumoBuffer, "%d", readValue);
			strcat(arrayConsumo, ",");
			strcat(arrayConsumo, arrayConsumoBuffer);
			
			iCounter += (delay_ccount / 200);
		}
    } while (ccount - start_ccount < delay_ccount); 	//29.555k
	
	
	//Última Leitura
	sprintf(arrayConsumoBuffer, "%d", readValue);
	strcat(arrayConsumo, ",");
	strcat(arrayConsumo, arrayConsumoBuffer);
	
	printf("DELAY_COUNT: %d => iCounter: %d\n", delay_ccount, iCounter);
	//*********************************************
	
//	//for (k = 0; k < 1099000; k++)
//	for (;;)
//	{
//		//if ((k % 15700) == 0)
//		//{
//			readValue =  sdk_system_adc_read();
//		//	
//		//	if (readValue > maxValue)
//		//	{
//		//		maxValue = readValue;
//		//	}
//		//	if (readValue < minValue)
//		//	{
//		//		minValue = readValue;
//		//	}
//			
//		//	sprintf(arrayConsumoBuffer, "%d", readValue);
//		//	if (k>0) strcat(arrayConsumo, ",");
//		//	strcat(arrayConsumo, arrayConsumoBuffer);
//			
//		//}
//		
//		printf("COUNTER: %d = %d\n", iCounter, readValue);
//		iCounter++;
//	}
	
	strcat(arrayConsumo, "]\"");
	
	printf("maxValue: %d\n", maxValue);
	printf("minValue: %d\n", minValue);
	

	result = ((maxValue - minValue) * 4.9) / 1024.0;
	
	printf("result VPP (((maxValue - minValue) * 4.9) / 1024.0)  : %.3f\n", result);
	
	
	//printf(padraoConsumo);

	/*
	printf("Minimo: %d\n", minValue);
	printf("Maximo: %d\n", maxValue);
	printf("Amplitude: %d\n", (maxValue - minValue));
	printf("Leitura Resultado: %d\n", result);


	*/

	
	return result;
}


static float getACS712() {  // for AC
	float dVoltage = (float)getVPP();


	uint8_t mVperAmp = 100;   // 185 for 5A, 100 for 20A and 66 for 30A Module
	float ACSVoltage = 0;
	float VRMS = 0;
	float AmpsRMS = 0;
	float mcuVoltage = 4.9;         // MCU ADC maximum - voltage divider means we should use 5.0V reference not 3.2V
	float WH = 0;
	uint8_t calibration = 62;  // V2 slider calibrates this
	uint8_t pF = 95;           // Power Factor default 90

	ACSVoltage = dVoltage;
	VRMS = ((ACSVoltage / 2.0) * 0.707);
	VRMS = VRMS - (calibration / 10000.0);     // calibtrate to zero with slider
	AmpsRMS = (VRMS * 1000) / mVperAmp;
	
	//if (AmpsRMS < 0.05){  // remove low end chatter
	if ((AmpsRMS > -0.02) && (AmpsRMS < 0.02)){  // remove low end chatter
		AmpsRMS = 0.0;
	}

	if (AmpsRMS<0)
	{
		AmpsRMS = 0;
	}
	
	dConsumo = AmpsRMS;
	printf("Amps RMS: %.3f\n", AmpsRMS);


	/* 
	Serial.print("VRMS: ");
	Serial.println(String(VRMS, 3));

	Serial.print("ACSVoltage: ");
	Serial.println(String(ACSVoltage, 3));
	Serial.print("Amps RMS: ");
	Serial.println(String(AmpsRMS, 3));
	Serial.print("Power: ");
	WH = (inputV * AmpsRMS) * (pF / 100.0);
	Serial.print(String(WH, 3));
	Serial.println(" WH");
	*/
	
	return AmpsRMS;
	//return (AmpsRMS / 4);



}


#include "include/stringLib.h"


#include "mbedtls/config.h"
#include "mbedtls/net_sockets.h"
#include "mbedtls/debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"
#include "mbedtls/md.h"
#include "mbedtls/sha256.h"
#include "mbedtls/aes.h"

char *int2hexa(unsigned char nHexa)
{
	unsigned char arr[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	char sResult [3];

	sResult[0] = (unsigned char)arr[(nHexa / 16)];
	sResult[1] = (unsigned char)arr[(nHexa % 16)];
	sResult[2] = '\0';

	return  sResult;

}

//uint8_t hexa2int(String sHexa)
//{
//	uint8_t iResult = 0;
//	String arr = "0123456789ABCDEF";
//
//	iResult = (arr.indexOf(sHexa.charAt(0)) * 16);
//	iResult += arr.indexOf(sHexa.charAt(1));
//
//	return iResult;
//}

void print_hex(const char *texto, int iSize)
{
    printf("\n*** PRINT BLOCK ***\n");
    for(int j=0; j<iSize; j++)
    {
        if ((j%16)==0) printf("\n");
		printf("%02x ", texto[j]);
    }
    printf("\n*******************\n");
}

char *_generate_signature(char *api_secret, char *payload)
{    
  int i,j = 0;
  unsigned char hmac_hash[64] = {'\0'};
  unsigned char signature[64] = {'\0'};
  mbedtls_md_context_t ctx;
  mbedtls_md_type_t md_type = MBEDTLS_MD_SHA256;

  char payload_buffer[512] = {'\0'};
  const size_t payload_len = strlen(payload);
  size_t key_len = strlen(api_secret);            
  strcpy(payload_buffer, payload);        

  mbedtls_md_init(&ctx);  
  mbedtls_md_setup(&ctx, mbedtls_md_info_from_type(md_type) , 1); //use hmac
  mbedtls_md_hmac_starts(&ctx, api_secret, key_len);
  mbedtls_md_hmac_update(&ctx, (const unsigned char *) payload_buffer, payload_len);    
  mbedtls_md_hmac_finish(&ctx, hmac_hash);

  
  for(i=0; i<sizeof(hmac_hash)-1; i++)
  {
  
    sprintf(signature+(i+j), "%02x", hmac_hash[i]);
	
    j++;
  }  

  mbedtls_md_free(&ctx);    

  return (left(&signature, 64));
} 

void encrypt_block(char * plainText, char * key3, unsigned char * outputBuffer){
 
  mbedtls_aes_context aes;
 
  mbedtls_aes_init( &aes );
  mbedtls_aes_setkey_enc( &aes, (const unsigned char*) key3, 256 );
  mbedtls_aes_crypt_ecb( &aes, MBEDTLS_AES_ENCRYPT, (const unsigned char*)plainText, outputBuffer);
  mbedtls_aes_free( &aes );
}

void decrypt_block(unsigned char * chipherText, char * key3, unsigned char * outputBuffer){
 
  mbedtls_aes_context aes;
 
  mbedtls_aes_init( &aes );
  mbedtls_aes_setkey_dec( &aes, (const unsigned char*) key3, 256 );
  mbedtls_aes_crypt_ecb(&aes, MBEDTLS_AES_DECRYPT, (const unsigned char*)chipherText, outputBuffer);
  mbedtls_aes_free( &aes );
}

char *encode_Base64(unsigned char *texto)
{
	unsigned int olen = ((strlen(texto) * 8) / 6);
	
    //mbedtls_base64_encode(NULL, 0, &olen, texto, strlen( texto )); //get length
    
	
	unsigned char *encoded_key;
	encoded_key = (unsigned char *)malloc(olen);
	bzero(encoded_key, olen);
	
	size_t n;
	int ok = mbedtls_base64_encode(encoded_key, olen, &n, texto, strlen( texto ));
	
	return encoded_key;
	
}

unsigned char *decode_Base64(unsigned char *texto)
{
	unsigned int olen = ((strlen(texto) * 6) / 8) ;
	
	unsigned char *decoded_key;
	decoded_key = (unsigned char *)malloc(olen);
	bzero(decoded_key, olen);
	
	size_t n;
	
	int ok = mbedtls_base64_decode(decoded_key, olen, &n, texto, strlen( texto ));
	
	
	return (left(decoded_key, n));
}

void vEncode_Base64(char **encoded_key, char *texto)
{
	unsigned int olen = ((strlen(texto) * 8) / 6);
	
    //mbedtls_base64_encode(NULL, 0, &olen, texto, strlen( texto )); //get length
    
	
	//unsigned char *encoded_key;
	*encoded_key = (char *)malloc(olen);
	bzero(*encoded_key, olen);
	
	size_t n;
	int ok = mbedtls_base64_encode(*encoded_key, olen, &n, texto, strlen( texto ));
	
	//*sReturn = (char *)malloc(n);
	//memcpy(*sReturn, encoded_key, n);
	
	//return encoded_key;
	
}

int vDecode_Base64(char **decoded_key, char *texto)
{
	unsigned int olen = ((strlen(texto) * 6) / 8) ;
	
	//*decoded_key = (char *)malloc(olen);
	bzero(*decoded_key, olen);
	
	size_t n;
	
	int ok = mbedtls_base64_decode(*decoded_key, olen, &n, texto, strlen( texto ));
	
	return (int)n;
	//*sReturn = (char *)malloc(n);
	//memcpy(*sReturn, decoded_key, n);
	
	//return (left(decoded_key, n));
}


char *encrypt(const char* texto, const char *key, const char *av)
{
	mbedtls_aes_context aes;
    unsigned char  iv[]      = "0000000000000000";
	memcpy(iv, av, 16);
	
	unsigned char input[INPUT_SIZE] = {0};
    unsigned char output[INPUT_SIZE] = {0};
    int ret = 0, m =0;
    int TXT_VALID_LENGTH = ((strlen((const char*)texto)%16 == 0)  ?  strlen((const char*)texto) :  (strlen((const char*)texto)/16 + 1)*16);

	//printf("TAMANHO IDA: %d\n", TXT_VALID_LENGTH);
	
	for(m=0; m<strlen((const char*)texto); m++)
    {
        input[m] = *(texto+m);
    }

    mbedtls_aes_setkey_enc( &aes, key, 256);
    ret = mbedtls_aes_crypt_cbc( &aes, MBEDTLS_AES_ENCRYPT, TXT_VALID_LENGTH, iv, input, output );
    //printf("Ret: %d  - strlen(input)=%d -  ENCRYPTED output  %s\n", ret , TXT_VALID_LENGTH, output);
    
    //printf("HEX ENCRYPTED output\n");
    //print_hex(output, TXT_VALID_LENGTH);
    
    //Converter para BASE64
    size_t out_len;
    char *encoded64 = (char *)malloc(INPUT_SIZE);
    mbedtls_base64_encode(encoded64, INPUT_SIZE, &out_len, (const unsigned char *)output, TXT_VALID_LENGTH );
    //printf("64ENCODED ENCRYPTED  output  %s\n", encoded64);
	
	return encoded64;
}

unsigned char *decrypt(char *encoded64, const char *key, const char *av)
{
	mbedtls_aes_context aes2;
    unsigned char  iv[]      = "0000000000000000";
	memcpy(iv, av, 16);
	
	unsigned char input[INPUT_SIZE] = {0};
    unsigned char output[INPUT_SIZE] = {0};
    int ret = 0;
    int TXT_VALID_LENGTH = ((strlen(encoded64) * 6) / 8) ;
	
	//Retira o PADDING do Base64
	if (position(encoded64, "==") > 0)
		TXT_VALID_LENGTH -= 2;
	else if (position(encoded64, "=") > 0)
		TXT_VALID_LENGTH -= 1;
	
	//printf("TAMANHO VOLTA: %d\n", TXT_VALID_LENGTH);
	
	
	
    //DECODE BASE64
    size_t out_len;
    mbedtls_base64_decode(input, INPUT_SIZE, &out_len, (const unsigned char *)encoded64, strlen((const char*)encoded64) );

    //printf("HEX ENCRYPTED input\n");
    //print_hex(input, TXT_VALID_LENGTH);
	
    mbedtls_aes_setkey_dec( &aes2, key, 256);
    ret = mbedtls_aes_crypt_cbc(&aes2, MBEDTLS_AES_DECRYPT, TXT_VALID_LENGTH, iv, input, output );
	
    //printf("DECRYPTED output: %s \n", output);
	return output;
	
}

int vEncrypt(char **sReturn, const char* texto, const char *key, const char *av)
{
	mbedtls_aes_context aes;
    unsigned char  iv[]      = "0000000000000000";
	memcpy(iv, av, 16);
	
	unsigned char input[INPUT_SIZE] = {0};
    unsigned char output[INPUT_SIZE] = {0};
    int ret = 0, m =0;
    int TXT_VALID_LENGTH = ((strlen((const char*)texto)%16 == 0)  ?  strlen((const char*)texto) :  (strlen((const char*)texto)/16 + 1)*16);

	//printf("TAMANHO IDA: %d\n", TXT_VALID_LENGTH);
	
	for(m=0; m<strlen((const char*)texto); m++)
    {
        input[m] = *(texto+m);
    }

    mbedtls_aes_setkey_enc( &aes, key, 256);
    ret = mbedtls_aes_crypt_cbc( &aes, MBEDTLS_AES_ENCRYPT, TXT_VALID_LENGTH, iv, input, output );
    //printf("Ret: %d  - strlen(input)=%d -  ENCRYPTED output  %s\n", ret , TXT_VALID_LENGTH, output);
    
    //printf("HEX ENCRYPTED output\n");
    //print_hex(output, TXT_VALID_LENGTH);
    
    //Converter para BASE64
    size_t out_len;
    char *encoded64 = (char *)malloc(INPUT_SIZE);
    mbedtls_base64_encode(encoded64, INPUT_SIZE, &out_len, (const unsigned char *)output, TXT_VALID_LENGTH );
    //printf("64ENCODED ENCRYPTED  output  %s\n", encoded64);
	
	*sReturn = (char *)malloc(out_len);
	bzero(*sReturn, out_len);
	memcpy(*sReturn, encoded64, out_len);
	
	printf("vEncrypt TAMANHO: %d\n", out_len);
	return out_len;
	
}

int vDecrypt(char **sReturn, char *encoded64, const char *key, const char *av)
{
	mbedtls_aes_context aes2;
    unsigned char  iv[]      = "0000000000000000";
	memcpy(iv, av, 16);
	
	unsigned char input[INPUT_SIZE] = {0};
    unsigned char output[INPUT_SIZE] = {0};
    int ret = 0;
    int TXT_VALID_LENGTH = ((strlen(encoded64) * 6) / 8) ;
	
	//Retira o PADDING do Base64
	if (position(encoded64, "==") > 0)
		TXT_VALID_LENGTH -= 2;
	else if (position(encoded64, "=") > 0)
		TXT_VALID_LENGTH -= 1;
	
	//printf("TAMANHO VOLTA: %d\n", TXT_VALID_LENGTH);
	
	
	
    //DECODE BASE64
    size_t out_len;
    mbedtls_base64_decode(input, INPUT_SIZE, &out_len, (const unsigned char *)encoded64, strlen((const char*)encoded64) );

    //printf("HEX ENCRYPTED input\n");
    //print_hex(input, TXT_VALID_LENGTH);
	
    mbedtls_aes_setkey_dec( &aes2, key, 256);
    ret = mbedtls_aes_crypt_cbc(&aes2, MBEDTLS_AES_DECRYPT, TXT_VALID_LENGTH, iv, input, output );
	
    //printf("DECRYPTED output: %s \n", output);
	
	//*sReturn = (char *)malloc(TXT_VALID_LENGTH);
	memcpy(*sReturn, output, TXT_VALID_LENGTH);
	
	return TXT_VALID_LENGTH;
}

void crypto_teste()
{
    int i=0;
    
    // We create two AES contexts
    mbedtls_aes_context aes;
    mbedtls_aes_context aes2;

    // Our KEY , should be shared only with the receiver and has to be as random as possible.
    // Here we use this simple example for ease and simplicity
    unsigned char key[32] = "1234567890123456789012345678901" ; 
    key[31] = '2';   // we replace the 32th (index 31) which contains '/0' with the '8' char.

    // This is the Init Vector generated with the random KEY from http://aes.online-domain-tools.com/
    unsigned char orig_iv[] = { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36 };
    unsigned char iv1[IV_SIZE] = {0};
    unsigned char iv2[IV_SIZE] =  {0};

    // This is the text that we want to encrypt and send
    unsigned char my_txt[] = "{\"GUID\":\"2d3952c0-e71f-4405-8c11-936ea8fa53b5\",\"key\":\"fVyolDzNaOzhAhD5HlBRWKrX6/cl7cTz\",\"ssid_sta\":\"Faccion\",\"pass_sta\":\"casagrande\",\"ssid_ap\":\"Wise3785165\",\"pass_ap\":\"senha12345\",\"device_state\":\"0\" }";

    unsigned char input[INPUT_SIZE] = {0};
    
    for(int m=0; m<strlen((const char*)my_txt); m++)
    {
        input[m] = *(my_txt+m);
    }
       

    // Output of encryption
    unsigned char output[INPUT_SIZE] = {0};
    // Output of decryption
    unsigned char output2[INPUT_SIZE] = {0};
 
     unsigned char output3[INPUT_SIZE] = {0};
	 
   int ret =0;
    
    // To be a valid length, it has to be a multiple of 16, so we take the muliple of 16 that is nearly bigger than the actual size
    // Or the 
    int TXT_VALID_LENGTH = ((strlen((const char*)my_txt)%16 == 0)  ?  strlen((const char*)my_txt) :  (strlen((const char*)my_txt)/16 + 1)*16);
    //while(1){
        //init_ivs(orig_iv , iv1, iv2);
        init_ivs(globalIV , iv1, iv2);
        
        
        mbedtls_aes_setkey_enc( &aes, globalKey, 256);
        //mbedtls_aes_setkey_enc( &aes, key, 256);
        ret = mbedtls_aes_crypt_cbc( &aes, MBEDTLS_AES_ENCRYPT, TXT_VALID_LENGTH, iv1, input, output );
        //ret = mbedtls_aes_crypt_cbc( &aes, MBEDTLS_AES_ENCRYPT, TXT_VALID_LENGTH, iv1, input, output );
        printf("%d  -  Ret: %d  - strlen(input)=%d -  ENCRYPTED output  %s\n", i, ret , TXT_VALID_LENGTH, output);
        
        // We can use 64encoding to send data and to avoid encoding problems
        // In our case we use haxadecimal representation of the characters so no need.  
       
        printf("%d  HEX ENCRYPTED output\n", i);
        for(int j=0; j<TXT_VALID_LENGTH; j++)
        {
            printf("%02x ", output[j]);
        }
        printf("\n");
        
		//Converter para BASE64
		size_t out_len;
		char *encoded64 = (char *)malloc(INPUT_SIZE);
        mbedtls_base64_encode(encoded64, INPUT_SIZE, &out_len, (const unsigned char *)output, TXT_VALID_LENGTH );
        printf("64ENCODED ENCRYPTED  output  %s\n", encoded64);
		
		//DECODE BASE64
        mbedtls_base64_decode(output3, INPUT_SIZE, &out_len, (const unsigned char *)encoded64, strlen((const char*)encoded64) );
		
        printf("%d  HEX ENCRYPTED RETURNED BASE64\n");
        for(int j=0; j<TXT_VALID_LENGTH; j++)
        {
            printf("%02x ", output3[j]);
        }
        printf("\n");
		
		
        // We decrypt the OUTPUT1 and we get the same input string
    
        mbedtls_aes_setkey_dec( &aes2, globalKey, 256);
        //mbedtls_aes_setkey_dec( &aes2, key, 256);
        ret = mbedtls_aes_crypt_cbc(&aes2, MBEDTLS_AES_DECRYPT, TXT_VALID_LENGTH, iv2, output3, output2 );
        //ret = mbedtls_aes_crypt_cbc(&aes2, MBEDTLS_AES_DECRYPT, TXT_VALID_LENGTH, iv2, output3, output2 );
        printf("%d  -  Ret: %d  -  DECRYPTED output  %s\n\n", i , ret,  output2);
        
        // Showing the decrypted result in HEX
        printf("%d  HEX DECRYPTED output\n", i, output2);
        for(int j=0; j<TXT_VALID_LENGTH; j++)
        {
            printf("%02x ", output2[j]);
        }
        printf("\n\n\n");
    
        i++;
        // Wait 2 seconds and re-do it.
    //    wait(2);
    //}
}

void init_ivs(unsigned char iv_orig[], unsigned char iv1[], unsigned char iv2[])
{
    for(int i=0; i<IV_SIZE; i++){
    
         iv2[i] = iv1[i] = iv_orig[i];
        
    }
}

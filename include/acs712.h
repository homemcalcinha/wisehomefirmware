
#include <math.h>

static float getVPP()
{
	float result;
	uint32_t readValue;
	uint32_t readValueAnt = 0;

	uint32_t maxValue = 0;
	uint32_t minValue = 1024;
	uint64_t iLeituras = 70;
	int iCalibragem = 10;
	int ICalrum = 0;
	uint64_t iEfetuadas = 0;
	float occupancy_threshold = .0259;

	unsigned long k = 0;
	uint32_t iInversoes = 0;
	int bSobe = 0;
	int bSobeAnt = 0;
	unsigned long iTempo = 0;
	unsigned long iCounter = 0;
	seekAnalogInput(iAnalogPointer);

	bzero(arrayConsumo, 1024);
	sprintf(arrayConsumo, "%s", "\"[");


	//**************fazemos uma leiura, porem, não é correto para o RMS ******************
	//**************temos que ler 10 vezes para ter uma posicação ajustada ***************

	uint32_t start_ccount, ccount;
  uint32_t delay_ccount = sdk_os_get_cpu_frequency() * 16666; // 2.000k



	//Primeira Leitura
	readValue =  sdk_system_adc_read();
	sprintf(arrayConsumoBuffer, "%d", readValue);
	strcat(arrayConsumo, arrayConsumoBuffer);

	for (ICalrum=0; ICalrum<iCalibragem; ICalrum++)
	{
		RSR(start_ccount, ccount); // start_ccount = 27.555k
		iCounter = start_ccount + (delay_ccount / 200);
		//printf("ICalrum %d\n", ICalrum);
		do {
        RSR(ccount, ccount);
				readValue = ((readValue + sdk_system_adc_read()) / 2);
				iEfetuadas++;
				if (readValue > maxValue)
				{
						maxValue = readValue;
				}
				if (readValue < minValue)
				{
						minValue = readValue;
				}

				if ((ccount >= (iCounter) )&&(ICalrum==0))
				{
						sprintf(arrayConsumoBuffer, "%d", readValue);
						strcat(arrayConsumo, ",");
						strcat(arrayConsumo, arrayConsumoBuffer);
						iCounter += (delay_ccount / 200);
				}
				else if ((ccount >= (iCounter) )&&(ICalrum>0))
				{
						iCounter += (delay_ccount / 200);
				}

    } while (ccount - start_ccount < delay_ccount); 	//29.555k
	}
	//Última Leitura
	//float rms = sqrt((float)currentAcc / (float)iEfetuadas);

	sprintf(arrayConsumoBuffer, "%d", readValue);
	strcat(arrayConsumo, ",");
	strcat(arrayConsumo, arrayConsumoBuffer);

	//printf("DELAY_COUNT: %d => iCounter: %d\n", delay_ccount, iCounter);
	//printf("iEfetuadas: %d => Media: %d\n ", iEfetuadas, readValue);
	//*********************************************

//	//for (k = 0; k < 1099000; k++)
//	for (;;)
//	{
//		//if ((k % 15700) == 0)
//		//{
//			readValue =  sdk_system_adc_read();
//		//
//		//	if (readValue > maxValue)
//		//	{
//		//		maxValue = readValue;
//		//	}
//		//	if (readValue < minValue)
//		//	{
//		//		minValue = readValue;
//		//	}
//
//		//	sprintf(arrayConsumoBuffer, "%d", readValue);
//		//	if (k>0) strcat(arrayConsumo, ",");
//		//	strcat(arrayConsumo, arrayConsumoBuffer);
//
//		//}
//
//		//printf("COUNTER: %d = %d\n", iCounter, readValue);
//		iCounter++;
//	}

	strcat(arrayConsumo, "]\"");

	//printf("maxValue: %d\n", maxValue);
	//printf("minValue: %d\n", minValue);


	result = ((maxValue - minValue) * 4.9) / 1024.0;

	//printf("result VPP (((maxValue - minValue) * 4.9) / 1024.0)  : %.3f\n", result);


	//printf(padraoConsumo);

	/*
	//printf("Minimo: %d\n", minValue);
	//printf("Maximo: %d\n", maxValue);
	//printf("Amplitude: %d\n", (maxValue - minValue));
	//printf("Leitura Resultado: %d\n", result);


	*/


	return result;
}


static float getACS712() {  // for AC
	// printf("lendo o ADC %d\n", xTaskGetTickCount());
	float dVoltage = (float)getVPP();
	// printf("terminei de ler o ADC %d\n", xTaskGetTickCount());


	float mVperAmp = 100;   // 185 for 5A, 100 for 20A and 66 for 30A Module
	float ACSVoltage = 0;
	float VRMS = 0;
	float AmpsRMS = 0;
	float mcuVoltage = 4.9;         // MCU ADC maximum - voltage divider means we should use 5.0V reference not 3.2V
	float WH = 0;
	float calibration = 600;  // V2 slider calibrates this
	uint8_t pF = 90;           // Power Factor default 90

	ACSVoltage = dVoltage;
	//printf("\nporque??? ==>>%d\n", ACSVoltage);
	VRMS = ((ACSVoltage / 2.0) * 0.707);
	//printf("VRMS??? ==>>%d\n", VRMS);
	VRMS = VRMS - (calibration / 10000.0);     // calibtrate to zero with slider
	//printf("VRMS??? ==>>%d\n", VRMS);
	AmpsRMS = (VRMS * 1000) / mVperAmp;
	//printf("AmpsRMS??? ==>>%d\n", AmpsRMS);

	//if (AmpsRMS < 0.05){  // remove low end chatter
	if ((AmpsRMS > -0.02) && (AmpsRMS < 0.02)){  // remove low end chatter
		AmpsRMS = 0.0;
	}

	if (AmpsRMS<0)
	{
		AmpsRMS = 0;
	}

	dConsumo = AmpsRMS;
	printf("Amps RMS: %.3f\n", AmpsRMS);


	/*
	Serial.print("VRMS: ");
	Serial.println(String(VRMS, 3));

	Serial.print("ACSVoltage: ");
	Serial.println(String(ACSVoltage, 3));
	Serial.print("Amps RMS: ");
	Serial.println(String(AmpsRMS, 3));
	Serial.print("Power: ");
	WH = (inputV * AmpsRMS) * (pF / 100.0);
	Serial.print(String(WH, 3));
	Serial.println(" WH");
	*/

	return AmpsRMS;
	//return (AmpsRMS / 4);



}

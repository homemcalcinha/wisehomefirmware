#ifndef STRINGLIB_H
#define STRINGLIB_H

#include <stdlib.h>
#include <string.h>
#include "stringLib.c"

char *left(const char* origem, int iCh);
char *right(const char* origem, int iCh);
char *substring(const char* origem, int iStart, int iCount);
void vSubstring(const char *origem, char **destino, int iStart, int iCount);
int position(const char *texto, const char *procura);
int chPosition(const char *texto, const char cCh);
char *append(const char *texto1, const char* texto2);
char *insert(const char *texto, const char* novo, int iPos);
char *replace(const char* texto, const char *antigo, const char *novo);
int compare(const char *texto1, const char *texto2);
bool startswith(const char *texto, const char *starts);
bool endswith(const char *texto, const char *ends);
char *between(const char *texto, const char *chStart, const char *chEnd);
char *trim(const char *texto);
char *remove_spaces(char *texto);
void* memcpy_index(void *s1, const void *s2, size_t index, size_t n);
#endif


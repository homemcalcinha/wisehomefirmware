SemaphoreHandle_t xSemaforo_dataBus = NULL;			//MUTEX

static void get_globalKey(unsigned char **oGlobalKey)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(*oGlobalKey, 32);
			memcpy(*oGlobalKey, globalKey, 32);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_globalIV(unsigned char **oGlobalIV)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(*oGlobalIV, 16);
			memcpy(*oGlobalIV, globalIV, 16);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static bool get_SD_state(uint8_t iSD_Number)
{
	bool bRet = false;
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			switch (iSD_Number)
			{
				case 1:
						bRet = SD1.digital_value;
						break;
				case 2:
						bRet = SD2.digital_value;
						break;
				case 3:
						bRet = SD3.digital_value;
						break;
				case 4:
						bRet = SD4.digital_value;
						break;
				case 5:
						bRet = SD5.digital_value;
						break;
				case 6:
						bRet = SD6.digital_value;
						break;
				case 7:
						bRet = SD7.digital_value;
						break;
				case 8:
						bRet = SD8.digital_value;
						break;
			}
			xSemaphoreGive( xSemaforo_dataBus);
		}
		else bRet = false;
	}
	return bRet;
}

static void set_SD_state(uint8_t iSD_Number, bool bValue)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			switch (iSD_Number)
			{
				case 1:
						SD1.digital_value = bValue;
						break;
				case 2:
						SD2.digital_value = bValue;
						break;
				case 3:
						SD3.digital_value = bValue;
						break;
				case 4:
						SD4.digital_value = bValue;
						break;
				case 5:
						SD5.digital_value = bValue;
						break;
				case 6:
						SD6.digital_value = bValue;
						break;
				case 7:
						SD7.digital_value = bValue;
						break;
				case 8:
						SD8.digital_value = bValue;
						break;
			}
			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static uint8_t get_EA_value(uint8_t iEA_Number)
{
	uint8_t nRet = 0;
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			switch (iEA_Number)
			{
				case 1:
						nRet = EA1.analog_value;
						break;
				case 2:
						nRet = EA2.analog_value;
						break;
				case 3:
						nRet = EA3.analog_value;
						break;
				case 4:
						nRet = EA4.analog_value;
						break;
				case 5:
						nRet = EA5.analog_value;
						break;
				case 6:
						nRet = EA6.analog_value;
						break;
				case 7:
						nRet = EA7.analog_value;
						break;
				case 8:
						nRet = EA8.analog_value;
						break;
			}
			xSemaphoreGive( xSemaforo_dataBus);
		}
		else nRet = 0;
	}
	return nRet;
}

static void invert_SD_state(uint8_t iSD_Number)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			switch (iSD_Number)
			{
				case 1:
						SD1.digital_value = !SD1.digital_value;
						break;
				case 2:
						SD2.digital_value = !SD2.digital_value;
						break;
				case 3:
						SD3.digital_value = !SD3.digital_value;
						break;
				case 4:
						SD4.digital_value = !SD4.digital_value;
						break;
				case 5:
						SD5.digital_value = !SD5.digital_value;
						break;
				case 6:
						SD6.digital_value = !SD6.digital_value;
						break;
				case 7:
						SD7.digital_value = !SD7.digital_value;
						break;
				case 8:
						SD8.digital_value = !SD8.digital_value;
						break;
			}
			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static bool get_ED_state(uint8_t iED_Number)
{
	bool bRet = false;
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			switch (iED_Number)
			{
				case 1:
						bRet = ED1.digital_value;
						break;
				case 2:
						bRet = ED1.digital_value;
						break;
				case 3:
						bRet = ED1.digital_value;
						break;
				case 4:
						bRet = ED1.digital_value;
						break;
				case 5:
						bRet = ED1.digital_value;
						break;
				case 6:
						bRet = ED1.digital_value;
						break;
				case 7:
						bRet = ED1.digital_value;
						break;
				case 8:
						bRet = ED1.digital_value;
						break;
			}
			xSemaphoreGive( xSemaforo_dataBus);
		}
		else bRet = false;
	}
	return bRet;
}

static void set_ED_state(uint8_t iED_Number, bool bValue)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			switch (iED_Number)
			{
				case 1:
						ED1.digital_value = bValue;
						break;
				case 2:
						ED1.digital_value = bValue;
						break;
				case 3:
						ED1.digital_value = bValue;
						break;
				case 4:
						ED1.digital_value = bValue;
						break;
				case 5:
						ED1.digital_value = bValue;
						break;
				case 6:
						ED1.digital_value = bValue;
						break;
				case 7:
						ED1.digital_value = bValue;
						break;
				case 8:
						ED1.digital_value = bValue;
						break;
			}
			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_DeviceMemo_Ssid_ap(char **sRet)
{

	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*sRet = (char *)malloc(50);
			memcpy(*sRet, deviceMemo.ssid_ap, 50);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_DeviceMemo_Ssid_ap(const char *sValue)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(deviceMemo.ssid_ap, 50);
			memcpy(deviceMemo.ssid_ap, sValue, strlen(sValue));

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_DeviceMemo_Pass_ap(char **sRet)
{

	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*sRet = (char *)malloc(50);
			memcpy(*sRet, deviceMemo.pass_ap, 50);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_DeviceMemo_Pass_ap(const char *sValue)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(deviceMemo.pass_ap, 50);
			memcpy(deviceMemo.pass_ap, sValue, strlen(sValue));

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_DeviceMemo_Ssid_sta(char **sRet)
{

	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*sRet = (char *)malloc(50);
			memcpy(*sRet, deviceMemo.ssid_sta, 50);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_DeviceMemo_Ssid_sta(const char *sValue)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(deviceMemo.ssid_sta, 50);
			memcpy(deviceMemo.ssid_sta, sValue, strlen(sValue));

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_DeviceMemo_Pass_sta(char **sRet)
{

	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*sRet = (char *)malloc(50);
			memcpy(*sRet, deviceMemo.pass_sta, 50);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_DeviceMemo_Pass_sta(const char *sValue)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(deviceMemo.pass_sta, 50);
			memcpy(deviceMemo.pass_sta, sValue, strlen(sValue));

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_DeviceMemo_Device_State(char *cCh)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*cCh = deviceMemo.device_state;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_DeviceMemo_Device_State(const char cCh)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			deviceMemo.device_state = cCh;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}


static void get_GUID(char **sGUID)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(*sGUID, 36);
			memcpy(*sGUID, deviceMemo.GUID, 36);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_RecuperarConfig(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_RecuperarConfig;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_RecuperarConfig(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_RecuperarConfig = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_CreateWiFi(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_CreateWiFi;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_CreateWiFi(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_CreateWiFi = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}


static void get_Flag_OTA_SHA(unsigned char **oOTA_SHA)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(*oOTA_SHA, 64);
			memcpy(*oOTA_SHA, flag_OTA_SHA, sizeof(oOTA_SHA));
			//*bFlag = flag_OTA_SHA;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_OTA_SHA(unsigned char oOTA_SHA)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(*flag_OTA_SHA, 64);
			memcpy(*flag_OTA_SHA, oOTA_SHA, sizeof(oOTA_SHA));

			//flag_OTA_SHA = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}


static void get_Flag_OTA_FILE(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			//*bFlag = flag_OTA_FILE;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_OTA_FILE(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			//flag_OTA_FILE = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_OTA_SERVER(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			//*bFlag = flag_OTA_SERVER;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_OTA_SERVER(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			//flag_OTA_SERVER = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_AfterSave(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_AfterSave;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_AfterSave(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_AfterSave = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

//********************

static void get_Flag_ConnectFail(uint16_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_ConnectFail;
			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_ConnectFail(uint16_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_ConnectFail = bFlag;
			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}


//******************

static void get_Flag_InicializarIOs(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_InicializarIOs;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_InicializarIOs(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_InicializarIOs = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_ReadWriteIOs(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_ReadWriteIOs;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_ReadWriteIOs(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_ReadWriteIOs = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_TestOutputs(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_TestOutputs;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_TestOutputs(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_TestOutputs = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_ServidorTLS(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_ServidorTLS;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_ServidorTLS(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_ServidorTLS = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_ServidorHTTP(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_ServidorHTTP;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_ServidorHTTP(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_ServidorHTTP = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_Factory(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_Factory;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_Factory(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_Factory = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_CheckCloud(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_CheckCloud;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_CheckCloud(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_CheckCloud = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_GravarConfig(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_GravarConfig;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_GravarConfig(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_GravarConfig = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_Reiniciar(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_Reiniciar;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_Reiniciar(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_Reiniciar = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_ReceivingHTTP(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_ReceivingHTTP;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_ReceivingHTTP(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_ReceivingHTTP = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_RequisitarGravacao(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_RequisitarGravacao;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_RequisitarGravacao(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_RequisitarGravacao = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_OTA(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_OTA;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_OTA(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_OTA = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}


static void get_Flag_IniciarZerocross(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_IniciarZerocross;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_IniciarZerocross(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_IniciarZerocross = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_Flag_Eventos(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = flag_Eventos;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_Eventos(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			flag_Eventos = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_AnalogPointer(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = iAnalogPointer;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_AnalogPointer(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			iAnalogPointer = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void get_AnalogPointerAnt(uint8_t *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = iAnalogPointer_ant;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_AnalogPointerAnt(uint8_t bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			iAnalogPointer_ant = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

//static void get_AdcArray_value(uint8_t iPosition, uint32_t *bFlag)
//{
//	if( xSemaforo_dataBus != NULL )
//	{
//		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
//		{
//			*bFlag = adcArray[iPosition];
//
//			xSemaphoreGive( xSemaforo_dataBus);
//		}
//	}
//}
//
//static void set_AdcArray_value(uint8_t iPosition, uint32_t bFlag)
//{
//	if( xSemaforo_dataBus != NULL )
//	{
//		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
//		{
//			adcArray[iPosition] = bFlag;
//
//			xSemaphoreGive( xSemaforo_dataBus);
//		}
//	}
//}

static void get_GlobalConsumo(float *dValor)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*dValor = dConsumo;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_GlobalConsumo(float dValor)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			dConsumo = dValor;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}



static void get_Flag_Seguranca(bool *bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*bFlag = bFlag_Seguranca;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_Flag_Seguranca(bool bFlag)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bFlag_Seguranca = bFlag;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}


static void get_Key(char **sKey)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			bzero(*sKey, 32);
			memcpy(*sKey, deviceMemo.key, 32);

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}



static void get_DeviceMemo(struct memoryreg_t *oDevice)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			*oDevice = deviceMemo;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

static void set_DeviceMemo(struct memoryreg_t oDevice)
{
	if( xSemaforo_dataBus != NULL )
	{
		if( xSemaphoreTake( xSemaforo_dataBus, portMAX_DELAY) )
		{
			deviceMemo = oDevice;

			xSemaphoreGive( xSemaforo_dataBus);
		}
	}
}

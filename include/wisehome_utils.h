void checkconnection(void) {
  struct memoryreg_t localDevice;
  get_DeviceMemo(&localDevice);
  uint8_t http_dados =0;
  get_Flag_ReceivingHTTP(&http_dados);
  //printf("estou checando a conexao\n");
  uint8_t Wifi_mode_5 = 0;
  get_Flag_CreateWiFi(&Wifi_mode_5);
  if ((localDevice.device_state == '2')&&(http_dados==0)&&(Wifi_mode_5!=5))
	{
          uint8_t checkconnection_status = 0;
          uint16_t checkconnection_ifalhasconexao = 0;
          uint8_t Wifi_mode_5 = 5;
          get_Flag_ConnectFail(&checkconnection_ifalhasconexao);
          printf("Modo de conexao e: %d\n", sdk_wifi_get_opmode());
          checkconnection_status = sdk_wifi_station_get_connect_status();
          bool is_secured_now = true;
          get_Flag_Seguranca(&is_secured_now);
          if (sdk_wifi_get_opmode()==SOFTAP_MODE) {
              checkconnection_ifalhasconexao++;
              set_Flag_ConnectFail(checkconnection_ifalhasconexao);
              if (checkconnection_ifalhasconexao>500) {
                    //set_Flag_Seguranca(false);
                    set_Flag_ConnectFail(0);
                    set_Flag_CreateWiFi(5);
                    wait(10);
                    printf("TENTANDO VOLTAR AO ESTADO INICIAL\n" );
                }
          }
          else
          {
            if (checkconnection_ifalhasconexao>100){
                    set_Flag_Seguranca(true);
                    set_Flag_CreateWiFi(Wifi_mode_5);
                    wait(10);
                    printf("TENTANDO VOLTAR AO ESTADO INICIAL\n" );
            }

          checkconnection_status = sdk_wifi_station_get_connect_status();


          //printf("status = %d\n\r", status);
          if (checkconnection_status == STATION_WRONG_PASSWORD) {
                      printf("WiFi: wrong password\n\r");
                      checkconnection_ifalhasconexao++;
                      set_Flag_ConnectFail(checkconnection_ifalhasconexao);
                      //set_Flag_Seguranca(true);

          } else if (checkconnection_status == STATION_NO_AP_FOUND) {
                      printf("WiFi: AP not found\n\r");
                      checkconnection_ifalhasconexao++;
                      set_Flag_ConnectFail(checkconnection_ifalhasconexao);
                      //set_Flag_Seguranca(true);

            } else if (checkconnection_status == STATION_CONNECT_FAIL) {
                      printf("WiFi: connection failed\r\n");
                      checkconnection_ifalhasconexao++;
                      set_Flag_ConnectFail(checkconnection_ifalhasconexao);
                      //set_Flag_Seguranca(true);

              }
              else if (checkconnection_status == STATION_GOT_IP) {
          			     printf("WiFi: connected\n");
                     set_Flag_Seguranca(false);  //parei aqui
          		}
          }
              printf("Modo Atual: %d --- SOFTAP_MODE %d -> Passei aqui no check nConnection: connect_status() %d\n", sdk_wifi_get_opmode(), SOFTAP_MODE, sdk_wifi_station_get_connect_status());
              printf("Erros até agora %d\n", checkconnection_ifalhasconexao);
              wait(10);
    }
}


static const char *getMCADD(void) {
    // Use MAC address for Station as unique ID

	static char my_id[13];

    static bool my_id_done = false;
    int8_t i;
    uint8_t x;
    if (my_id_done)
        return my_id;
    if (!sdk_wifi_get_macaddr(STATION_IF, (uint8_t *) my_id))
        return NULL;
    for (i = 5; i >= 0; --i) {
        x = my_id[i] & 0x0F;
        if (x > 9)
            x += 7;
        my_id[i * 2 + 1] = x + '0';
        x = my_id[i] >> 4;
        if (x > 9)
            x += 7;
        my_id[i * 2] = x + '0';

    }
    my_id[12] = '\0';
    my_id_done = true;


	return my_id;
}

static void showMessage(const char *sTask, const char *sFunc, const char *sTipo, const char *sMessage)
{
	printf("\n\n\n********************|| %s ||************************\n", sTipo);
	printf(" TASK        : %s \n", sTask);
	printf(" FUNCTION    : %s \n", sFunc);
	printf(" MESSAGE     : %s \n", sMessage);
	printf(" DISPARADA EM: %d ms\n", xTaskGetTickCount());
	printf("===================================================================\n\n");
}

static void showMemoryRAMStatus()
{
	//uint8_t localFlag_RecuperarConfig			= 0;
	//uint8_t localFlag_CreateWiFi				= 0;
	//uint8_t localFlag_InicializarIOs			= 0;
	//uint8_t localFlag_ReadWriteIOs				= 0;
	//uint8_t localFlag_TestOutputs				= 0;
	//uint8_t localFlag_ServidorTLS				= 0;

	//struct mallinfo after_info = mallinfo();
	uint32_t after = xPortGetFreeHeapSize();

	printf("\n\n=======================================================\n");
	//printf("localFlag_RecuperarConfig => %d\n", localFlag_RecuperarConfig);
	//printf("localFlag_CreateWiFi      => %d\n", localFlag_CreateWiFi);
	//printf("localFlag_InicializarIOs  => %d\n", localFlag_InicializarIOs);
	//printf("localFlag_ReadWriteIOs    => %d\n", localFlag_ReadWriteIOs);
	//printf("localFlag_TestOutputs     => %d\n", localFlag_TestOutputs);
	//printf("localFlag_ServidorTLS     => %d\n", localFlag_ServidorTLS);
	//printf("Arena size                => %d bytes\n", after_info.arena);
	printf("FreeHeapSize              => %d bytes\n", after);
	printf("=======================================================\n\n");
}

static void ExecutarTask(const char *sTask)
{
	//char queueContent[128];

	//bzero(queueContent, 128);
	//memcpy(queueContent, sTask, strlen(sTask));
	//xQueueSend(queue_taskControl, &queueContent, 0);

}

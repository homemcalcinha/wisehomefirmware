
static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
			strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}

void print_DeviceMemo(struct memoryreg_t localDevice)
{
	printf("\n\n************** DEVICE MEMO ******************\n");
	printf(" GUID           : %.36s\n", localDevice.GUID);
	printf(" key            : %.32s\n", localDevice.key);
	printf(" ssid_sta       : %s\n", localDevice.ssid_sta);
	printf(" pass_sta       : %s\n", localDevice.pass_sta);
	printf(" ssid_ap        : %s\n", localDevice.ssid_ap);
	printf(" pass_ap        : %s\n", localDevice.pass_ap);
	printf(" device_state   : %c\n", localDevice.device_state);
	printf(" Chip ID        : %d\n", localDevice.chip_id);
	printf(" Flash ID       : %d\n", localDevice.flash_id);
	printf("*********************************************\n\n");
}

bool desserializar_DeviceMemo(struct memoryreg_t *localDevice, unsigned char *sJson)
{
	printf("\n\n\n****************************************************\n\n");
	printf(" FUNCTION    : desserializar_DeviceMemo\n");
	printf(" OBJETIVO    : Converter JSON para objeto deviceMemo\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");
	
	
	printf("desserializar: %s\n", sJson);
	
    if (!sJson) return false;
	
	char *sTemp;
	//char *sCompleto = (char *)malloc(strlen(sJson));
	//memcpy(sCompleto, sJson, (strlen(sJson)));
	int i, iSize;
	int r;
	jsmn_parser p;
	jsmntok_t t[strlen(sJson)]; 
	
	jsmn_init(&p);
	r = jsmn_parse(&p, sJson, strlen(sJson), t, sizeof(t)/sizeof(t[0]));
	
	if (r < 0) {
		printf("Failed to parse JSON: %d\n", r);
		//free(sCompleto);
		return false;
	}
    
	if (r < 1 || t[0].type != JSMN_OBJECT) {
		printf("Object expected\n");
		//free(sCompleto);
		return false;
	}
    
	//Limpar a estrutura de comandos
	bzero((*localDevice).GUID, 36);
	bzero((*localDevice).key, 32);
	bzero((*localDevice).ssid_sta, 50);
	bzero((*localDevice).pass_sta, 50);
	bzero((*localDevice).ssid_ap, 50);
	bzero((*localDevice).pass_ap, 50);
	(*localDevice).device_state = '0';
	
	
	
	/* Loop over all keys of the root object */
	for (i = 1; i < r; i++) {
		if (jsoneq(sJson, &t[i], "GUID") == 0) {
			sTemp = (char *)zalloc(t[i+1].end-t[i+1].start+1);
			bzero(sTemp, (t[i+1].end-t[i+1].start+1));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localDevice).GUID, sTemp, (t[i+1].end-t[i+1].start+1));
			//sprintf((*localDevice).GUID, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		} else if (jsoneq(sJson, &t[i], "KEY") == 0) {
			sTemp = (char *)zalloc(t[i+1].end-t[i+1].start+1);
			bzero(sTemp, (t[i+1].end-t[i+1].start+1));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localDevice).key, sTemp, 32);
			//(*localDevice).key[31] = sTemp[31];
			//sprintf((*localDevice).key, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		} else if (jsoneq(sJson, &t[i], "key") == 0) {
			sTemp = (char *)zalloc(t[i+1].end-t[i+1].start+1);
			bzero(sTemp, (t[i+1].end-t[i+1].start+1));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localDevice).key, sTemp, 32);
			//sprintf((*localDevice).key, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		} else if (jsoneq(sJson, &t[i], "ssid_sta") == 0) {
			sTemp = (char *)zalloc(t[i+1].end-t[i+1].start+1);
			bzero(sTemp, (t[i+1].end-t[i+1].start+1));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localDevice).ssid_sta, sTemp, (t[i+1].end-t[i+1].start+1));
			//sprintf((*localDevice).ssid_sta, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		} else if (jsoneq(sJson, &t[i], "pass_sta") == 0) {
			sTemp = (char *)zalloc(t[i+1].end-t[i+1].start+1);
			bzero(sTemp, (t[i+1].end-t[i+1].start+1));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localDevice).pass_sta, sTemp, (t[i+1].end-t[i+1].start+1));
			//sprintf((*localDevice).pass_sta, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		} else if (jsoneq(sJson, &t[i], "device_state") == 0) {
			(*localDevice).device_state = (char)sJson[t[i+1].start];
			i++;
		} 
	}
	
	//free(sCompleto);
	free(sTemp);
	
	printf(" desserializar_DeviceMemo -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	
	return true;
}

int serializar_DeviceMemo(unsigned char **sJson)
{
	printf("\n\n\n****************************************************\n\n");
	printf(" FUNCTION    : serializar_DeviceMemo\n");
	printf(" OBJETIVO    : Converter objeto deviceMemo para JSON\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");
	
	struct memoryreg_t localDevice;
	
	get_DeviceMemo(&localDevice);
	
	int iSize = (sizeof(localDevice) + sizeof(JSON_DEVICEMEMO_SCHEMA) + 1);
	*sJson = (unsigned char *)zalloc(iSize);
	bzero(*sJson, iSize);
	
	sprintf((unsigned char *)*sJson, JSON_DEVICEMEMO_SCHEMA, localDevice.GUID, localDevice.key, localDevice.ssid_sta, localDevice.pass_sta, localDevice.ssid_ap, localDevice.pass_ap, localDevice.device_state);
	
	print_DeviceMemo(localDevice);

	printf(" serializar_DeviceMemo -> TERMINO EM : %d ms\n", xTaskGetTickCount());
	
	return iSize;
}


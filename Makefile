PROGRAM = WisehomeFactory

EXTRA_COMPONENTS = extras/mbedtls extras/jsmn extras/spiffs extras/dhcpserver extras/httpd lib/irremote extras/rboot-ota
OTA=1


FLASH_SIZE = 32
# spiffs configuration
SPIFFS_BASE_ADDR = 0x200000
SPIFFS_SIZE = 0x010000
LIBS=hal gcc c m
# For the mDNS responder included with lwip:
#EXTRA_CFLAGS += -DLWIP_MDNS_RESPONDER=1 -DLWIP_NUM_NETIF_CLIENT_DATA=1 -DLWIP_NETIF_EXT_STATUS_CALLBACK=1

# Avoid writing the wifi state to flash when using wificfg.
EXTRA_CFLAGS += -DWIFI_PARAM_SAVE=0 -DLWIP_HTTPD_CGI=1 -DLWIP_HTTPD_SSI=1 -I./fsdata

include ../../common.mk

#Faz upload de arquivos para a memoria
$(eval $(call make_spiffs_image,files))

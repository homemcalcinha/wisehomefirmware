/* *******************************************************************************************************

 FUNCTION	: checkDestiny
 OBJETIVO	: Checar se o GUID passado pertence ao dispositivo

*********************************************************************************************************/
bool checkDestiny(char sGUID[37])
{
	if (strcmp(sGUID, "")==0)
	{
		printf("checkDestiny => Destino generico.\n");
		return true;
	}
	int i;
	char *localGUID = (char *)malloc(36);

	get_GUID(&localGUID);


	for (i = 0; i < 36; i++)
	{
		if ((char)sGUID[i] != (char)localGUID[i] )
		{
			printf("checkDestiny => Destino recusado.\n");
			return false;
		}
	}

	printf("checkDestiny => Destino verificado.\n");
	free(localGUID);

	return true;
}

bool desserializar_Command(const char *sJson, char **sCommand, char **sPort, char **sData)
{
    if (!sJson) return false;

	int i, r;
	jsmn_parser p;
	jsmntok_t t[strlen(sJson)]; /* We expect no more than 128 tokens */

	jsmn_init(&p);
	r = jsmn_parse(&p, sJson, strlen(sJson), t, sizeof(t)/sizeof(t[0]));
	if (r < 0) {
		printf("Failed to parse JSON: %d\n", r);
		return false;
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT) {
		printf("Object expected\n");
		return false;
	}

	bzero(*sCommand, sizeof(*sCommand));
	bzero(*sPort, sizeof(*sPort));
	bzero(*sData, sizeof(*sData));

	for (i = 1; i < r; i++)
	{
		if (jsoneq(sJson, &t[i], "command") == 0) {
			sprintf(*sCommand, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		} else if (jsoneq(sJson, &t[i], "port") == 0) {
			sprintf(*sPort, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		} else if (jsoneq(sJson, &t[i], "data") == 0) {
			sprintf(*sData, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;
		}
	}

	return true;
}

/* *******************************************************************************************************

 FUNCTION	: desserializar_deviceCommand
 OBJETIVO	: Converter JSON para o objeto deviceCommand

*********************************************************************************************************/
bool desserializar_deviceCommand(struct command_t *localCommand, char *sJson)
{
	char sTemp[2048];
	printf("\n\n\n****************************************************\n\n");
	printf(" FUNCTION    : desserializar_deviceCommand\n");
	printf(" OBJETIVO    : Converter JSON para objeto deviceCommand\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");

	//printf("JSON RECEIVED: %s size: %d\n", sJson, strlen(sJson));

    if (!sJson) return false;

	printf("%s", sJson);

	int i, j, k, r, iIndex;
	bool bFlag;

	jsmn_parser p;
	jsmntok_t t[strlen(sJson)]; /* We expect no more than 128 tokens */

	jsmn_init(&p);
	r = jsmn_parse(&p, sJson, strlen(sJson), t, sizeof(t)/sizeof(t[0]));
	if (r < 0) {
		printf("Failed to parse JSON: %d\n", r);
		return false;
	}

	/* Assume the top-level element is an object */
	if (r < 1 || t[0].type != JSMN_OBJECT) {
		printf("Object expected\n");
		return false;
	}


	//Limpar a estrutura de comandos
	bzero((*localCommand).source, sizeof((*localCommand).source));
	bzero((*localCommand).destiny, sizeof((*localCommand).destiny));
	bzero((*localCommand).date, sizeof((*localCommand).date));
	for (k = 0; k < 10; k++)
	{
		bzero((*localCommand).queueCommand[k], sizeof((*localCommand).queueCommand[k]));
	}


	/* Loop over all keys of the root object */
	for (i = 1; i < r; i++) {
		if (jsoneq(sJson, &t[i], "source") == 0) {
			//sprintf((*localCommand).source, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			bzero(sTemp, sizeof(sTemp));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localCommand).source, sTemp, strlen(sTemp));
			//printf("- source: %s\n", (*localCommand).source);
			i++;
		} else if (jsoneq(sJson, &t[i], "destiny") == 0) {
			//sprintf((*localCommand).destiny, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			bzero(sTemp, sizeof(sTemp));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localCommand).destiny, sTemp, strlen(sTemp));
			//printf("- destiny: %s\n", (*localCommand).destiny);
			i++;
		} else if (jsoneq(sJson, &t[i], "date") == 0) {
			//sprintf((*localCommand).date, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			bzero(sTemp, sizeof(sTemp));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			memcpy((*localCommand).date, sTemp, strlen(sTemp));
			//printf("- date: %s\n", (*localCommand).date);
			i++;
		} else if (jsoneq(sJson, &t[i], "queueContent") == 0) {
			bzero(sTemp, sizeof(sTemp));
			sprintf(sTemp, "%.*s", t[i+1].end-t[i+1].start, sJson + t[i+1].start);
			i++;


			k = 0;
			iIndex = 0;
			bFlag = false;
			//printf("\n\n************** LISTA DE COMANDOS ***************\n");
			for (j = 0; j < strlen(sTemp); j++)
			{
				if ((char)sTemp[j] == '{') bFlag = true;
				if (((char)sTemp[j] != '[' || (char)sTemp[j] != ']') && (bFlag))
				{
					//printf("%c", (char)(*localCommand).queueContent[j]);
					(*localCommand).queueCommand[k][iIndex] = (char)sTemp[j];
					iIndex++;

					//Determina onde termina o comado
					if ((char)sTemp[j] == '}')
					{
						//printf("\n*** FIM DO COMANDO %d ***\n\n", k);
						bFlag = false;
						iIndex = 0;
						k++;
					}


				}
			}


		}
	}


	printf("\n\n************** DEVICE COMMAND ***************\n");
	printf(" source         : %s\n", (char *)(*localCommand).source);
	printf(" destiny        : %s\n", (char *)(*localCommand).destiny);
	printf(" date           : %s\n\n", (char *)(*localCommand).date);
	printf(" == COMMANDS ==\n\n");
	for (k = 0; k < 10; k++)
	{
		if (strlen((*localCommand).queueCommand[k])>0)
			printf("    %d: %s \n", k, (*localCommand).queueCommand[k]);
	}
	printf("*********************************************\n\n");

	printf(" TERMINO EM : %d ms\n", xTaskGetTickCount());

	return true;
}

/* *******************************************************************************************************

 FUNCTION	: executar
 OBJETIVO	: Executar comando

*********************************************************************************************************/
bool executar(const char *sCommand, const char *sPort, const char *sData)
{
	set_Flag_AfterSave(1);
	char *sJsonCrypt = (char *)malloc(1024);
	char *sJson = (char *)malloc(1024);
	bool bFlag = false;

	printf("EXECUTAR COMANDO: %s, PORTA: %s, DADOS: %s ...", sCommand, sPort, sData );

	if (strcmp(sCommand, "0001")==0)
	{
		printf(" OK.\n");
		//sdk_system_restart();
		set_Flag_Reiniciar(1);
		bFlag = true;
	}
	if (strcmp(sCommand, "0010")==0)
	{
		set_DeviceMemo_Ssid_sta(sData);
		printf(" OK.\n");
		bFlag = true;
	}

	if (strcmp(sCommand, "0011")==0)
	{
		set_DeviceMemo_Pass_sta(sData);
		printf(" OK.\n");
		bFlag = true;
	}



	if (strcmp(sCommand, "9999")==0)
	{
		printf("estoudentro");
		set_Flag_AfterSave(1);
		set_Flag_ReceivingHTTP(3);
		set_Flag_ServidorHTTP(3);
		bFlag = true;
		wait(4000);
		set_DeviceMemo_Device_State('0');
		//set_DeviceMemo_Pass_sta(' ');
		//set_DeviceMemo_Ssid_sta(' ');
		set_Flag_AfterSave(1);
		set_Flag_ReceivingHTTP(3);
		set_Flag_ServidorHTTP(3);
		bFlag = true;
		wait(4000);
		printf("estou setando o config pra gravar ");
		set_Flag_GravarConfig(3);


		//

		printf(" OK.\n");
		bFlag = true;
	}


	if (strcmp(sCommand, "0019")==0)
	{
		set_DeviceMemo_Device_State('1');
		printf(" OK.\n");
		bFlag = true;
	}
	if (strcmp(sCommand, "0020")==0)
	{
		set_Flag_AfterSave(5);
		wait(100);
		set_DeviceMemo_Device_State('2');

		printf(" OK.\n");
		bFlag = true;
	}


	if (strcmp(sCommand, "7774")==0)
	{
		set_Flag_OTA_SERVER(sData);
		printf(" OK.\n");
		bFlag = true;
	}
	if (strcmp(sCommand, "7775")==0)
	{
		set_Flag_OTA_FILE(sData);
		printf(" OK.\n");
		bFlag = true;
	}
	if (strcmp(sCommand, "7776")==0)
	{
		set_Flag_OTA_SHA(sData);
		printf(" OK.\n");
		bFlag = true;
	}


	if (strcmp(sCommand, "7777")==0)
	{
		set_Flag_OTA(5);
		wait(100);
		//set_DeviceMemo_Device_State('2');

		printf(" OK.\n");
		bFlag = true;
	}

	if (strcmp(sCommand, "1000")==0)
	{

		//serializar_DeviceMemo(&sJson);
		//sJsonCrypt = (encrypt(sJson, globalKey, globalIV));
		//
		//esp_spiffs_init();
		//if (esp_spiffs_mount() != SPIFFS_OK) {
		//	showMessage("Function executar", "Montar SPIFFS", "ERRO", "Falha ao montar a SPIFFS");
		//}
		//
		//int fd = open("config.cfg", O_WRONLY|O_CREAT, 0);
		//if (fd < 0) {
		//	showMessage("Function executar", "Criar config.cfg", "ERRO", "Falha ao criar o arquivo config.cfg");
		//}
		//else
		//{
		//	write(fd, sJsonCrypt, strlen(sJsonCrypt));
		//}
		//close(fd);
		//SPIFFS_unmount(&fs);
		printf("\n entrei no comando 1000 OK.\n");




		//set_Flag_GravarConfig(0); // mudado pelo keko
		//taskGravarConfig(NULL);


printf("\n\n\n****************************************************\n\n");
printf(" FUNCTION    : GravarConfig Dentro da Tarefa-Exec-utils\n");
printf(" OBJETIVO    : Serializa e armazena os dados de configuracao\n\n");
printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
printf("***************************************************\n\n");


printf("linha 1\n");
struct memoryreg_t localDevice;
printf("linha 2\n");
get_DeviceMemo(&localDevice);

printf("linha 3\n");
//char *sJson = (char *)malloc(1025);

//char *sJson; // 				= (char *)malloc(1500);
printf("linha 4\n");
//char *sJsonCrypt 			= (char *)malloc(1024);


printf("linha 5\n");
unsigned char *oGlobalKey 	= (unsigned char *)malloc(33);
printf("linha 6\n");
unsigned char *oGlobalIV 	= (unsigned char *)malloc(17);

printf("linha 7\n");
get_globalKey(&oGlobalKey);
printf("linha 8\n");
get_globalIV(&oGlobalIV);
printf("linha 9\n");

//bzero(sJson, 1499);
printf("linha 10\n");
//bzero(sJsonCrypt, 1499);
printf("linha 11\n");
serializar_DeviceMemo(&sJson);

printf("linha 12\n");

int iSize = vEncrypt(&sJsonCrypt, sJson, globalKey, globalIV);
free(sJson);

sJsonCrypt[iSize] = 0;
//sJsonCrypt[iSize+1] = 0;
printf("linha 13\n");
sJsonCrypt = append(sJsonCrypt, "|");

unsigned char *kbrax 	= (unsigned char *)malloc(strlen(sJsonCrypt)+1);


showMemoryRAMStatus();

kbrax 	= (char*)left(sJsonCrypt,strlen(sJsonCrypt));

printf("kbrax: %s#############\n", kbrax);





printf("linha 14\n");

free(sJsonCrypt);

if( xSemaforo_spiffs != NULL )
{
	if( xSemaphoreTake( xSemaforo_spiffs, portMAX_DELAY) )
	{
		esp_spiffs_init();
printf("linha 15\n");
		if (esp_spiffs_mount() != SPIFFS_OK) {
			printf("linha 16\n");
			showMessage("taskGravarConfig", "Montar SPIFFS", "ERRO", "Falha ao montar a SPIFFS");
		}

printf("linha 17\n");
		int fd = open("config.cfg", O_WRONLY|O_CREAT, 0);
printf("linha 18\n");
		if (fd < 0) {
			printf("linha 19\n");
			showMessage("taskGravarConfig", "Criar config.cfg", "ERRO", "Falha ao criar o arquivo config.cfg");
		}
		else
		{
			//write(fd, sJsonCrypt, strlen(sJsonCrypt));
printf("linha 20\n");
			//SPIFFS_write(&fs, (spiffs_file)fd, (char*)left(sJsonCrypt, iSize+5), iSize+5);
			SPIFFS_write(&fs, (spiffs_file)fd, kbrax, strlen(kbrax));

printf("linha 21\n");
			free(kbrax);
		/*
			SPIFFS_write(&fs, (spiffs_file)fd, (char*)"|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ", 1000);
		*/

printf("linha 22\n");

		}
printf("linha 23\n");
		close(fd);
printf("linha 24\n");
		SPIFFS_unmount(&fs);
printf("linha 25\n");
		esp_spiffs_deinit();


		xSemaphoreGive( xSemaforo_spiffs);
	}
}

printf(" TERMINO EM : %d ms\n", xTaskGetTickCount());

printf("linha 26\n");
free(sJson);
printf("linha 27\n");
free(sJsonCrypt);
printf("linha 28\n");
free(oGlobalKey);
printf("linha 29\n");
free(oGlobalIV);
printf("linha 30 ????????\n");



set_Flag_GravarConfig(2);
set_Flag_AfterSave(0);








		// wait(500);  // mudado pelo keko

		printf("\n comando 1000 OK.\n");
		bFlag = true;
	}

	if (strcmp(sCommand, "1717")==0)
	{
		printf("executar OTA");
		//set_Flag_OTA(3);
		set_Flag_Reiniciar(3);
		printf(" OK.\n");
		bFlag = true;
	}


	if (strcmp(sCommand, "5001")==0)
	{
		if (strcmp(sPort, "0001")==0)
		{
			set_SD_state(1, true);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0002")==0)
		{
			set_SD_state(2, true);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0003")==0)
		{
			set_SD_state(3, true);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0004")==0)
		{
			set_SD_state(4, true);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0005")==0)
		{
			set_SD_state(5, true);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0006")==0)
		{
			//Triac1.analog_value = 100;
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0007")==0)
		{
			//Triac2.analog_value = 100;
			printf(" OK.\n");
			bFlag = true;
		}
	}

	if (strcmp(sCommand, "5002")==0)
	{
		if (strcmp(sPort, "0001")==0)
		{
			set_SD_state(1, false);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0002")==0)
		{
			set_SD_state(2, false);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0003")==0)
		{
			set_SD_state(3, false);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0004")==0)
		{
			set_SD_state(4, false);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0005")==0)
		{
			set_SD_state(5, false);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0006")==0)
		{
			//Triac1.analog_value = 0;
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0007")==0)
		{
			//Triac2.analog_value = 0;
			printf(" OK.\n");
			bFlag = true;
		}
	}

	if (strcmp(sCommand, "5003")==0)
	{
		if (strcmp(sPort, "0001")==0)
		{
			invert_SD_state(1);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0002")==0)
		{
			invert_SD_state(2);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0003")==0)
		{
			invert_SD_state(3);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0004")==0)
		{
			invert_SD_state(4);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0005")==0)
		{
			invert_SD_state(5);
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0006")==0)
		{
			//if (Triac1.analog_value == 0) Triac1.analog_value = 100;
			//else Triac1.analog_value = 0;
			printf(" OK.\n");
			bFlag = true;
		}
		if (strcmp(sPort, "0007")==0)
		{
			//if (Triac2.analog_value == 0) Triac2.analog_value = 100;
			//else Triac2.analog_value = 0;
			printf(" OK.\n");
			bFlag = true;
		}
	}



	if (bFlag == false) printf(" Comando nao encontrado.\n");

	free(sJson);
	free(sJsonCrypt);

	set_Flag_AfterSave(0);


	return bFlag;
}


/* *******************************************************************************************************

 FUNCTION	: executar_deviceCommand
 OBJETIVO	: Percorre o objeto localCommand desserializa e executa toda a fila de comandos

*********************************************************************************************************/
bool executar_deviceCommand(struct command_t localCommand)
{
	printf("\n\n\n****************************************************\n\n");
	printf(" FUNCTION    : executar_deviceCommand\n");
	printf(" OBJETIVO    : Percorre o objeto localCommand\n");
	printf("               desserializa e executa toda a fila de comandos\n\n");
	printf(" INICIADO EM : %d ms\n", xTaskGetTickCount());
	printf("***************************************************\n\n");

	char *sJson 	= (char *)malloc(256);
	char *sCommand 	= (char *)malloc(5);
	char *sPort 	= (char *)malloc(5);
	char *sData 	= (char *)malloc(256);
	int k;

	//Entrar se o comando for para o dispositivo
	if (checkDestiny(localCommand.destiny))
	{

		//Percorrer queue de comandos
		for (k = 0; k < 10; k++)
		{
			if (strlen(localCommand.queueCommand[k])>0)
			{
				//printf("COMANDO %d: %s \n", k, localCommand.queueCommand[k]);
				bzero(sJson, 256);
				memcpy(sJson, localCommand.queueCommand[k], strlen(localCommand.queueCommand[k]));

				if (desserializar_Command(sJson, &sCommand, &sPort, &sData))
				{
					//if (sCommand) printf("COMANDO: %s\n", sCommand);
					//if (sPort) printf("PORTA: %s\n", sPort);
					//if (sData) printf("DATA: %s\n\n", sData);
					executar(sCommand, sPort, sData);

				}
				else
				{
					printf("falha ao desserializar comando.\n");
				}
			}
		}

		//print_DeviceMemo();

		printf(" EXECUTADO \n\n");
	}

	free(sJson);
	free(sCommand);
	free(sPort);
	free(sData);


	printf(" TERMINO EM : %d ms\n", xTaskGetTickCount());

	return true;
}
